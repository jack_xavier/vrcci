<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'subject'           => 'Вы успешно зарегистрированы',
    'welcome'           => 'Добро пожаловать в',
    'headline-text-one' => 'Планируйте с удобством',
    'headline-text-two' => 'и удовольствием!',
    'link-text'         => 'Начать планировать',
    'footer-text'       => 'С уважением,',
];
