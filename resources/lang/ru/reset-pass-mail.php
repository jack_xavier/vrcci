<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'subject' => 'Восстановление пароля',

    'hello'                  => 'Здравствуйте!',
    'p1-text-on-site'        => 'На сайте',
    'p1-text-was-made'       => 'сделан запрос',
    'p1-text-to-change-pass' => 'на смену пароля аккаунта',
    'p2-text'                => 'Чтобы изменить пароль нажмите на кнопку:',
    'btn1-text'              => 'Сбросить пароль',
    'p3-text-link'           => 'Или перейдите по ссылке:',
    'p4-text'                => 'Данная ссылка будет действительна в течение',
    'p4-text-time'           => '60 минут.',
    'p5-text'                => 'Если вы не подавали запроса на изменение пароля или передумали его изменять — проигнорирйте данное письмо.',
    'footer-text'            => 'C уважением,',
];
