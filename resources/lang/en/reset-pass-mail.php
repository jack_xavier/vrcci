<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'subject' => 'Password reset',

    'hello'                  => 'Hello!',
    'p1-text-on-site'        => 'On web-site',
    'p1-text-was-made'       => 'it was requested ',
    'p1-text-to-change-pass' => 'to reset your account password',
    'p2-text'                => 'Click the button bellow to reset it.',
    'btn1-text'              => 'Reset your password',
    'p3-text-link'           => 'Or follow the link bellow:',
    'p4-text'                => 'This password reset is only valid for the next',
    'p4-text-time'           => '60 minutes.',
    'p5-text'                => 'If you haven\'t requested to reset your password or you\'ve changed your mind, you can simply ignore this email.',
    'footer-text'            => 'Gratefully yours,',
];
