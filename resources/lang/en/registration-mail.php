<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'subject'           => 'You have successfully registered',
    'welcome'           => 'Welcome to',
    'headline-text-one' => 'Start planning with greater convenience',
    'headline-text-two' => 'and pleasure!',
    'link-text'         => 'Start planning',
    'footer-text'       => 'Gratefully yours,',
];
