<?php

namespace App\Module\Feature;

trait ProvidesModuleConfig
{
    /**
     * @param string $key
     *
     * @return mixed
     */
    public static function config($key)
    {
        return \Config::get(static::ns($key, '.'));
    }
}
