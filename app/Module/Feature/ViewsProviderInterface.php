<?php

namespace App\Module\Feature;

interface ViewsProviderInterface
{
    /**
     * @return string
     */
    public function getViewsPath();
}
