<?php

namespace App\Module\Feature;

interface AliasProviderInterface
{
    /**
     * @return string
     */
    public static function getAlias();
}
