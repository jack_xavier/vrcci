<?php

namespace App\Module\Feature;

interface ConfigProviderInterface
{
    /**
     * @return string
     */
    public function getConfigPath();
}
