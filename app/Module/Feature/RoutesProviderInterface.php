<?php

namespace App\Module\Feature;

interface RoutesProviderInterface
{
    /**
     * @return string[]|string
     */
    public function getRoutesPath();
}
