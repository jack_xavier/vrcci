<?php

namespace App\Module\Feature;

trait ProvidesNamespacedPath
{
    /**
     * @param string $path
     * @param string $delimiter
     *
     * @return string
     */
    public static function ns($path, $delimiter = '::')
    {
        if (!is_callable(sprintf('%s::%s', static::class, 'getAlias'))) {
            throw new \RuntimeException(
                sprintf('Module [%s] does not provide alias', static::class)
            );
        }

        return sprintf('%s%s%s', static::getAlias(), $delimiter, $path);
    }
}
