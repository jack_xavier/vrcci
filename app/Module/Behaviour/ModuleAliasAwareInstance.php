<?php

namespace App\Module\Behaviour;

interface ModuleAliasAwareInstance
{
    /**
     * @param string $alias
     *
     * @return void
     */
    public function setModuleAlias(string $alias);
}
