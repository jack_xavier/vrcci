<?php

namespace App\Service;

class PasswordGenerator
{
    /**
     * @param int $length Password length
     *
     * @return string
     */
    public function generate(int $length): string
    {
        return bin2hex(random_bytes($length > 1 ? $length / 2 : 1));
    }
}
