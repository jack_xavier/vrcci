<?php

namespace App\Service;

use Illuminate\Routing\Route;
use Vrcci\User\Dto\UserTo;

class RoutePermissionCheck
{
    /**
     * @var array
     */
    protected $rules = [];

    /**
     * @param array $rules
     */
    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    /**
     * @param UserTo $user
     * @param Route  $route
     *
     * @return bool
     */
    public function isGranted(UserTo $user, Route $route): bool
    {
        $allowedPermissions = null;

        foreach ($this->rules as $routeRule => $permissions) {
            if (fnmatch($routeRule, $route->getName(), FNM_CASEFOLD | FNM_NOESCAPE)) {
                $allowedPermissions = (array)$permissions;
                break;
            }
        }

        if (empty($allowedPermissions)) {
            return true;
        }

        if (in_array('*', $allowedPermissions)) {
            return true;
        }

        return $user->getOriginal()->hasPermission($allowedPermissions);
    }
}
