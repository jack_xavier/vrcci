<?php

namespace App\Service;

use Illuminate\Http\UploadedFile;
use App\Entity\File;

class StorageService
{
    public const VAR_DEFAULT_STORAGE_DISK = 'local';

    /**
     * @param string       $filename
     * @param UploadedFile $file
     *
     * @return UploadedFile
     */
    public static function storeFile(string $filename, UploadedFile $file): UploadedFile
    {
        $pathParts = pathinfo($filename);
        $file      = $file->move($pathParts['dirname'], $pathParts['basename']);

        return new UploadedFile($file->getPathname(), $file->getPathname(), $file->getMimeType());
    }


    public function replace(File $file, UploadedFile $uploaded){
        if(Storage::disk(self::VAR_DEFAULT_STORAGE_DISK)->exists($filename)){

        }
    }

    public static function getFileUrl(string $filePath): ?string
    {
        return Storage::disk(self::VAR_DEFAULT_STORAGE_DISK)->url($filePath);
    }


    public function delete(File $file){
        return Storage::disk(self::VAR_DEFAULT_STORAGE_DISK)->delete($file->filename);
    }
}
