<?php

namespace App\Criterion\Criteria;

use App\Eloquent\AbstractDataRequest;

abstract class AbstractCriteria implements CriteriaInterface
{
    /**
     * @var boolean
     */
    protected $applied = false;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @return boolean
     */
    public function isApplied()
    {
        return $this->applied;
    }

    /**
     * @param mixed $value
     *
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param AbstractDataRequest $dataRequest
     */
    public function apply(AbstractDataRequest $dataRequest)
    {
        $this->applied = $this->doApply($dataRequest);
    }

    /**
     * @param AbstractDataRequest $dataRequest
     *
     * @return bool
     */
    abstract protected function doApply(AbstractDataRequest $dataRequest): bool;
}
