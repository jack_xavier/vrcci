<?php

namespace App\Criterion\Criteria;

use App\Eloquent\AbstractDataRequest;

interface CriteriaInterface
{
    /**
     * @param AbstractDataRequest $dataRequest
     */
    public function apply(AbstractDataRequest $dataRequest);

    /**
     * @return boolean
     */
    public function isApplied();

    /**
     * @param mixed $value
     *
     * @return void
     */
    public function setValue($value);

    /**
     * @return mixed
     */
    public function getValue();
}
