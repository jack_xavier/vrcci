<?php

namespace App\Criterion\Criteria;

use App\Eloquent\AbstractDataRequest;

class OrderDirectionCriteria extends AbstractCriteria
{
    /**
     * @param AbstractDataRequest $dataRequest
     *
     * @return bool
     */
    protected function doApply(AbstractDataRequest $dataRequest): bool
    {
        if (empty($this->value) || !$this->cheak($this->value)) {
            return false;
        }

        $dataRequest->sort($this->value['field'], $this->value['direction']);

        return true;
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    private function cheak($value): bool
    {
        return is_array($value) && isset($value['field']) && isset($value['direction']);
    }
}
