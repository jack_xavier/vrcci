<?php

namespace App\Criterion;

use App\Criterion\Criteria\CriteriaInterface;
use App\Criterion\Exception\InvalidArgumentException;
use IteratorAggregate;
use SplPriorityQueue;

class Filter implements IteratorAggregate
{
    /**
     * @var array
     */
    private $criteria = [];

    /**
     * @var array
     */
    protected $priority = [];

    /**
     * @var SplPriorityQueue|CriteriaInterface[]
     */
    protected $boundQueue;

    /**
     * @var CriteriaInterface[]
     */
    protected $boundRegistry = [];

    public function __construct()
    {
        $this->boundQueue = new SplPriorityQueue();
    }

    /**
     * @param string            $name
     * @param CriteriaInterface $criteria
     * @param int               $priority
     *
     * @return Filter
     */
    public function attachCriteria(string $name, CriteriaInterface $criteria, $priority = 1): Filter
    {
        $this->criteria[$name] = $criteria;
        $this->priority[$name] = $priority;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasCriteria($name): bool
    {
        return isset($this->criteria[$name]);
    }

    /**
     * @param string $name
     *
     * @return CriteriaInterface
     */
    public function getCriteria(string $name): CriteriaInterface
    {
        if (!$this->hasCriteria($name)) {
            throw InvalidArgumentException::criteriaIsNotDefined($name);
        }

        return $this->criteria[$name];
    }

    /**
     * @return array
     */
    public function getAllCriteria(): array
    {
        return $this->criteria;
    }

    /**
     * @param array $params
     */
    public function populate(array $params)
    {
        foreach ($params as $name => $value) {
            if (!$this->hasCriteria($name)) {
                continue;
            }

            $this->bindParam($name, $value);
        }
    }

    /**
     * @return CriteriaInterface[]|SplPriorityQueue
     */
    public function getIterator()
    {
        return clone $this->boundQueue;
    }

    /**
     * @param string            $name
     * @param CriteriaInterface $criteria
     * @param int               $priority
     *
     * @return Filter
     */
    public function attachAndBindCriteria(string $name, CriteriaInterface $criteria, int $priority = 1): Filter
    {
        $this->attachCriteria($name, $criteria, $priority)->bindCriteria($criteria, $priority);

        return $this;
    }

    /**
     * @param CriteriaInterface $criteria
     * @param int               $priority
     */
    private function bindCriteria(CriteriaInterface $criteria, $priority)
    {
        $hash = spl_object_hash($criteria);

        if (in_array($hash, $this->boundRegistry)) {
            return;
        }

        $this->boundQueue->insert($criteria, $priority);
        array_push($this->boundRegistry, $hash);
    }

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @return self
     */
    private function bindParam($name, $value)
    {
        $criteria = $this->getCriteria($name);
        $priority = isset($this->priority[$name]) ? $this->priority[$name] : 1;

        $criteria->setValue($value);
        $this->bindCriteria($criteria, $priority);

        return $this;
    }
}
