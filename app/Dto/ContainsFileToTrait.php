<?php

namespace App\Dto;

use Vrcci\Excursion\Dto\ExcursionTo;

trait ContainsFileToTrait
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $filename;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return ExcursionTo
     */
    public function setUrl(string $url = ''): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     *
     * @return ExcursionTo
     */
    public function setFilename(string $filename = ''): self
    {
        $this->filename = $filename;

        return $this;
    }
}
