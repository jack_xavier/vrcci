<?php

namespace App\Entity\Guard;

use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Config;
use Psr\Log\InvalidArgumentException;

trait GuardForDateTime
{
    /**
     * @param DateTime|Carbon|string|null $date
     * @param DateTimeZone|string|null    $tz The Timezone in which the date is to be transformed
     *
     * @return Carbon|DateTime|null
     * @throws InvalidArgumentException
     */
    public function guardForDateTimeZone($date = null, $tz = null)
    {
        $tz = $tz ?: Config::get('app.timezone');

        if (null === $date || is_string($date)) {
            $date = Carbon::parse($date);
        }

        if (!$date instanceof DateTime && !$date instanceof Carbon) {
            throw new InvalidArgumentException(
                sprintf(
                    "Date must be a valid date string, DateTime or Carbon object, [%s] given",
                    is_object($date) ? get_class($date) : gettype($date)
                )
            );
        }

        if (is_string($tz)) {
            $tz = new DateTimeZone($tz);
        }

        if (!$tz instanceof DateTimeZone) {
            throw new InvalidArgumentException(
                sprintf(
                    "Tz must be a valid timezone string or DateTimeZone object, [%s] given",
                    is_object($tz) ? get_class($tz) : gettype($tz)
                )
            );
        }

        $date->setTimezone($tz);

        return $date;
    }
}
