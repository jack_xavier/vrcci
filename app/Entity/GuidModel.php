<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $guid
 */
class GuidModel extends Model
{
    protected $primaryKey = 'guid';

    public $incrementing = false;

    public function save(array $options = [])
    {
        if (null === $this->guid) {
            $this->guid = $this->generateGuid();
        }

        return parent::save($options);
    }

    /**
     * Generates random identifier
     *
     * @return string
     */
    private function generateGuid(): string
    {
        return GuidGenerator::generateGuid();
    }
}
