<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\UploadedFile;
use Storage;
use Vrcci\User\Entity\User;

/**
 * @property string $guid
 * @property string $original_filename
 * @property string $filename
 */
abstract class File extends GuidModel
{
    public const VAR_DEFAULT_STORAGE_DISK = 'local';

    /**
     * @var string[]
     */
    protected $fillable = [
        'guid',
        'original_filename',
        'filename',
    ];

    public function setFile(UploadedFile $file): self
    {
        if (null !== $this->filename) {
            $this->removeFile();
        }

        $this->storeFile($file);

        return $this;
    }

    public function removeFile(): bool
    {
        if (file_exists($this->prepareFilePath($this->filename))) {
            return unlink($this->prepareFilePath($this->filename));
        }

        return false;
    }

    public function getFileUrl(): string
    {
        return Storage::disk(self::VAR_DEFAULT_STORAGE_DISK)->url(
            $this->prepareFilePath($this->filename)
        );
    }

    private function storeFile(UploadedFile $file): UploadedFile
    {
        $newName = $this->prepareFilePath($file->getFilename());
        $extension = $file->getClientOriginalExtension();
        $this->original_filename = $file->getClientOriginalName();
        $pathParts = pathinfo($newName);

        $this->filename = sprintf('%s.%s', $pathParts['basename'], $extension);
        $file = $file->move(
            $pathParts['dirname'],
            $this->filename
        );

        return new UploadedFile($file->getPathname(), $file->getPathname(), $file->getMimeType());
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    private function prepareFilePath(string $fileName): string
    {
        $directoryName = $this->getDirectoryName();

        if (!Storage::disk(self::VAR_DEFAULT_STORAGE_DISK)->exists($directoryName)) {
            Storage::disk(self::VAR_DEFAULT_STORAGE_DISK)->makeDirectory($directoryName);
        }

        return storage_path(sprintf('app/uploads/%s/%s', $directoryName, $fileName));
    }

    /**
     * @return BelongsTo
     */
    public function uploadedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'uploaded_by_id', 'id');
    }

    public function getFileSize(): int
    {
        $filePath = $this->getDirectoryName() . '/' . $this->filename;

        if (!Storage::disk(self::VAR_DEFAULT_STORAGE_DISK)->exists($filePath)) {
            return 0;
        }

        return Storage::disk(self::VAR_DEFAULT_STORAGE_DISK)->size(
                $filePath
            ) / 1000;
    }

    public function delete()
    {
        if ($this->removeFile()) {
            return parent::delete();
        }

        return false;
    }

    abstract protected function getDirectoryName(): string;
}
