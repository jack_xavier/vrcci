<?php

namespace App\Entity;

use App\Entity\Guard\GuardForDateTime;
use Illuminate\Database\Eloquent\Model;

abstract class TimezoneAwareModel extends Model
{
    use GuardForDateTime;

    /**
     * @inheritdoc
     */
    protected function asDateTime($value)
    {
        return parent::asDateTime($this->guardForDateTimeZone($value));
    }
}
