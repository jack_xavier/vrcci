<?php

namespace App\Entity;

class GuidGenerator
{
    public const GUID_PATTERN = '[0-9A-z]{8}\-[0-9A-z]{4}\-[0-9]{4}\-[0-9]{4}\-[0-9A-z]{12}';
    /**
     * Generates GUID by format q0werty5-ui7o-9135-7913-pas4567890d1
     *
     * @return string
     */
    public static function generateGuid(): string
    {
        return implode(
            '-',
            [
                self::random(8),
                self::random(4),
                self::random(4, true),
                self::random(4, true),
                self::random(12),
            ]
        );
    }

    private static function random($length, $numbersOnly = false)
    {
        $randomString = '';
        $characters = '0123456789';

        if (!$numbersOnly) {
            $characters .= 'abcdefghijklmnopqrstuvwxyz';
        }

        $totalCharsAvailable = strlen($characters);

        for ($i = 0; $i < $length; $i++) {
            $randomKey = mt_rand(1, $totalCharsAvailable);
            $randomChararcter = $characters[$randomKey - 1];
            $randomString .= $randomChararcter;
        }

        return $randomString;
    }
}
