<?php

namespace App\Validator;

trait CompositeMessageRuleTrait
{
    /**
     * @var string|null
     */
    protected $error = null;

    /**
     * @var string[]
     */
    protected $messages = [];

    /**
     * @return string|null
     */
    public function message(): ?string
    {
        return $this->defineErrorMessage();
    }

    /**
     * @return string|null
     */
    protected function defineErrorMessage(): ?string
    {
        if (!isset($this->error) || !isset($this->messages[$this->error])) {
            return null;
        }

        return $this->messages[$this->error];
    }

    /**
     * @param string  $error
     * @param mixed[] $parameters
     *
     * @return void
     */
    protected function error(string $error, array $parameters = []): void
    {
        $this->error      = $error;
        $messageTemplates = $this->getMessageTemplates();

        if (!isset($messageTemplates[$this->error])) {
            return;
        }
        $message                      = vsprintf($messageTemplates[$this->error], $parameters);
        $this->messages[$this->error] = $message;
    }

    /**
     * @return string[]
     */
    abstract protected function getMessageTemplates(): array;
}
