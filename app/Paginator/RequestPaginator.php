<?php

namespace App\Paginator;

use App\Eloquent\AbstractDataRequest;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class RequestPaginator
{
    /**
     * @param AbstractDataRequest $dataRequest
     * @param int                 $perPage
     * @param string              $pageName
     * @param null                $page
     *
     * @return LengthAwarePaginator
     */
    public static function paginate(
        AbstractDataRequest $dataRequest,
        $perPage = 15,
        $pageName = 'page',
        $page = null
    ): LengthAwarePaginator {
        $page    = $page ?: Paginator::resolveCurrentPage($pageName);
        $total   = $dataRequest->countWithCover();
        $results = $total ? $dataRequest->forPage($page, $perPage)->get() : [];

        return new LengthAwarePaginator(
            $results, $total, $perPage, $page,
            [
                'path'     => Paginator::resolveCurrentPath(),
                'pageName' => $pageName,
            ]
        );
    }
}
