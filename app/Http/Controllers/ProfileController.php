<?php

namespace App\Http\Controllers;

use App\Http\Resource\EmptyResource;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;
use Vrcci\User\Service\UserService;

class ProfileController extends Controller
{

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var BcryptHasher
     */
    protected $hash;

    /**
     * ProfileController constructor.
     *
     * @param UserService  $userService
     * @param BcryptHasher $hash
     */
    public function __construct(UserService $userService, BcryptHasher $hash)
    {
        $this->userService = $userService;
        $this->hash = $hash;
    }

    public function index()
    {
        $user = $this->userService->getCurrent();

        return view('app/profile/index')->with('user', $user);
    }

    /**
     * @param Request $request
     *
     * @return EmptyResource
     */
    public function updatePassword(Request $request)
    {
        $user = $this->userService->getCurrent();

        $this->validatePassword($request);
        $this->userService->updatePassword($user, $request->post('password'));

        $message = 'Пароль изменен';
        $resource = new EmptyResource();
        $resource->withMessage($message);
        $resource->withRedirect(route('user.profile'));

        return $resource;
    }

    protected function validatePassword(Request $request)
    {
        $request->validate(
            [
                'current_password' => 'required|current_password',
                'password' => 'required|min:6|confirmed',
            ]
        );
    }
}
