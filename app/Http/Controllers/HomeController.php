<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function welcome()
    {
        return View::make('app/home/dashboard');
    }
}
