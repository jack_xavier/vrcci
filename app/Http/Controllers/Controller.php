<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="VRCCI Api",
 * )
 *
 * @OA\SecurityScheme(
 *     type="http",
 *     in="header",
 *     scheme="bearer",
 *     securityScheme="bearerAuth",
 *     bearerFormat="JWT"
 * )
 *
 * @OA\Schema(
 *     schema="CollectionLinks",
 *     @OA\Property(property="first", type="string", description="First page"),
 *     @OA\Property(property="last", type="string", description="Last page"),
 *     @OA\Property(property="prev", type="string", description="Previous page"),
 *     @OA\Property(property="next", type="string", description="Next page"),
 * )
 *
 * @OA\Schema(
 *     schema="CollectionMeta",
 *     @OA\Property(property="currentPage", type="integer", description="Current page"),
 *     @OA\Property(property="from", type="integer", description="From"),
 *     @OA\Property(property="lastPage", type="integer", description="Last page"),
 *     @OA\Property(property="path", type="string", description="Path"),
 *     @OA\Property(property="perPage", type="integer", description="Per page"),
 *     @OA\Property(property="to", type="integer", description="To"),
 *     @OA\Property(property="total", type="integer", description="Total"),
 * )
 **/
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
