<?php

namespace App\Http\Middleware;

use App\Helper\CurrentUserTrait;
use App\Service\RoutePermissionCheck;
use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;

class CheckPermissions
{
    use CurrentUserTrait;

    /**
     * @var RoutePermissionCheck
     */
    protected $permissionService;

    /**
     * @param RoutePermissionCheck $permissionService
     */
    public function __construct(RoutePermissionCheck $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest() || $this->permissionService->isGranted($this->getCurrentUser(), $request->route())) {
            return $next($request);
        }

        return abort(403, 'Access denied');
    }
}
