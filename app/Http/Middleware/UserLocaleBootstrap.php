<?php

namespace App\Http\Middleware;

use App;
use App\Helper\CurrentUserTrait;
use Closure;
use Illuminate\Auth\AuthenticationException;

class UserLocaleBootstrap
{
    use CurrentUserTrait;

    /**
     * @param         $request
     * @param Closure $next
     *
     * @throws AuthenticationException
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (null === $user = $this->getCurrentUser()) {
            return $next($request);
        }

        App::setLocale($user->getOriginal()->language());

        return $next($request);
    }
}