<?php

namespace App\Http\Request;

use Illuminate\Http\Request;

class BaseRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function expectsJson()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function wantsJson()
    {
        return true;
    }
}
