<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SortableRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'sort'           => [
                'nullable',
                'array',
            ],
            'sort.field'     => [
                'required_with:sort.direction',
                'string',
                Rule::in($this->sortableFields()),
            ],
            'sort.direction' => [
                'nullable',
                Rule::in(['asc', 'desc', 'ASC', 'DESC']),
            ],
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function getSortAttributes(): array
    {
        return $this->get('sort', []);
    }

    /**
     * @return array
     */
    protected function sortableFields(): array
    {
        return [];
    }
}
