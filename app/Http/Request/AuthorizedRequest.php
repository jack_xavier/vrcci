<?php

namespace App\Http\Request;

trait AuthorizedRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
