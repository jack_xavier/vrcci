<?php

namespace App\Http\Enum;

class Messages
{
    private static $messages = [
        'presentations.file_limit' => 'Максимальное количество файлов презентаций которое можно загрузить - %s.',
        'presentations.file_amount' => 'Общий объём файлов презентаций не должен превышать %s МБ.',
        'certificates.file_limit' => 'Максимальное количество файлов которое можно загрузить - %s.',
        'certificates.file_amount' => 'Объём файла грамоты/награды не должен превышать %s МБ.',
        'spheres360.file_limit' => 'Максимальное количество файлов Фото 360 которое можно загрузить - %s.',
        'spheres360.file_amount' => 'Объём файла фото 360 не должен превышать %s МБ.',
        'excursions.file_limit' => 'Максимальное количество файлов экскурсий которое можно загрузить - %s.',
        'excursions.file_amount' => 'Объём файла экскурсии не должен превышать %s КБ.',
        'models.file_amount' => 'Объём файла 3D-модели не должен превышать %s МБ.',
        'textures.file_amount' => 'Общий объём файлов текстур не должен превышать %s МБ.',
        'textures.file_limit' => 'Максимальное количество файлов текстур которое можно загрузить - %s.',
    ];

    public static function get($key)
    {
        return self::$messages[$key];
    }
}
