<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'vrcci.companies.list';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('app/auth/login');
    }

    /**
     * @param Request $request
     * @param         $user
     *
     * @return RedirectResponse
     */
    protected function authenticated(Request $request, $user)
    {
        return \Redirect::route($this->redirectTo);
    }

    /**
     * @param Request $request
     *
     * @return Factory|View
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return view('app/auth/login', ['loginFailed' => true]);
    }

    public function redirectPath()
    {
        return route($this->redirectTo);
    }

    protected function loggedOut(Request $request)
    {
        return \Redirect::route('login');
    }
}
