<?php

namespace App\Http\Resource;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class TransformableResourceCollection extends ResourceCollection
{
    /**
     * @param Collection|Paginator $collection
     * @param callable|null        $transformer
     */
    public function __construct($collection, callable $transformer = null)
    {
        $this->transformCollection($collection, $transformer);
        parent::__construct($collection);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    /**
     * @param Collection|Paginator $collection
     * @param callable|null        $transformer
     */
    protected function transformCollection($collection, callable $transformer = null)
    {
        if ($transformer === null) {
            return;
        }
        $collection->transform($transformer);
    }
}
