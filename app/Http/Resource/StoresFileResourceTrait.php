<?php

namespace App\Http\Resource;

use App\Http\Resource\Extractor\DefaultResourceExtractor;
use App\Http\Resource\Extractor\ExtractorInterface;

trait StoresFileResourceTrait
{
    /**
     * @var string[]
     */
    protected $extractable = [
        'guid',
        'url',
        'filename',
    ];

    /**
     * @param ExtractorInterface $extractor
     * @param object             $resource
     *
     * @return mixed[]
     */
    protected function extract(ExtractorInterface $extractor, $resource): array
    {
        if ($extractor instanceof DefaultResourceExtractor) {
            $extractor->properties($this->extractable);
        }

        $extractor->extract($resource);

        return $extractor->toArray();
    }
}
