<?php

namespace App\Http\Resource\Extractor\Strategy;

interface NamingStrategyInterface
{
    /**
     * @param string $property
     *
     * @return string
     */
    public function getName(string $property): string;
}
