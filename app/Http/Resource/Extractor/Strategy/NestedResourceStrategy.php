<?php

namespace App\Http\Resource\Extractor\Strategy;

use App\Http\Resource\ResourcesMap;
use Illuminate\Http\Resources\Json\ResourceCollection;

class NestedResourceStrategy implements StrategyInterface
{
    /**
     * @var ResourcesMap
     */
    protected $resourcesMap;

    /**
     * @param ResourcesMap $resourcesMap
     */
    public function __construct(ResourcesMap $resourcesMap)
    {
        $this->resourcesMap = $resourcesMap;
    }

    /**
     * @param mixed $value
     * @param null  $context
     *
     * @return ResourceCollection|Resource
     */
    public function extract($value, $context = null)
    {
        if (null === $value) {
            return $value;
        }

        if (is_array($value) || $value instanceof \Traversable) {
            return $this->resourcesMap->collection($value);
        }
        return $this->resourcesMap->resource($value);
    }
}
