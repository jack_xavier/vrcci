<?php

namespace App\Http\Resource\Extractor\Strategy;

class MapNamingStrategy implements NamingStrategyInterface
{
    /**
     * @var string[]
     */
    protected $properties = [];

    /**
     * @param string[] $properties
     */
    public function __construct(array $properties)
    {
        $this->properties = $properties;
    }

    /**
     * @param string $property
     *
     * @return string
     */
    public function getName(string $property): string
    {
        return $this->properties[$property] ?? $property;
    }
}
