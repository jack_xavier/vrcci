<?php

namespace App\Http\Resource\Extractor\Strategy;

class SnakeCaseNamingStrategy implements NamingStrategyInterface
{
    /**
     * @param string $property
     *
     * @return string
     */
    public function getName(string $property): string
    {
        return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $property));
    }
}
