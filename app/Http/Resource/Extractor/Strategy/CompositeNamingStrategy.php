<?php

namespace App\Http\Resource\Extractor\Strategy;

class CompositeNamingStrategy implements NamingStrategyInterface
{
    /**
     * @var NamingStrategyInterface[]
     */
    protected $strategies = [];

    /**
     * @param NamingStrategyInterface[] $strategies
     */
    public function __construct(array $strategies)
    {
        foreach ($strategies as $strategy) {
            $this->addStrategy($strategy);
        }
    }

    /**
     * @param string $property
     *
     * @return string
     */
    public function getName(string $property): string
    {
        foreach ($this->strategies as $strategy) {
            $property = $strategy->getName($property);
        }

        return $property;
    }

    /**
     * @param NamingStrategyInterface $strategy
     *
     * @return $this
     */
    public function addStrategy(NamingStrategyInterface $strategy): CompositeNamingStrategy
    {
        $this->strategies[] = $strategy;
        return $this;
    }
}
