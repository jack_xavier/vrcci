<?php

namespace App\Http\Resource\Extractor;

interface PropertiesExtractorInterface extends ExtractorInterface
{
    /**
     * Defines properties to extract from object
     *
     * @param array $properties
     */
    public function properties(array $properties): void;
}
