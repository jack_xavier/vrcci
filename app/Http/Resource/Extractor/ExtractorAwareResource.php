<?php

namespace App\Http\Resource\Extractor;

interface ExtractorAwareResource
{
    /**
     * @param ExtractorInterface $extractor
     *
     * @return void
     */
    public function setExtractor(ExtractorInterface $extractor);
}
