<?php

namespace App\Http\Resource;

use App\Http\Resource\Helper\ResourceBehavioursTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection as BaseResourceCollection;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Collection;

class ResourceCollection extends BaseResourceCollection
{
    use ResourceBehavioursTrait;

    /**
     * @var ResourcesMap
     */
    protected $resourcesMap;

    /**
     * @param ResourcesMap $resourcesMap
     * @param Collection   $resource
     */
    public function __construct(ResourcesMap $resourcesMap, $resource)
    {
        $this->resourcesMap = $resourcesMap;
        $this->resource     = $this->collectResource($resource);
    }

    /**
     * @param Collection $resource
     *
     * @return Collection
     */
    protected function collectResource($resource)
    {
        $resource = $this->normalizeResource($resource);

        $resource->transform(
            function ($model) {
                if ($model instanceof Resource) {
                    return $model;
                }

                return $this->resourcesMap->has($model) ? $this->resourcesMap->resource($model) : $model;
            }
        );

        return parent::collectResource($resource);
    }

    public function resolve($request = null)
    {
        if (!$this->resource instanceof AbstractPaginator) {
            $this->withMeta('total', $this->collection->count());
        }

        return parent::resolve($request);
    }

    public function toArray($request)
    {
        return $this->collection->map(
            function ($resource) use ($request) {
                if ($resource instanceof AbstractExtractableResource) {
                    return $this->doWrap(
                        $resource->toArray($request),
                        $resource->with($request),
                        $resource->additional
                    );
                }

                return $resource->toArray($request);
            }
        )->all();
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function toResponse($request)
    {
        return $this->resource instanceof AbstractPaginator
            ? (new PaginatedResourceResponse($this))->toResponse($request)
            : parent::toResponse($request);
    }

    /**
     * @param mixed $resource
     *
     * @return Collection | AbstractPaginator
     */
    protected function normalizeResource($resource)
    {
        if (!is_callable([$resource, 'transform'])) {
            return Collection::wrap($resource);
        }

        return $resource;
    }
}
