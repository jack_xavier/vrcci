<?php

namespace App\Http\Resource;

use App\Http\Resource\Extractor\ExtractorAwareResource;
use App\Http\Resource\Extractor\ExtractorAwareTrait;
use App\Http\Resource\Extractor\ExtractorInterface;
use App\Http\Resource\Helper\ResourceBehavioursTrait;
use Illuminate\Http\Resources\Json\Resource;

abstract class AbstractExtractableResource extends Resource implements ExtractorAwareResource
{
    use ExtractorAwareTrait,
        ResourceBehavioursTrait;

    /**
     * @var array
     */
    protected $links = [];

    public function toArray($request)
    {
        $data = $this->extractor ? $this->extract($this->extractor, $this->resource) : parent::toArray($request);

        return array_merge($data, $this->wrapLinks());
    }

    /**
     * @param array $links
     *
     * @return void
     */
    public function withLinks(array $links)
    {
        $this->links = $links;
    }

    /**
     * @return array
     */
    public function wrapLinks()
    {
        return empty($this->links) ? [] : ['links' => $this->links];
    }

    /**
     * @param ExtractorInterface $extractor
     * @param object             $resource
     *
     * @return array
     */
    abstract protected function extract(ExtractorInterface $extractor, $resource): array;
}
