<?php

namespace App\Http\Resource;

use Illuminate\Http\Resources\Json\PaginatedResourceResponse as BasePaginatedResourceResponse;
use Illuminate\Support\Arr;

class PaginatedResourceResponse extends BasePaginatedResourceResponse
{
    /**
     * @param mixed $paginated
     *
     * @return mixed
     */
    protected function meta($paginated)
    {
        $metaSource = parent::meta($paginated);

        $meta = [];

        foreach ($metaSource as $key => $item) {
            $meta[camel_case($key)] = $item;
        }

        return $meta;
    }
}
