<?php

namespace App\Twig\Extension\App;

use App\Service\MenuBuilder;
use Illuminate\Auth\AuthenticationException;
use Lavary\Menu\Menu as LavaryMenu;
use Twig_SimpleFunction;
use Vrcci\User\Service\UserService;

class Menu extends \Twig_Extension
{

    /**
     * @var MenuBuilder
     */
    protected $menuBuilder;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * Menu constructor.
     *
     * @param MenuBuilder $menuBuilder
     * @param UserService $userService
     */
    public function __construct(MenuBuilder $menuBuilder, UserService $userService)
    {
        $this->menuBuilder = $menuBuilder;
        $this->userService = $userService;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'App_Extension_App_Menu';
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('menu', [$this, 'getMenu']),
        ];
    }

    /**
     * @param string $name
     *
     * @return LavaryMenu
     */
    public function getMenu(string $name)
    {
        try {
            $user = $this->userService->getCurrent();
        } catch (AuthenticationException $exception) {
            return null;
        }

        return $this->menuBuilder->build($name, $user);
    }
}
