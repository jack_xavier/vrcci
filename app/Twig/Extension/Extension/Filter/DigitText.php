<?php

namespace App\Twig\Extension\Filter;

use TwigBridge\Extension\Loader\Loader;

class DigitText extends Loader
{

    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('digit_text', [$this, 'digitText']),
        ];
    }

    /**
     * @param float $digit
     * @param bool  $currency
     *
     * @return string
     */
    public function digitText($digit, $currency = false): string
    {
        return digit_text($digit, config('app.locale'), $currency);
    }
}
