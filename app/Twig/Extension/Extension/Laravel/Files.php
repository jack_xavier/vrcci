<?php

namespace App\Twig\Extension\Laravel;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig_SimpleFunction;

class Files extends AbstractExtension
{
    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('transform_mb', [$this, 'transformMB']),
        ];
    }

    public function transformMB($number): string
    {
        if ($number / 1024 >= 1) {
            $number = round($number / 1024);
        }

        return $number;
    }
}
