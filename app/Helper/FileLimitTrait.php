<?php

namespace App\Helper;

use App\Exceptions\FilesAmountException;
use App\Exceptions\FilesCountException;
use App\Exceptions\FileUploadingException;
use App\Http\Enum\Messages;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Vrcci\Company\Dto\CompanyTo;

trait FileLimitTrait
{
    /**
     * @param CompanyTo   $companyTo
     * @param Collection  $collection
     * @param FormRequest $request
     * @param string      $keyword
     */
    protected function doValidate(
        CompanyTo $companyTo,
        Collection $collection,
        FormRequest $request,
        string $keyword = '',
        bool $onUpdate = false
    ) {
        $limits = $companyTo->getPreferences()->getByKeyWord($keyword);
        if (empty($limits)) {
            return;
        }

        $this->validateFilesCount($limits['totalLimit'], $collection, $keyword, $onUpdate);

        if (!in_array($keyword, ['presentations', 'textures'])) {
            $this->validateSingleFileAmount($limits['totalAmount'], $request, $keyword);

            return;
        }

        $this->validateFilesAmount($limits['totalAmount'], $request, $collection, $keyword, $onUpdate);
    }

    /**
     * @param int        $totalLimit
     * @param Collection $collection
     * @param string     $keyword
     * @param bool       $onUpdate
     *
     * @return void
     */
    public function validateFilesCount(
        int $totalLimit,
        Collection $collection,
        string $keyword,
        bool $onUpdate = false
    ): void {
        if ($onUpdate) {
            $totalLimit += 1;
        }

        if (count($collection) >= $totalLimit) {
            throw new FilesCountException(
                sprintf(
                    Messages::get(sprintf('%s.file_limit', $keyword)),
                    (int)$totalLimit
                )
            );
        }
    }

    /**
     * @param int         $totalAmount
     * @param FormRequest $request
     * @param string      $keyword
     *
     * @throws FileUploadingException
     *
     * @return void
     */
    public function validateSingleFileAmount(int $totalAmount, FormRequest $request, string $keyword): void
    {
        if ($request->getFileSize() > $totalAmount) {
            throw new FilesAmountException(
                sprintf(
                    Messages::get(sprintf('%s.file_amount', $keyword)),
                    (int)$totalAmount / 1024
                )
            );
        }
    }

    /**
     * @param int         $totalAmount
     * @param int         $totalCount
     * @param FormRequest $request
     * @param string      $keyword
     */
    public function validateRequestFilesArray(
        int $totalAmount,
        int $totalCount,
        FormRequest $request,
        string $keyword
    ): void {
        $attributes = $request->toArray();
        if (!array_key_exists($keyword, $attributes) && !is_array($attributes[$keyword])) {
            return;
        }

        if (count($attributes[$keyword]) > $totalCount) {
            throw new FilesCountException(
                sprintf(
                    Messages::get(sprintf('%s.file_limit', $keyword)),
                    (int)$totalCount
                )
            );
        }

        $filesAmount = 0;
        /** @var UploadedFile $file */
        foreach ($attributes[$keyword] as $file) {
            $filesAmount += $file->getSize();
        }

        $filesAmount = $filesAmount / 1024;
        if ($filesAmount > $totalAmount) {
            throw new FilesAmountException(
                sprintf(
                    Messages::get(sprintf('%s.file_amount', $keyword)),
                    (int)$totalAmount / 1024
                )
            );
        }
    }

    /**
     * @param int         $totalAmount
     * @param FormRequest $request
     * @param Collection  $collection
     *
     * @throws FileUploadingException
     *
     * @return void
     */
    public function validateFilesAmount(
        int $totalAmount,
        FormRequest $request,
        Collection $collection,
        string $keyword,
        bool $onUpdate = false
    ): void {
        $totalSize = 0;

        foreach ($collection as $item) {
            $totalSize += $item->getFileSize();
        }

        if ($onUpdate && $totalSize >= $totalAmount) {
            throw new FilesAmountException(
                sprintf(
                    Messages::get(sprintf('%s.file_amount', $keyword)),
                    (int)$totalAmount / 1024
                )
            );
        }

        if ($totalSize + $request->getFileSize() > $totalAmount) {
            throw new FilesAmountException(
                sprintf(
                    Messages::get(sprintf('%s.file_amount', $keyword)),
                    (int)$totalAmount / 1024
                )
            );
        }
    }
}
