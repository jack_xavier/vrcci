<?php

namespace App\Helper;

use Vrcci\User\Dto\UserTo;
use Vrcci\User\Service\UserService;

trait CurrentUserTrait
{
    /**
     * @return UserTo
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function getCurrentUser(): UserTo
    {
        return resolve(UserService::class)->getCurrent();
    }

    /**
     * @param UserTo   $user
     * @param string[] $roles
     *
     * @return bool
     */
    protected function hasAccess(UserTo $user, array $roles = []): bool
    {
        if ($user->getGuid() == $this->getCurrentUser()->getGuid()) {
            return true;
        }

        $original = $this->getCurrentUser()->getOriginal();
        foreach ($roles as $role) {
            if ($original->hasRole($role)) {
                return true;
            }
        }

        return false;
    }
}
