<?php

namespace App\Exceptions;

use LogicException;

class FileUploadingException extends LogicException
{

}
