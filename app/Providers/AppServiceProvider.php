<?php

namespace App\Providers;

use App\Http\Resource\ResourcesMap;
use App\Service\IdentityMap;
use App\Service\RouteFeatureCheck;
use App\Service\RoutePermissionCheck;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('extensions', function ($attribute, $value, $parameters) {
            return in_array($value->getClientOriginalExtension(), $parameters);
        },'File :attribute must be a type of :values' );

        Validator::replacer('extensions', function($message, $attribute, $rule, $parameters) {
            return str_replace([':attribute', ':values'], [$attribute, implode(',', $parameters)], $message);
        });

        Validator::extend(
            'max_uploaded_file_size',
            function ($attribute, $value, $parameters, $validator) {
                $total_size = array_reduce(
                    $value,
                    function ($sum, $item) {
                        $sum += filesize($item->path());

                        return $sum;
                    }
                );

                return $total_size < $parameters[0] * 1024;
            }
        );

        $this->app->when(ResourcesMap::class)
                  ->needs('$config')
                  ->give(config('http.resources.map'));

        $this->app->when(RoutePermissionCheck::class)
                  ->needs('$rules')
                  ->give(config('permissions'));

        $this->app->when(ResourcesMap::class)
                  ->needs('$config')
                  ->give(config('http.resources.map'));

        $this->app->singleton(IdentityMap::class, IdentityMap::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(AutoMapperProvider::class);
        $this->app->register(ModuleAliasServiceProvider::class);
    }
}
