<?php

namespace App\Providers;

use App\Auth\Provider\Vrcci;
use Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Vrcci\Auth\Common\UserProvider\VrcciUserProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('vrcci', function ($app, array $config) {
            return new VrcciUserProvider($app['hash'], $config['model']);
        });
    }
}
