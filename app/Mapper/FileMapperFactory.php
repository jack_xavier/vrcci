<?php

namespace App\Mapper;

use App\Entity\File;
use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\AutoMapper\Operation\MapToRelation;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\Company\Dto\CompanyTo;

class FileMapperFactory implements MappingFactoryInterface
{
    /**
     * @var string
     */
    protected $entityClassName = '';

    /**
     * @var string
     */
    protected $dtoClassName = '';

    /**
     * @param AutoMapperConfig $config
     *
     * @return void
     */
    public function registerMapping(AutoMapperConfig $config): void
    {
        $mappings = $config->registerMapping($this->entityClassName, $this->dtoClassName);
        $mappings->forMember('originalModel', new MapOriginal())
            ->forMember('company', new MapToRelation(CompanyTo::class))
            ->forMember(
                'url',
                static function (File $file): string {
                    return $file->getFileUrl();
                }
            )->forMember(
                'filename',
                static function (File $file): string {
                    return $file->original_filename;
                }
            );

        if (!empty($this->additionalMappings())) {
            foreach ($this->additionalMappings() as $key => $callback) {
                $mappings->forMember($key, $callback);
            }
        }
    }

    public function additionalMappings(): array
    {
        return [];
    }
}
