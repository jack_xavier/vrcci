<?php

namespace App\Mapper\AutoMapper\Operation;

class MapOriginal
{
    /**
     * @param mixed $original
     *
     * @return mixed
     */
    public function __invoke($original)
    {
        return $original;
    }
}
