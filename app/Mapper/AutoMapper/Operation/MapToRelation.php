<?php

namespace App\Mapper\AutoMapper\Operation;

use AutoMapperPlus\MappingOperation\Implementations\MapTo;
use Illuminate\Database\Eloquent\Model;

class MapToRelation extends MapTo
{
    /**
     * @var string|null
     */
    private $fromProperty;

    /**
     * @param string      $destinationClass
     * @param string|null $fromProperty
     */
    public function __construct(string $destinationClass, ?string $fromProperty = null)
    {
        parent::__construct($destinationClass);

        $this->fromProperty = $fromProperty;
    }

    /**
     * {@inheritdoc}
     */
    protected function canMapProperty(string $propertyName, $source): bool
    {
        if (!$source instanceof Model) {
            return false;
        }

        return null !== $this->getSourceValue($source, $propertyName);
    }

    /**
     * {@inheritdoc}
     */
    protected function getSourceValue($source, string $propertyName)
    {
        /** @var Model $model */
        $model = $source;
        $path  = $this->getRelationPath($propertyName);

        foreach ($path as $relation) {
            if (!$model->relationLoaded($relation) || null === $model->{$relation}) {
                return null;
            }

            $model = $model->{$relation};
        }

        return is_iterable($model)
            ? $this->mapper->mapMultiple($model, $this->getDestinationClass())
            : $this->mapper->map($model, $this->getDestinationClass());
    }

    /**
     * @param string $propertyName
     *
     * @return string[]
     */
    private function getRelationPath(string $propertyName): array
    {
        if (empty($this->fromProperty)) {
            return [$propertyName];
        }

        return explode('.', $this->fromProperty);
    }
}
