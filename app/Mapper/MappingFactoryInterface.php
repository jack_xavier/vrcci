<?php

namespace App\Mapper;

use AutoMapperPlus\Configuration\AutoMapperConfig;

interface MappingFactoryInterface
{
    public function registerMapping(AutoMapperConfig $config);
}
