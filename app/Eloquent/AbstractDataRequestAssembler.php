<?php

namespace App\Eloquent;

use InvalidArgumentException;

abstract class AbstractDataRequestAssembler
{
    /**
     * @param AbstractDataRequest $dataRequest
     * @param mixed[]             $params
     *
     * @return AbstractDataRequest
     */
    public static function assembleDataRequest(AbstractDataRequest $dataRequest, array $params): AbstractDataRequest
    {
        return (new static())->assemble($dataRequest, $params);
    }

    /**
     * @param AbstractDataRequest $dataRequest
     * @param mixed[]             $params
     *
     * @return AbstractDataRequest
     */
    public function assemble(AbstractDataRequest $dataRequest, array $params): AbstractDataRequest
    {
        $map = $this->getStrategyMap();

        foreach ($params as $paramName => $value) {
            if (array_key_exists($paramName, $map) && is_callable($map[$paramName])) {
                $map[$paramName]($dataRequest, $value, $params);
            }
        }

        return $dataRequest;
    }

    /**
     * @throws InvalidArgumentException
     *
     * @return callable[]
     */
    abstract protected function getStrategyMap(): array;
}
