<?php

namespace App\Enum;

use ReflectionClass;

abstract class AbstractReflectionEnum
{
    /**
     * @var ReflectionClass[]
     */
    private static $reflectionMap = [];

    /**
     * @var string[][]
     */
    private static $constantsMap = [];

    /**
     * @return ReflectionClass
     */
    private static function getReflection(): ReflectionClass
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return self::$reflectionMap[static::class] = self::$reflectionMap[static::class]
            ?? new ReflectionClass(static::class);
    }

    /**
     * @return mixed[]
     */
    private static function getPublicConstants(): array
    {
        if (!isset(self::$constantsMap[static::class])) {
            self::$constantsMap[static::class] = [];

            foreach (static::getReflection()->getReflectionConstants() as $constant) {
                if (!$constant->isPublic()) {
                    continue;
                }

                self::$constantsMap[static::class][$constant->getName()] = $constant->getValue();
            }
        }

        return self::$constantsMap[static::class];
    }

    /**
     * Returns values of all public constants which keys match to `fnmatch` filter
     *
     * @param string $filter
     *
     * @return mixed[]
     */
    protected static function getByKeysMatch(string $filter = '*'): array
    {
        $constants = static::getPublicConstants();

        if ('*' === $filter) {
            return array_values($constants);
        }

        $filtered = [];

        foreach ($constants as $key => $constant) {
            if (fnmatch($filter, $key, FNM_CASEFOLD | FNM_NOESCAPE)) {
                $filtered[] = $constant;
            }
        }

        return $filtered;
    }

    /**
     * Returns values of all public constants
     *
     * @return mixed[]
     */
    public static function getAvailable(): array
    {
        return static::getByKeysMatch();
    }
}
