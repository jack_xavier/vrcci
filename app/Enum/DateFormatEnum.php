<?php

namespace App\Enum;

use Carbon\Carbon;
use DateTime;

class DateFormatEnum
{
    public const FE_IO_DATETIME = DateTime::ISO8601;
    public const FE_DB_DATETIME = Carbon::DEFAULT_TO_STRING_FORMAT;
}
