<?php

namespace App\Console\Command;

use App\Exceptions\User\NotUniqueUserException;
use App\Service\PasswordGenerator;
use Illuminate\Console\Command;
use Vrcci\Auth\Common\Enum\AccessEnum;
use Vrcci\User\Dto\Attributes\UserAttributesVo;
use Vrcci\User\Service\UserService;

class CreateAdminCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'user:create:admin {--email= : New user Email} {--name= : New user name} {--password= : New user password}';

    /**
     * @var string
     */
    protected $description = 'Creates admin user with given Email, Username and Password';

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var PasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @param UserService       $userService
     * @param PasswordGenerator $passwordGenerator
     */
    public function __construct(UserService $userService, PasswordGenerator $passwordGenerator)
    {
        $this->userService       = $userService;
        $this->passwordGenerator = $passwordGenerator;

        parent::__construct();
    }

    /**
     * @return int
     */
    public function handle(): int
    {
        $email    = $this->option('email') ?: $this->ask('Please define the Email');
        $password = $this->option('password') ?: $this->ask('What to defined the password? (Enter to skip)');

        if (empty($password)) {
            $password = $this->passwordGenerator->generate(8);
        }

        try {
            $this->userService->create(
                UserAttributesVo::fromArray(compact('email', 'password')),
                AccessEnum::R_ADMIN
            );
        } catch (NotUniqueUserException $ex) {
            $this->error($ex->getMessage());

            return 1;
        }

        $this->info('User created successfully');
        $this->comment(sprintf('New user password: %s', $password));

        return 0;
    }
}
