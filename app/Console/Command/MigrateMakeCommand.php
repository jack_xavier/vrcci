<?php

namespace App\Console\Command;

use Illuminate\Database\Console\Migrations\MigrateMakeCommand as BaseCommand;
use Illuminate\Support\Composer;

class MigrateMakeCommand extends BaseCommand
{
    /**
     * @var string
     */
    protected $signature = 'make:migration {name : The name of the migration.}
        {--module= : Module to make migration for.}
        {--create= : The table to be created.}
        {--table= : The table to migrate.}
        {--path= : The location where the migration file should be created.}
        {--realpath : Indicate any provided migration file paths are pre-resolved absolute paths.}';

    /**
     * @param MigrationCreator $creator
     * @param Composer         $composer
     */
    public function __construct(MigrationCreator $creator, Composer $composer)
    {
        parent::__construct($creator, $composer);
    }

    /**
     * @return void
     */
    public function handle(): void
    {
        if ($this->input->getOption('module') && $this->input->getOption('path')) {
            $this->warn('You specified both "module" and "path" options. Path option has been ignored' . PHP_EOL);
        }

        $module = $this->input->getOption('module');

        if ($module) {
            $path = sprintf('module/%s/database/migrations', str_replace('\\', '/', $module));

            $this->input->setOption('path', $path);
        }

        parent::handle();
    }
}

