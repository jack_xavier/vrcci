.DEFAULT_GOAL := help
.PHONY: help init update docs up down logs test-down test-run watch cs fix-perms

help: ## Show this help (default)
	@echo "Usage: make [command] [args=\"\"]"
	@echo
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

init: ## Initialize whole local development environment
	cp -n .env.dist .env
	$(MAKE) up
	.docker/update.sh
	.docker/artisan.sh storage:link
	.docker/artisan.sh jwt:keys
	.docker/artisan.sh user:create:admin

update: ## Updates project after changes
	$(MAKE) up
	.docker/update.sh
	@echo -e "\E[1;31mDon't forget to sync .env file"

docs: ## Generage swagger docs
	.docker/artisan.sh vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
	.docker/artisan.sh l5-swagger:generate
	@echo "Documentation available on /api/documentation page"

up: ## Start docker environment
	docker-compose up -d

down: ## Shutdown docker environment
	docker-compose down --remove-orphans

restart: down up ## Stop and start docker environment

logs: ## Show logs for docker env
	docker-compose logs -f $(args)

test-up: up ## Start docker environment for test
	docker-compose -f docker-compose-test.yml up -d

test-down: ## Shutdown docker environment for test
	-docker-compose -f docker-compose-test.yml down

test: test-up ## Start and init test environment and run tests (recieves `args`)
	.docker/codecept.sh build
	.docker/artisan.sh config:clear
	.docker/artisan.sh migrate:fresh --seed --env=testing
	$(MAKE) test-run run args="$(args)"

test-run: ## Just run tests without initialization (recieves `args`)
	.docker/codecept.sh run $(args)

test-failed: ## Run previously failed tests (recieves `args`)
	$(MAKE) test-run args="$(args) -g failed"

watch: ## Run webpack in server mode
	@sed --in-place=.back 's/"proxy": ".*",/"proxy": "http:\/\/nginx\/",/g' resources/client/package.json
	.docker/build-npm.sh start || true
	@mv -f resources/client/package.json.back resources/client/package.json

cs: ## Run PHP CodeSniffer with only edited files
	docker-compose exec app php ./vendor/bin/phpcs --report=gitblame `git ls-files -om --exclude-standard | grep "\.php$$"`

fix-perms: ## Fix typical linux permission issues
	sudo chown `id -u`:`id -g` -R -c resources/client/node_modules vendor
