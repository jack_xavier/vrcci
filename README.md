## Local setup

This project uses [Docker](https://docs.docker.com/install/) 
and [Docker Compose](https://docs.docker.com/compose/install/). 
Ensure you have both installed.

#### Setup steps

Run `make init` once after you clone project.

This will:

* Copy `.env.dist` file into `.env`. (Update parameters if needed) 
* Deploy all necessary containers to run application.
* Install all composer dependencies, run migrations, clear env and update Laravel plugin assets.
* Link public storage.
* Generate JWT keys.
* And offer to create new user.

To shutdown application and deployed environment run `make down`
within project's root directory. It stops test environment as well.

To start application again (without initialisation) use `make up` command.

After setup is finished your application can be reached from the localhost: 
[http://localhost:8011] - the main backend application entrypoint.

To start frontend server you need access to front repository and make steps described there.
 
## Update application

After pulling new changes (or changing branch) from git you should run `make update`.

Don't forget to update `.env` file if `.env.dist` was changed.

## Useful scripts

`Makefile` has some useful commands. 
To list all of them and see short documentation just run `make` or `make help`

`.docker` directory also contains some useful scripts:

**Aggregated scripts:**

- `build-app.sh` will build all the backend deps and run migrations.
- `build-npm.sh` will update node modules and rebuild assets. You can pass second param to specify yarn job. `build` is uses by default.
- `update-ide-plugin.sh` will update meta and helpers for Laravel plugin.  

**Proxy scripts:**

- `composer.sh` Proxy to `composer` executable, you can call with any valid composer arguments and options.
- `artisan.sh` Proxy to Laravel's `artisan` executable, you can call with any valid artisan arguments and options.
- `bash.sh` Runs bash cli inside `app` container.

**For Linux users:**

On Linux scripts run in current user mode inside containers (not root).

This is done in order to solve problems with permissions.

If you need to run scripts in root mode you must use `ROOT=true` before script like this:

`ROOT=true .docker/artisan.sh`

To fix some permission issues you can run `make fix-perms`.
It requires `sudo` access and will request your user password.  

## Module management
Laravel is not providing a module management feature by default. 
To utilize advantages of modulear architecture we created a 
simple module manager (`App\Module\ModuleManager`).

To add a new custom module you need to create directory under `./module` directory (see autoloading section below)
and place a `[Namespace]\Module` class in the root. We recommend tou to use `src` for code sources and other folders for misc.

`Module` class can provide several interfaces called `features`. See `App\\Module\\Feature` namespace for reference.

Also each module should be an instance of Laravel's `ServiceProvider` to be able 
for custom `register` and `boot` actions.

After you have created your new module register it within `./config/modules.php` config.
Registering module is the same as for `ZF2`-like projects. Order of modules within config does matter. 

#### Modules autoloading

To have your modules structured please settle them into `./module` directory only. 
Each module MUST reside in it's own folder inside the `./module`.

Each module should be `PSR-4` compliant and be register at `./composer.json` file within `psr-4` section. 
Once you have added a new module into `./composer.json` file 
get autoloading files generated via command `.docker/composer.sh dump-autoload`. 

## Laravel plugin

To have all the autocompletion advantages with Laravel facades & models you need Laravel plugin installed.

**Installation:**

- Go to Plugins settings
- Search for `Laravel Plugin`
- Install it with PHPStorm and reload your IDE

**Usage:**

Run `.docker/update-ide-plugin.sh` (Runs by default with `make init` and `make update`)

## Application debugging with XDebug

In order to use XDebug setup within app container you should have Docker version `>=18.03-ce` 
because of special hosts forwarding variable `host.docker.internal` become available only with this version.

#### PHPStorm IDE configuration:

**Remote server:**

- Host: `localhost`
- Port: `8011`
- Path mappings: `[local project root] > /var/www/app`

**Debug configuration:**

- PHP Web Page
- Start URL: `/`
- Server: *choose server defined previously*

Now you are all set and ready to debug application as usual.

## Direct database access

Access to the database can be obtained through the standard DB connection on `localhost` at port `15432`

## Swagger

Run `make docs` to generate/regenerate Swagger documentation.

UI available on page [http://localhost:8011/api/documentation]

## Code style

**Installing for PhpStorm: (^2018.2)**
1. Make sure that PHP CodeSniffer plugin installed and enabled in IDE
2. Set path to phpcs that is located in vendor/bin. 

    2.1 Go to Settings -> Languages and Frameworks -> PHP -> Code Sniffer
    
    2.2 In option `Configuration` select `Local` then path to `vendor/bin/phpcs` via "dots" button
3. Set path to project's phpcs.xml.

    3.1 Go to Settings -> Editor -> Inspections -> PHP -> Quality Tools -> PHP Code Sniffer validation 
    
    3.2 In option `Coding standard` select `Custom` then path to `phpcs.xml` via "dots" button


## Testing

**Run tests:**

Run `make test` to init test environment run all tests.

Run `make test-run` to run tests without test environment initialisation to save some time.

Run `make test-down` to shutdown test environment.

You can set `args` parameter in `test` and `test-run` to pass arguments to `codeception`. Examples: `make test args="-g failed"`, `make test args="unit"`
