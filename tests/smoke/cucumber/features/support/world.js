const webdriver = require('selenium-webdriver');
const {setWorldConstructor, setDefaultTimeout} = require('cucumber');

function CustomWorld() {
    this.driver = new webdriver.Builder()
        .forBrowser('chrome')
      // .usingServer('http://localhost:4444/wd/hub')
      .build();

    // Returns a promise that resolves to the element
    this.waitForElement = async function(locator) {
        const el = await this.driver.wait(webdriver.until.elementLocated(locator));
        await this.driver.wait( webdriver.until.elementIsVisible(el), 10000, 'element is still disabled')
        return el;
    }
};

setDefaultTimeout(60 * 1000);
setWorldConstructor(CustomWorld);