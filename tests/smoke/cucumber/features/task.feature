Feature: Smoke test #2, add/update/complete a task

  Scenario: Add a new task as manager or administrator
    Given login on server http://staging.zelos-app.com in web-app with login admin@test.com and password 111111
    When go to Task Management tab
    And press button "Add task"
    And enter in the Title field task title "Task for Smoke test"
    And press collapse area "Advanced"
    And turn on "Assignment needs manager confirmation" toggle
    And press button "Save"
    Then task with title "Task for Smoke test" is displaying in task table

  Scenario: Complete the task with title "Task for Smoke test" as regular user "TestUser1"
    Given login on server http://staging.zelos-app.com in web-app with login TestUser1@test.com and password 123456
    When press to Task menu
    And press to tile, wich named "Task for Smoke test"
    And press to button "Assign to me"
    Then displaying status "Waiting for assignment approval" in status string

  Scenario: Approve assignment of "Task for Smoke test"
    Given login on server http://staging.zelos-app.com in web-app with login admin@test.com and password 111111
    When go to Task Management tab
    And press to title of task, wich named "Task for Smoke test"
    And go to Users tab
    And press to "Approve assignment" for this regular user
    Then displaying status "Assignment approved" for this regular user


