const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^press button "Save"$/, async function () {
  // Write code here that turns the phrase above into concrete actions
  const saveBtn = await this.waitForElement(By.css('#root > div > div > div > div > div > div.ant-card-body > form > div:nth-child(6) > div > div > span > button'));
  await saveBtn.isDisplayed();
  await saveBtn.click();
});