const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^press button "Add task"$/, async function () {
  const addTaskBtn = await this.waitForElement(By.css('#root > div > div > div > div > div.ant-row-flex.ant-row-flex-center.ant-row-flex-middle.data-table__content-row > div.data-table__add-control.ant-col-xs-11 > a:nth-child(1)'));
  await addTaskBtn.isDisplayed();
  await addTaskBtn.click();
});