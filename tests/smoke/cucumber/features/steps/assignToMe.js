const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^press to button "Assign to me"$/, async function () {
  const assignBtn = await this.waitForElement(By.xpath('//*[@id="root"]/div/div/div/div/div[2]/div[2]/div/button'));
  await assignBtn.isDisplayed();
  await assignBtn.click();
});