const {By} = require('selenium-webdriver');
const {Then} = require('cucumber');

Then(/^displaying status "Waiting for assignment approval" in status string$/, async function () {

  await this.waitForElement(By.xpath('//span[contains(text(),\'Waiting for assignment approval\')]'));
});