const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^press to "Approve assignment" for this regular user$/, async function () {
  // Write code here that turns the phrase above into concrete actions
  //find and scroll to cell with  "Approve assignment" and "Revoke assignment" elements
  const actionCell = await this.waitForElement(By.css('#root > div > div > div > div > div.ant-tabs-content.ant-tabs-content-animated > div.ant-tabs-tabpane.ant-tabs-tabpane-active > div > div > div > div > div > div > div > div > div > table > tbody > tr > td:nth-child(5)'));
  await this.driver.sleep(5000); //TODO try to fine more clear way do that #MTF-407
  await this.driver.executeScript("arguments[0].scrollIntoView(true)", actionCell);

  //find and click to "Approve assignment" element in Action cell
  const approveAssignment = await this.waitForElement(By.xpath('//*[@id="root"]/div/div/div/div/div[2]/div[2]/div/div/div/div/div/div/div/div/div/table/tbody/tr/td[5]/div/span[1]/a'));
  await this.driver.sleep(5000); //TODO try to fine more clear way do that #MTF-407
  await approveAssignment.click();
});