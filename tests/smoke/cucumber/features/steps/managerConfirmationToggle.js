const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^turn on "Assignment needs manager confirmation" toggle$/, async function () {
  // Write code here that turns the phrase above into concrete actions
  const assignmentToggle = await this.waitForElement(By.id('assignmentApproveNeeded'));
  await assignmentToggle.isDisplayed();
  await assignmentToggle.click();
});