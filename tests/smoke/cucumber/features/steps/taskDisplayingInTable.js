const {By} = require('selenium-webdriver');
const {Then} = require('cucumber');

Then(/^task with title "Task for Smoke test" is displaying in task table$/, async function () {
  // Write code here that turns the phrase above into concrete actions
  const taskForSmokeTest = await this.waitForElement(By.linkText('Task for Smoke test'));
  await taskForSmokeTest.isDisplayed();
});