const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^press collapse area "Advanced"$/, async function () {
  // Write code here that turns the phrase above into concrete actions
  const advCollapseAria = await this.waitForElement(By.css('#root > div > div > div > div > div > div.ant-card-body > form > div.ant-collapse.ant-collapse-borderless.collapse > div:nth-child(1) > div.ant-collapse-header'));
  await advCollapseAria.isDisplayed();
  await advCollapseAria.click();
});