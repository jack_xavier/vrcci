const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^press to title of task, wich named "Task for Smoke test"$/, async function () {
  const taskName = await this.waitForElement(By.linkText('Task for Smoke test'));
  await this.driver.sleep(5000); //TODO try to fine more clear way do that (wait until modal spinner will hide )
  await taskName.click();
});