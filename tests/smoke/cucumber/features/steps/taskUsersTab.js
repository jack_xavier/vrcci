const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^go to Users tab$/, async function () {
  // Write code here that turns the phrase above into concrete actions
const usersTab = await this.waitForElement(By.xpath('//*[@id="root"]/div/div/div/div/div[1]/div/div/div/div/div[1]/div[2]'));
await usersTab.isDisplayed();
await usersTab.click();
});