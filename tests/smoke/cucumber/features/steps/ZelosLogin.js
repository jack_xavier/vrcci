const {By} = require('selenium-webdriver');
const {Given, When, Then} = require('cucumber');

Given(/^open zelos\-app staging server$/, async function (){
    this.driver.get('http://staging.zelos-app.com/#/login');
});
When(/^press button Login using password$/, async function() {
    const collapseArea = await this.waitForElement(By.className('ant-collapse-header'));
    collapseArea.click();
});
When(/^enter in the email field admin@test\.com$/, async function () {
    const emailField = await this.waitForElement(By.id('email'));
    await emailField.click();
    await emailField.sendKeys('admin@test.com');
});
When(/^enter in the password field current password$/, async function () {
    const passwdField = await this.waitForElement(By.id('password'));
    await passwdField.click();
    await passwdField.sendKeys('111111');
});
When(/^press button Log in$/, async function () {
    const loginBtn = await this.driver.findElement(By.css('#root > div > div > div > div > div > div > div.ant-card-body > div.ant-collapse > div > div.ant-collapse-content.ant-collapse-content-active > div > form > div:nth-child(3) > div > div > span > button'));
    loginBtn.click();
});
Then(/^element Dashboard should exist$/, async function () {
    await this.waitForElement(By.className('data-table__title data-table__title--dashboard'));
});