const {By, until} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^enter in the Title field task title "(.*)"$/, async function (title) {
  const titleField = await this.waitForElement(By.id('name'));
  await this.driver.wait(until.elementIsEnabled(titleField), 10000, 'element is still disabled');
  await titleField.click();
  await titleField.sendKeys(title);
});