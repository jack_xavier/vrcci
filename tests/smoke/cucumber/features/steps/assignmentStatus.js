const {By} = require('selenium-webdriver');
const {Then} = require('cucumber');

Then(/^displaying status "Assignment approved" for this regular user$/, async function () {
  await this.waitForElement(By.xpath('//td[contains(text(),\'approved\')]'));
});