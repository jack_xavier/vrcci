const {By} = require('selenium-webdriver');
const {Given} = require('cucumber');

Given(/^login on server (.*) in web-app with login (.*) and password (.*)$/, async function (server, login, password) {
  // Write code here that turns the phrase above into concrete actions
  this.driver.get(`${server}/#/login`);
  const collapseArea = await this.waitForElement(By.className('ant-collapse-header'));
  collapseArea.click();

  const emailField = await this.waitForElement(By.id('email'));
  await emailField.click();
  await emailField.sendKeys(login);

  const passwdField = await this.waitForElement(By.id('password'));
  await passwdField.click();
  await passwdField.sendKeys(password);

  const loginBtn = await this.waitForElement(By.className('ant-btn ant-btn-primary'));
  await loginBtn.isDisplayed();
  await loginBtn.click();

  const dashboard = await this.waitForElement(By.className('data-table__title data-table__title--dashboard'));
  await dashboard.isDisplayed();
});
