const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^press to tile, wich named "Task for Smoke test"$/, async function () {
  // Write code here that turns the phrase above into concrete actions
  const taskTile = await this.waitForElement(By.xpath('//*[@id="root"]/div/div/div/div/div/div[2]/div[1]/div/div/div/div[1]/div[1]'));
  await taskTile.isDisplayed();
  await taskTile.click();
});