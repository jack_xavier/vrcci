const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^press to Task menu$/, async function () {
  // Write code here that turns the phrase above into concrete actions
const myTaskMenu = await this.waitForElement(By.css('#root > div > ul > li:nth-child(4) > div'));
await myTaskMenu.isDisplayed();
await myTaskMenu.click();
});