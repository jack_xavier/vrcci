const {By} = require('selenium-webdriver');
const {When} = require('cucumber');

When(/^go to Task Management tab$/, async function () {
  // Write code here that turns the phrase above into concrete actions
  const  taskMenu = await this.waitForElement(By.xpath('//*[@id="root"]/div/ul/li[4]/div'));
  await taskMenu.isDisplayed();
  await taskMenu.click();
  const taskManageMenu = await this.waitForElement(By.linkText('Task Management'));
  await taskManageMenu.isDisplayed();
  await taskManageMenu.click();
});
