Feature: Smoke test #1, Log in to zelos-app staging server

  Scenario: go through the service to dashboards
    Given open zelos-app staging server
    When press button Login using password
    And enter in the email field admin@test.com
    And enter in the password field current password
    And press button Log in
    Then element Dashboard should exist