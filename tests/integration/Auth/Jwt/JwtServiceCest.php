<?php

namespace Tests\Integration\Auth\Jwt;

use Carbon\Carbon;
use IntegrationTester;
use Vrcci\Auth\Jwt\Dto\JwtPairTo;
use Vrcci\Auth\Jwt\Enum\JwtStatusCodeEnum;
use Vrcci\Auth\Jwt\Enum\JwtTypeEnum;
use Vrcci\Auth\Jwt\Exception\JwtAuthenticationException;
use Vrcci\Auth\Jwt\Service\Config\Configuration;
use Vrcci\Auth\Jwt\Service\Config\FileKey;
use Vrcci\Auth\Jwt\Service\JwtTokenService;
use Vrcci\User\Entity\User;

class JwtServiceCest
{
    /**
     * @var JwtTokenService
     */
    private $service;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @param IntegrationTester $I
     *
     * @return void
     */
    public function _before(IntegrationTester $I): void
    {
        $this->configuration = new Configuration(
            new FileKey(__DIR__ . '/keys/jwt-public.key'),
            new FileKey(__DIR__ . '/keys/jwt-private.key')
        );

        $this->configuration->setAccessTokenLifetime(10);
        $this->configuration->setRefreshTokenLifetime(20);

        $this->service = new JwtTokenService($this->configuration);

        $this->user = factory(User::class)->create();
    }

    /**
     * @param IntegrationTester $I
     *
     * @return void
     */
    public function basic(IntegrationTester $I): void
    {
        $userId = $this->user->id;

        $pair = $this->service->generatePair($userId);

        $I->assertInstanceOf(JwtPairTo::class, $pair);

        $access  = $pair->getAccessToken();
        $refresh = $pair->getRefreshToken();

        $I->assertEquals(JwtTypeEnum::ACCESS, $access->getType());
        $I->assertEquals(JwtTypeEnum::REFRESH, $refresh->getType());

        $I->assertEquals($userId, $access->getUserId());
        $I->assertEquals($userId, $refresh->getUserId());

        $I->assertEquals(
            $access->getId(),
            $refresh->getId()
        );

        $I->assertGreaterThan($access->getExpiredTimestamp(), $refresh->getExpiredTimestamp());
        $I->assertEquals($access->getIssuedTimestamp(), $refresh->getIssuedTimestamp());

        $I->assertEquals(10, $access->getExpiredTimestamp() - $access->getIssuedTimestamp());
        $I->assertEquals(20, $refresh->getExpiredTimestamp() - $refresh->getIssuedTimestamp());

        $I->assertNotEquals(
            (string)$access->getToken(),
            (string)$refresh->getToken()
        );
    }

    /**
     * @param IntegrationTester $I
     *
     * @return void
     */
    public function tokensExpired(IntegrationTester $I): void
    {
        $now = Carbon::now();
        Carbon::setTestNow($now);

        $userId = $this->user->id;

        $pair = $this->service->generatePair($userId);

        Carbon::setTestNow($now->copy()->addSeconds(10));
        $this->service->verify($pair->getAccessToken());

        Carbon::setTestNow($now->copy()->addSeconds(20));
        $this->service->verify($pair->getRefreshToken(), JwtTypeEnum::REFRESH);

        Carbon::setTestNow($now->copy()->addSeconds(11));
        $I->expectThrowable(
            JwtAuthenticationException::forCode(JwtStatusCodeEnum::EXPIRED),
            function () use ($pair): void {
                $this->service->verify($pair->getAccessToken());
            }
        );

        Carbon::setTestNow($now->copy()->addSeconds(21));
        $I->expectThrowable(
            JwtAuthenticationException::forCode(JwtStatusCodeEnum::EXPIRED),
            function () use ($pair): void {
                $this->service->verify($pair->getRefreshToken(), JwtTypeEnum::REFRESH);
            }
        );

        Carbon::setTestNow();
    }

    /**
     * @param IntegrationTester $I
     *
     * @return void
     */
    public function wrongType(IntegrationTester $I): void
    {
        $userId = $this->user->id;

        $pair = $this->service->generatePair($userId);

        $this->service->verify($pair->getRefreshToken(), JwtTypeEnum::REFRESH);
        $this->service->verify($pair->getAccessToken(), JwtTypeEnum::ACCESS);

        $I->expectThrowable(
            JwtAuthenticationException::forCode(JwtStatusCodeEnum::WRONG_TYPE),
            function () use ($pair): void {
                $this->service->verify($pair->getRefreshToken(), JwtTypeEnum::ACCESS);
            }
        );

        $I->expectThrowable(
            JwtAuthenticationException::forCode(JwtStatusCodeEnum::WRONG_TYPE),
            function () use ($pair): void {
                $this->service->verify($pair->getAccessToken(), JwtTypeEnum::REFRESH);
            }
        );
    }

    /**
     * @param IntegrationTester $I
     *
     * @return void
     */
    public function keysChanged(IntegrationTester $I): void
    {
        $configuration = new Configuration(
            new FileKey(__DIR__ . '/keys/jwt-public-2.key'),
            new FileKey(__DIR__ . '/keys/jwt-private-2.key')
        );

        $service = new JwtTokenService($configuration);

        $pair = $this->service->generatePair($this->user->id);

        $I->expectThrowable(
            JwtAuthenticationException::forCode(JwtStatusCodeEnum::SIGN_PROBLEMS),
            function () use ($service, $pair): void {
                $service->verify($pair->getAccessToken());
            }
        );

        $I->expectThrowable(
            JwtAuthenticationException::forCode(JwtStatusCodeEnum::SIGN_PROBLEMS),
            function () use ($service, $pair): void {
                $service->verify($pair->getRefreshToken(), JwtTypeEnum::REFRESH);
            }
        );
    }

    /**
     * @param IntegrationTester $I
     *
     * @return void
     */
    public function tokenRevoked(IntegrationTester $I): void
    {
        $userId = $this->user->id;

        $pair = $this->service->generatePair($userId);

        $this->service->revoke($pair->getAccessToken()->getId());

        $I->dontSeeRecord(
            'user_jwt_tokens',
            [
                'id' => $pair->getAccessToken()->getId(),
            ]
        );

        $this->service->verify($pair->getAccessToken());

        $I->expectThrowable(
            JwtAuthenticationException::forCode(JwtStatusCodeEnum::REVOKED),
            function () use ($pair): void {
                $this->service->verify($pair->getRefreshToken(), JwtTypeEnum::REFRESH);
            }
        );
    }

    /**
     * @param IntegrationTester $I
     *
     * @return void
     */
    public function refreshPair(IntegrationTester $I): void
    {
        $userId = $this->user->id;

        $pair = $this->service->generatePair($userId);

        $newPair = $this->service->refreshPair($pair->getRefreshToken());

        $I->assertNotEquals($pair, $newPair);
    }
}
