<?php

namespace Tests\Api\ResetPassword;

use AdminSeeder;
use ApiTester;
use Codeception\Example;
use Vrcci\PasswordReset\Entity\PasswordReset;
use Vrcci\User\Entity\User;

class ForgotPasswordCest
{
    /**
     * @var string
     */
    protected $forgotPasswordEndpoint = '/api/password-reset';

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function createToken(ApiTester $I): void
    {
        $params = [
            'email' => AdminSeeder::EMAIL,
        ];

        $I->sendPOST($this->forgotPasswordEndpoint, $params);

        /** @var User $user */
        $user = $I->grabRecord(User::class, $params);

        $I->seeRecord('password_resets', ['user_id' => $user->id]);
        $I->seeResponseCodeIs(204);
    }

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function updateToken(ApiTester $I): void
    {
        $params = [
            'email' => AdminSeeder::EMAIL,
        ];

        $I->sendPOST($this->forgotPasswordEndpoint, $params);

        /** @var User $user */
        $user = $I->grabRecord(User::class, $params);

        $I->seeRecord('password_resets', ['user_id' => $user->id]);
        $I->seeResponseCodeIs(204);

        $oldResetPassword = PasswordReset::where(['user_id' => $user->id])->first();

        $I->sendPOST($this->forgotPasswordEndpoint, $params);

        $I->seeRecord('password_resets', ['user_id' => $user->id]);
        $I->seeResponseCodeIs(204);

        $newResetPassword = PasswordReset::where(['user_id' => $user->id])->first();

        $I->assertNotEquals($oldResetPassword->token, $newResetPassword->token, 'Old token equals new token');
    }

    /**
     * @dataProvider validateForgotPasswordDataProvider
     *
     * @param ApiTester $I
     * @param Example   $example
     */
    public function validationForgot(ApiTester $I, Example $example): void
    {
        $params = [
            'email' => $example['email'],
        ];

        $I->sendPOST($this->forgotPasswordEndpoint, $params);

        $I->seeValidationErrors($example['errors']);
    }

    /**
     * @return mixed[]
     */
    protected function validateForgotPasswordDataProvider(): array
    {
        return [
            'not_existed user'    => [
                'email'  => 'admin@notexisted.com',
                'errors' => ['email'],
            ],
            'format_registration' => [
                'email'  => 'admin',
                'errors' => ['email'],
            ],
        ];
    }
}
