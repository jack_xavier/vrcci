<?php

namespace Tests\Api\ResetPassword;

use AdminSeeder;
use ApiTester;
use Codeception\Example;
use Vrcci\PasswordReset\Entity\PasswordReset;
use Vrcci\User\Entity\User;

class ResetPasswordCest
{
    /**
     * @var string
     */
    protected $resetPasswordEndpoint = '/api/password-reset';

    /**
     * @var string
     */
    protected $loginEndpoint = '/api/auth';

    /**
     * @var string
     */
    protected $forgotPasswordEndpoint = '/api/password-reset';

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function reset(ApiTester $I): void
    {
        $params = [
            'email' => 'admin@vrcci.com',
        ];

        $I->sendPOST($this->forgotPasswordEndpoint, $params);

        /** @var User $user */
        $user = $I->grabRecord(User::class, $params);

        $resetPassword = PasswordReset::where(['user_id' => $user->id])->first();

        $params = [
            'token'    => $resetPassword->token,
            'password' => AdminSeeder::PASSWORD . 'random',
        ];

        $I->sendPUT($this->resetPasswordEndpoint, $params);

        $I->seeResponseJwtJsonWithData();

        $params = [
            'email'    => AdminSeeder::EMAIL,
            'password' => AdminSeeder::PASSWORD . 'random',
        ];
        $I->sendPOST($this->loginEndpoint, $params);
        $I->seeResponseJwtJsonWithData();
        $I->cantSeeRecord('password_resets', ['user_id' => $user->id]);
    }

    /**
     * @dataProvider validateResetPasswordDataProvider
     *
     * @param ApiTester $I
     * @param Example   $example
     *
     * @return void
     */
    public function validationReset(ApiTester $I, Example $example): void
    {
        /** @var User $user */
        $user = $I->grabRecord(User::class, ['email' => AdminSeeder::EMAIL]);

        $passwordReset       = factory(PasswordReset::class)->create(['user_id' => $user->id]);
        $useTokenFromExample = in_array('token', $example['errors']);

        $params = [
            'token'    => $useTokenFromExample ? $example['token'] : $passwordReset->token,
            'password' => $example['password'],
        ];

        $I->sendPUT($this->resetPasswordEndpoint, $params);

        $I->seeValidationErrors($example['errors']);
    }

    /**
     * @return mixed[]
     */
    protected function validateResetPasswordDataProvider(): array
    {
        return [
            'min_password_length' => [
                'token'    => 'tokenexample',
                'password' => 'asd',
                'errors'   => ['password'],
            ],
            'invalid_token'       => [
                'token'    => 'tokenexample',
                'password' => 'Very1Long2Password3',
                'errors'   => ['token'],
            ],
        ];
    }
}
