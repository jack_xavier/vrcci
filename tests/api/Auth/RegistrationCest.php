<?php

namespace Tests\Api\Auth;

use ApiTester;
use Codeception\Example;
use Vrcci\User\Entity\User;

class RegistrationCest
{
    /**
     * @var string
     */
    protected $registrationEndpoint = '/api/registration';

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function registration(ApiTester $I): void
    {
        $params = [
            'email'    => 'admin@registration.com',
            'password' => '123456',
        ];

        $I->sendPOST($this->registrationEndpoint, $params);

        $user = User::where(['email' => 'admin@registration.com'])->first();
        $I->seeResponseJwtJsonWithData($user->id, 'User was registered');
    }

    /**
     * @dataProvider validateRegistrationDataProvider
     *
     * @param ApiTester $I
     * @param Example   $example
     */
    public function validationRegistration(ApiTester $I, Example $example): void
    {
        $params = [
            'email'    => $example['email'],
            'password' => $example['password'],
        ];

        $I->sendPOST($this->registrationEndpoint, $params);

        $I->seeValidationErrors($example['errors']);
    }

    /**
     * @return mixed[]
     */
    protected function validateRegistrationDataProvider(): array
    {
        return [
            'existed user'        => [
                'email'    => 'admin@vrcci.com',
                'password' => 'asd123',
                'errors'   => ['email'],
            ],
            'min_password_length' => [
                'email'    => 'admin@validation.com',
                'password' => 'asd',
                'errors'   => ['password'],
            ],
            'format_registration' => [
                'email'    => 'admin',
                'password' => 'asd123',
                'errors'   => ['email'],
            ],
        ];
    }
}
