<?php

namespace Tests\Api\Auth;

use ApiTester;

class AuthorizationCest
{
    /**
     * @var string
     */
    protected $loginEndpoint = '/api/auth';

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function login(ApiTester $I): void
    {
        $params = [
            'email'    => 'admin@vrcci.com',
            'password' => '123456',
        ];

        $I->sendPOST($this->loginEndpoint, $params);
        $I->seeResponseJwtJsonWithData();
    }

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function loginThenRefreshToken(ApiTester $I): void
    {
        $this->login($I);
        $response = $I->grabDataFromResponseByJsonPath('$.data')[0];

        $params = [
            'refreshToken' => $response['refresh']['token'],
        ];

        $I->sendPUT($this->loginEndpoint, $params);

        $I->seeResponseJsonWithData(
            [
                'data' => [
                    'access'  => [
                        'userId' => $response['access']['userId'],
                    ],
                    'refresh' => [
                        'userId' => $response['access']['userId'],
                    ],
                ],
            ]
        );

        $I->seeResponseMatchesJsonType(ApiTester::JWT_PAIR_TYPE, '$.data');
    }

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function logout(ApiTester $I): void
    {
        $I->amLogged();
        $I->sendDELETE($this->loginEndpoint);
        $I->seeResponseCodeIs(204);
    }
}
