<?php

namespace Tests\Api\User;

use ApiTester;
use App\Helper\CurrentUserTrait;
use Codeception\Example;
use Tests\Api\ResetPassword\ForgotPasswordCest;
use Vrcci\User\Entity\User;

class UserCrudCest
{
    use CurrentUserTrait;

    /**
     * @var string
     */
    protected $userEndpoint = '/api/users/%d';

    /**
     * @var string
     */
    protected $loginEndpoint = '/api/auth';

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function getUser(ApiTester $I): void
    {
        $I->amLogged();
        $user = $this->getCurrentUser();

        $I->sendGET(sprintf($this->userEndpoint, $user->getGuid()));
        $I->seeResponseJsonWithData(['email' => $user->getEmail()]);
    }

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function updateUserInfo(ApiTester $I): void
    {
        $I->amLogged();
        $user   = $this->getCurrentUser();
        $params = [
            'email'    => str_random(10) . '@vrcci.com',
            'password' => null,
        ];

        $I->sendPUT(sprintf($this->userEndpoint, $user->getGuid()), $params);
        $I->seeResponseJsonWithData(['email' => $params['email']]);
    }

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function updateUserCredentials(ApiTester $I): void
    {
        $I->amLogged();
        $user   = $this->getCurrentUser();
        $params = [
            'email'    => str_random(10) . '@vrcci.com',
            'password' => str_random(10),
        ];

        $I->sendPUT(sprintf($this->userEndpoint, $user->getGuid()), $params);
        $I->seeResponseJsonWithData(['email' => $params['email']]);

        $I->sendPOST($this->loginEndpoint, $params);
        $I->seeResponseJwtJsonWithData();
    }

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function updateUserEmailAfterResetPasswordTokenCreation(ApiTester $I): void
    {
        (new ForgotPasswordCest())->createToken($I);

        $this->updateUserInfo($I);
    }

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function forbiddenFindUser(ApiTester $I): void
    {
        $password = str_random(10);
        $user     = factory(User::class)->create(
            [
                'password' => $password,
            ]
        );
        $I->amLogged($user->email, $password);

        $user = factory(User::class)->create();

        $I->sendGET(sprintf($this->userEndpoint, $user->id));
        $I->seeResponseCodeIs(403);
    }

    /**
     * @param ApiTester $I
     *
     * @return void
     */
    public function forbiddenUpdateUser(ApiTester $I): void
    {
        $password = str_random(10);
        $user     = factory(User::class)->create(
            [
                'password' => $password,
            ]
        );
        $I->amLogged($user->email, $password);

        $user = factory(User::class)->create();

        $params = [
            'email'    => str_random(10) . $user->email,
            'password' => str_random(10),
        ];

        $I->sendPUT(sprintf($this->userEndpoint, $user->id), $params);

        $I->seeResponseCodeIs(403);
    }

    /**
     * @dataProvider validateUserUpdateDataProvider
     *
     * @param ApiTester $I
     * @param Example   $example
     */
    public function validationUserUpdate(ApiTester $I, Example $example): void
    {
        $I->amLogged();
        factory(User::class)->create(
            [
                'email' => 'sobaka@sobaka.com',
            ]
        );

        $params = [
            'email'    => $example['email'],
            'password' => $example['password'],
        ];

        $I->sendPUT(sprintf($this->userEndpoint, 1), $params);

        $I->seeValidationErrors($example['errors']);
    }

    /**
     * @return mixed[]
     */
    protected function validateUserUpdateDataProvider(): array
    {
        return [
            'existed_user'        => [
                'email'    => 'sobaka@sobaka.com',
                'password' => 'asd123',
                'errors'   => ['email'],
            ],
            'min_password_length' => [
                'email'    => 'admin@validation.com',
                'password' => 'asd',
                'errors'   => ['password'],
            ],
            'format_registration' => [
                'email'    => 'admin',
                'password' => 'asd123',
                'errors'   => ['email'],
            ],
        ];
    }
}
