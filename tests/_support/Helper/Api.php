<?php

namespace Helper;

use Codeception\Exception\ModuleException;
use Codeception\Module;
use Codeception\Step;
use Codeception\TestInterface;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class Api extends Module
{
    /**
     * @param Step $test
     *
     * @throws ModuleException
     *
     * @return void
     */
    public function _beforeStep(Step $test): void
    {
        /** @var Module\REST $restModule */
        $restModule = $this->getModule('REST');

        $restModule->haveHttpHeader('Accept', 'application/json');
    }

    /**
     * @param TestInterface $test
     *
     * @return void
     */
    public function _before(TestInterface $test): void
    {
        Storage::fake();
    }

    /**
     * @param TestInterface $test
     *
     * @return void
     */
    public function _after(TestInterface $test): void
    {
        Cache::flush();
    }
}
