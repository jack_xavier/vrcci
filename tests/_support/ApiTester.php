<?php

use Codeception\Actor;
use Vrcci\Auth\Jwt\Service\JwtTokenService;
use Vrcci\User\Entity\User;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class ApiTester extends Actor
{
    use _generated\ApiTesterActions;

    public const JWT_PAIR_TYPE = [
        'access'  => self::JWT_JSON_TYPE,
        'refresh' => self::JWT_JSON_TYPE,
    ];

    protected const JWT_JSON_TYPE = [
        'token'     => 'string',
        'id'        => 'integer:>0',
        'userId'    => 'integer:>0',
        'issuedAt'  => 'integer:>0',
        'expiredAt' => 'integer:>0',
    ];

    /**
     * @param string $email
     * @param string $password
     *
     * @return User
     */
    public function amLogged(string $email = AdminSeeder::EMAIL, string $password = AdminSeeder::PASSWORD): User
    {
        /**
         * Dont know how this magic works,
         * but we need both `setUser` and `amBearerAuthenticated`
         * to force auth works properly
         *
         * Without `amBearerAuthenticated` ~20% of tests will be failed
         * with 401 (Unauthorized) error
         *
         * Without `setUser` almost all api tests will be failed
         */

        $user = Auth::getProvider()->retrieveByCredentials(['email' => $email, 'password' => $password]);

        $this->getApplication()->get('auth')->guard('api')->setUser($user);

        $pair = resolve(JwtTokenService::class)->generatePair($user->getAuthIdentifier());

        $this->amBearerAuthenticated((string)$pair->getAccessToken()->getToken());

        return $user;
    }

    /**
     * @param mixed|null $data
     *
     * @return void
     */
    public function seeResponseJsonWithData($data = null): void
    {
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();
        $this->seeResponseJsonMatchesXpath('//data');

        if ($data) {
            $this->seeResponseContainsJson($data);
        }
    }

    /**
     * @param string[] $fields
     * @param int      $code
     *
     * @return void
     */
    public function seeValidationErrors(array $fields, int $code = 422): void
    {
        $this->seeResponseCodeIs($code);
        $this->seeResponseIsJson();
        $response = $this->grabDataFromResponseByJsonPath('$.errors')[0];

        codecept_debug($response);

        $this->assertEquals($fields, array_keys($response));
    }

    /**
     * @param int    $userId
     * @param string $message
     *
     * @return void
     */
    public function seeResponseJwtJsonWithData(int $userId = 1, string $message = 'Authentication successful'): void
    {
        $this->seeResponseJsonWithData(
            [
                'data' => [
                    'access'  => [
                        'userId' => $userId,
                    ],
                    'refresh' => [
                        'userId' => $userId,
                    ],
                ],
                'meta' => [
                    'message' => $message,
                ],
            ]
        );

        $this->seeResponseMatchesJsonType(self::JWT_PAIR_TYPE, '$.data');
    }
}
