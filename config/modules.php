<?php

return [
    'modules' => [
        'Vrcci\\Api\\Auth\\Common',
        'Vrcci\\Api\\Company',
        'Vrcci\\Api\\Model',
        'Vrcci\\Api\\Certificate',
        'Vrcci\\Api\\Presentation',
        'Vrcci\\Api\\Excursion',
        'Vrcci\\Api\\Sphere360',
        'Vrcci\\Api\\Feed',
        'Vrcci\\Api\\PasswordReset',
        'Vrcci\\Api\\User',
        'Vrcci\\Auth\\Common',
        'Vrcci\\Auth\\Jwt',
        'Vrcci\\Model',
        'Vrcci\\Company',
        'Vrcci\\Feed',
        'Vrcci\\Certificate',
        'Vrcci\\Excursion',
        'Vrcci\\Presentation',
        'Vrcci\\Sphere360',
        'Vrcci\\PasswordReset',
        'Vrcci\\User',
    ],
];
