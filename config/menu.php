<?php

return [
    'top-menu' => [
        'items' => [
            'main' => [
                'label' => 'Управление стендами',
                'route' => 'vrcci.companies.list',
                'attributes' => [
                    'iconClass' => 'fa fa-line-chart mr-1',
                ],
            ],
            'import' => [
                'label' => 'Управление конференц-залом',
                'route' => 'home',
                'attributes' => [
                    'iconClass' => 'fa fa-university mr-1',
                ],
            ],
            'users' => [
                'label' => 'Управление пользователями',
                'route' => 'vrcci.users.list',
                'attributes' => [
                    'iconClass' => 'fa fa-users mr-1',
                ],
            ],
            'statistics' => [
                'label' => 'Статистика',
                'route' => 'home',
                'attributes' => [
                    'iconClass' => 'fa fa-chart mr-1',
                ],
            ],
        ],
    ],
];
