<?php

use Vrcci\Auth\Jwt\Dto\JwtPairTo;
use Vrcci\Auth\Jwt\Dto\Resource\JwtPairResource;
use Vrcci\Certificate\Dto\CertificateTo;
use Vrcci\Certificate\Dto\Resources\CertificateResource;
use Vrcci\Company\Dto\CompanySettingsTo;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Company\Dto\Resources\CompanyResource;
use Vrcci\Company\Dto\Resources\CompanySettingsResource;
use Vrcci\Excursion\Dto\ExcursionTo;
use Vrcci\Excursion\Dto\Resources\ExcursionResource;
use Vrcci\Feed\Dto\FeedTo;
use Vrcci\Feed\Dto\Resources\FeedResource;
use Vrcci\Model\Dto\ModelTo;
use Vrcci\Model\Dto\Resource\StandModelResource;
use Vrcci\Model\Dto\Resources\ModelResource;
use Vrcci\Model\Dto\Resources\TextureResource;
use Vrcci\Model\Dto\StandModelTo;
use Vrcci\Model\Dto\TextureTo;
use Vrcci\Presentation\Dto\PresentationTo;
use Vrcci\Presentation\Dto\Resources\PresentationResource;
use Vrcci\Sphere360\Dto\Resources\Sphere360Resource;
use Vrcci\Sphere360\Dto\Sphere360To;
use Vrcci\User\Dto\Resources\UserResource;
use Vrcci\User\Dto\UserTo;

return [
    'map' => [
        TextureTo::class => TextureResource::class,
        ModelTo::class => ModelResource::class,
        StandModelTo::class => StandModelResource::class,
        CertificateTo::class => CertificateResource::class,
        FeedTo::class => FeedResource::class,
        Sphere360To::class => Sphere360Resource::class,
        ExcursionTo::class => ExcursionResource::class,
        CompanyTo::class => CompanyResource::class,
        CompanySettingsTo::class => CompanySettingsResource::class,
        PresentationTo::class => PresentationResource::class,
        JwtPairTo::class => JwtPairResource::class,
        UserTo::class => UserResource::class,
    ],
];
