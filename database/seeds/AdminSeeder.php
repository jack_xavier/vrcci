<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Vrcci\Auth\Common\Enum\AccessEnum;
use Vrcci\User\Entity\User;

class AdminSeeder extends Seeder
{
    public const EMAIL    = 'admin@vrcci.com';
    public const PASSWORD = '123456';

    /**
     * @return void
     */
    public function run(): void
    {
        if (null !== User::first()){
            return;
        }
        /** @var User $user */
        $user = User::create(
            [
                'email'    => static::EMAIL,
                'password' => bcrypt(static::PASSWORD),
            ]
        );

        DB::table('role_user')->insert(
            [
                'user_id'   => $user->id,
                'role_id'   => DB::table('roles')
                    ->where('name', AccessEnum::R_ADMIN)
                    ->value('id'),
                'user_type' => User::class,
            ]
        );
    }
}
