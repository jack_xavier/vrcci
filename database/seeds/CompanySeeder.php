<?php

use Illuminate\Database\Seeder;
use Vrcci\Company\Eloquent\Entity\Company;
use Vrcci\Company\Eloquent\Entity\CompanySettings;
use Vrcci\User\Entity\User;

class CompanySeeder extends Seeder
{
    /**
     * @return void
     */
    public function run(): void
    {
        $user = User::firstOrFail();
        $company = (new Company())->fill(
            [
                'title' => 'Test Company',
                'bm_link' => '/nothing',
            ]
        );
        $company->user()->associate($user);
        $company->save();

        $companySettings = new CompanySettings();
        $companySettings->company()->associate($company);
        $companySettings->save();
    }
}
