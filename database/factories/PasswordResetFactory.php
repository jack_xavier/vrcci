<?php

use Illuminate\Support\Str;
use Vrcci\PasswordReset\Entity\PasswordReset;

$factory->define(
    PasswordReset::class,
    function (): array {
        return [
            'token' => Str::random(60),
        ];
    }
);
