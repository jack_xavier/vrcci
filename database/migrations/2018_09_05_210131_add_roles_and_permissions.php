<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Vrcci\Auth\Common\Enum\AccessEnum;

class AddRolesAndPermissions extends Migration
{
    protected $roles = [
        AccessEnum::R_ADMIN,
        AccessEnum::R_USER,
    ];

    protected $permissions = [
        AccessEnum::P_FULL_LOOKUP => [
            AccessEnum::R_ADMIN,
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roles       = [];
        $permissions = [];

        foreach ($this->roles as $role) {
            $roleId = DB::table('roles')->where('name', $role)->value('id');
            if (empty($roleId)) {
                $roleId = DB::table('roles')->insertGetId(
                    [
                        'name'         => $role,
                        'display_name' => sprintf('auth.rbac.%s', $role),
                    ]
                );
            }

            $roles[$role] = $roleId;
        }

        foreach ($this->permissions as $permission => $rel) {
            $permissionId = DB::table('permissions')->where('name', $permission)->value('id');
            if (empty($permissionId)) {
                $permissionId = DB::table('permissions')->insertGetId(
                    [
                        'name'         => $permission,
                        'display_name' => sprintf('auth.rbac.%s', $permission),
                    ]
                );
            }
            $permissions[$permission] = $permissionId;

            foreach ($rel as $relRole) {
                $permissionRole = DB::table('permission_role')
                                    ->where('permission_id', $permissionId)
                                    ->where('role_id', $roles[$relRole])
                                    ->first();
                if (empty($permissionRole)) {
                    DB::table('permission_role')->insert(
                        [
                            'permission_id' => $permissionId,
                            'role_id'       => $roles[$relRole],
                        ]
                    );
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
