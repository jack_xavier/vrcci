#!/bin/bash

DIR="$( dirname "${BASH_SOURCE[0]}" )"
U=`/bin/bash -c "$DIR/_docker-user.sh"`

set -o xtrace

docker-compose ${METHOD-exec} ${U} ${1-app} ${CMD-bash}
