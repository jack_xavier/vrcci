#!/bin/bash

echo "Building assets"

DIR="$( dirname "${BASH_SOURCE[0]}" )"
U=`/bin/bash -c "$DIR/_docker-user.sh"`

CMD=${1-build}

if [[ $CMD == "start" ]]; then
    PORTS="-p 3000:3000";
fi;

docker-compose run ${PORTS} --rm ${U} npm "yarn --frozen-lockfile && yarn ${CMD}"
