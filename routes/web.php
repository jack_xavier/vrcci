<?php

use Illuminate\Support\Facades\Route;

Route::middleware('guest')->get('/', 'HomeController@welcome')->name('home');
/** Only authenticated users */
Route::middleware('auth:web')->group(
    function () {
        Route::get('/home', 'HomeController@welcome')->name('home');
        Route::get('/profile', 'ProfileController@index')->name('user.profile');
        Route::post('/profile/pwd', 'ProfileController@updatePassword')->name('user.profile.update.password');
    }
);

/** Login routes */
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login.submit');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout')->middleware('auth');

