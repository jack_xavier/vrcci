#!/bin/bash

php artisan jwt:keys --force
php artisan config:cache
php artisan l5-swagger:generate

exec "$@"