FROM php:7.2-fpm

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        #locales \
        libpq-dev \
        #libxml2-dev \
        zlib1g-dev \
        libmemcached-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install -j$(nproc) pgsql pdo_pgsql opcache \
    && pecl install memcached \
    && docker-php-ext-enable memcached opcache

WORKDIR /wts

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY .deploy/php/php.ini /usr/local/etc/php/conf.d/php.ini
COPY .deploy/php/opcache/opcache.ini /usr/local/etc/php/conf.d/docker-php-ext-opcache-config.ini

COPY app ./app
COPY artisan ./artisan
COPY bootstrap ./bootstrap
COPY composer.json ./composer.json
COPY config ./config
COPY database ./database
COPY module ./module
COPY public ./public
COPY resources ./resources
COPY routes ./routes
COPY storage ./storage
COPY vendor ./vendor

COPY .deploy/entrypoint.sh /etc/entrypoint.sh

ENTRYPOINT ["/etc/entrypoint.sh"]
CMD ["php-fpm"]
