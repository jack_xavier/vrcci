<?php

namespace Vrcci\Presentation\Dto;

use App\Dto\ContainsFileToTrait;
use App\Dto\StoresOriginalModel;
use Vrcci\Company\Dto\StoresCompanyToHelper;
use Vrcci\Presentation\Eloquent\Entity\Presentation;

/**
 * @method Presentation getOriginal()
 */
class PresentationTo
{
    use StoresOriginalModel, ContainsFileToTrait, StoresCompanyToHelper;
}
