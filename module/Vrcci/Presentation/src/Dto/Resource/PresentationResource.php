<?php

namespace Vrcci\Presentation\Dto\Resources;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\StoresFileResourceTrait;

/**
 * @OA\Schema(
 *     schema="Presentation",
 *     @OA\Property(property="guid", type="string", description="Presentation Id"),
 *     @OA\Property(property="url", type="string", description="File url"),
 *     @OA\Property(property="filename", type="string", description="File name"),
 *     ),
 * )
 */
class PresentationResource extends AbstractExtractableResource
{
    use StoresFileResourceTrait;
}
