<?php

namespace Vrcci\Presentation\Service;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Exception;
use Illuminate\Http\UploadedFile;
use Vrcci\Presentation\Dto\PresentationTo;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Presentation\Eloquent\Entity\Presentation;
use Vrcci\User\Dto\UserTo;

class PresentationCrudService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param UploadedFile $file
     * @param CompanyTo    $companyTo
     * @param UserTo       $userTo
     *
     * @throws UnregisteredMappingException
     * @return PresentationTo
     */
    public function create(UploadedFile $file, CompanyTo $companyTo, UserTo $userTo): PresentationTo
    {
        $presentation = new Presentation();
        $presentation->company()->associate($companyTo->getOriginal());

        $presentation->setFile($file);

        $presentation->uploadedBy()->associate($userTo->getOriginal());
        $presentation->save();

        return $this->mapper->mapToObject($presentation, new PresentationTo());
    }

    /**
     * @param PresentationTo $presentationTo
     * @param UploadedFile   $file
     * @param UserTo         $userTo
     *
     * @throws UnregisteredMappingException
     *
     * @return PresentationTo
     */
    public function update(PresentationTo $presentationTo, UploadedFile $file, UserTo $userTo): PresentationTo
    {
        $presentation = $presentationTo->getOriginal();
        $presentation->setFile($file);

        $presentation->uploadedBy()->associate($userTo->getOriginal());
        $presentation->save();

        return $this->mapper->mapToObject($presentation, $presentationTo);
    }

    /**
     * @param PresentationTo $presentationTo
     *
     * @throws Exception
     *
     * @return void
     */
    public function delete(PresentationTo $presentationTo): void
    {
        $presentationTo->getOriginal()->delete();
    }
}
