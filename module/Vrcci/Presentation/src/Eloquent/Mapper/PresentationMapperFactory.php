<?php

namespace Vrcci\Presentation\Eloquent\Mapper;

use App\Mapper\FileMapperFactory;
use Vrcci\Presentation\Dto\PresentationTo;
use Vrcci\Presentation\Eloquent\Entity\Presentation;

class PresentationMapperFactory extends FileMapperFactory
{
    /**
     * @var string
     */
    protected $entityClassName = Presentation::class;

    /**
     * @var string
     */
    protected $dtoClassName = PresentationTo::class;
}
