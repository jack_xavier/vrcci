<?php

namespace Vrcci\Presentation\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Presentation\Eloquent\DataRequest\PresentationDataRequest;
use Vrcci\Presentation\Dto\PresentationTo;
use Vrcci\Presentation\Eloquent\Entity\Presentation;

class PresentationRepository
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return PresentationDataRequest
     */
    public function fetchAll(): PresentationDataRequest
    {
        $dataRequest = PresentationDataRequest::create(Presentation::with(['company']));
        $dataRequest->withTransformer(
            function (Presentation $presentation) {
                return $this->mapper->mapToObject($presentation, new PresentationTo());
            }
        );

        return $dataRequest;
    }

    /**
     * @param string $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return PresentationTo|null
     */
    public function find(string $guid): ?PresentationTo
    {
        return $this->fetchAll()->byGuid($guid)->first();
    }
}
