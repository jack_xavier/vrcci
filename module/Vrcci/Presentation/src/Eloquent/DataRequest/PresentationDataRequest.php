<?php

namespace Vrcci\Presentation\Eloquent\DataRequest;

use App\Eloquent\AbstractDataRequest;

class PresentationDataRequest extends AbstractDataRequest
{
    /**
     * @param string $companyId
     *
     * @return PresentationDataRequest
     */
    public function byCompanyGuid(string $companyId): self
    {
        $this->qb->where('company_guid', $companyId);

        return $this;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function byGuid(string $id): self
    {
        $this->qb->where('guid', $id);

        return $this;
    }
}
