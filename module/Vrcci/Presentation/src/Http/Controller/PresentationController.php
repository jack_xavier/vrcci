<?php

namespace Vrcci\Presentation\Http\Controller;

use App\Exceptions\FileUploadingException;
use App\Helper\CurrentUserTrait;
use App\Helper\FileLimitTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Presentation\Http\Request\UploadPresentationRequest;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Presentation\Dto\PresentationTo;
use Vrcci\Presentation\Eloquent\Repository\PresentationRepository;
use Vrcci\Presentation\Service\PresentationCrudService;

class PresentationController extends Controller implements ModuleAliasAwareInstance
{
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;
    use GuardForCompanyGuidTrait;
    use FileLimitTrait;

    /**
     * @var PresentationRepository
     */
    private $presentationRepository;

    /**
     * @var PresentationCrudService
     */
    private $presentationService;

    /**
     * @param CompanyRepository       $companyRepository
     * @param PresentationRepository  $presentationRepository
     * @param PresentationCrudService $presentationService
     */
    public function __construct(
        CompanyRepository $companyRepository,
        PresentationRepository $presentationRepository,
        PresentationCrudService $presentationService
    ) {
        $this->companyRepository = $companyRepository;
        $this->presentationRepository = $presentationRepository;
        $this->presentationService = $presentationService;
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     * @return View
     */
    public function listAction(string $companyGuid): View
    {
        $company = $this->guardCompanyGuid($companyGuid);

        $presentations = $this->presentationRepository
            ->fetchAll()
            ->byCompanyGuid($companyGuid)
            ->get();

        return $this->makeView('list')
            ->with('company', $company)
            ->with('presentations', $presentations);
    }

    /**
     * @param string                    $companyGuid
     * @param UploadPresentationRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function uploadAction(string $companyGuid, UploadPresentationRequest $request): RedirectResponse
    {
        $company = $this->guardCompanyGuid($companyGuid);

        try {
            $this->doValidate(
                $company,
                $company->getOriginal()->presentations()->get(),
                $request,
                'presentations'
            );
        } catch (FileUploadingException $exception) {
            return Redirect::back()->withErrors(
                [
                    $exception->getMessage(),
                ]
            );
        }

        try {
            $this->presentationService->create(
                $request->getFile(),
                $company,
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при загрузке файла презентации. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.presentation.list',
            ['company_guid' => $company->getGuid(),]
        );
    }

    /**
     * @param string                    $presentationGuid
     * @param UploadPresentationRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function reuploadAction(string $presentationGuid, UploadPresentationRequest $request): RedirectResponse
    {
        $presentationTo = $this->guardPresentationGuid($presentationGuid);
        $company = $presentationTo->getCompany();

        try {
            $this->doValidate(
                $company,
                $company->getOriginal()->presentations()->get(),
                $request,
                'presentations',
                true
            );
        } catch (FileUploadingException $exception) {
            return Redirect::back()->withErrors(
                [
                    $exception->getMessage(),
                ]
            );
        }

        try {
            $presentationTo = $this->presentationService->update(
                $presentationTo,
                $request->getFile(),
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при перезагрузке файла презентации. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.presentation.list',
            [
                'company_guid' => $presentationTo->getCompany()->getGuid(),
            ]
        );
    }

    /**
     * @param string|null $presentationGuid
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function deleteAction(?string $presentationGuid): RedirectResponse
    {
        $presentationTo = $this->guardPresentationGuid($presentationGuid);

        try {
            $this->presentationService->delete($presentationTo);
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при удалении файла презентации. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.presentation.list',
            ['company_guid' => $presentationTo->getCompany()->getGuid()]
        );
    }

    /**
     * @param string $presentationGuid
     *
     * @throws UnregisteredMappingException
     * @return PresentationTo
     */
    protected function guardPresentationGuid(string $presentationGuid): PresentationTo
    {
        $presentationTo = $this->presentationRepository->find($presentationGuid);
        if (empty($presentationTo)) {
            throw new NotFoundHttpException('Файл презентации не найден');
        }

        return $presentationTo;
    }
}
