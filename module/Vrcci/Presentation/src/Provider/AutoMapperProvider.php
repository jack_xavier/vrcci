<?php

namespace Vrcci\Presentation\Providers;

use App\Providers\AutoMapperProvider as BaseAutoMapperProvider;
use Illuminate\Support\ServiceProvider;
use Vrcci\Presentation\Eloquent\Mapper\PresentationMapperFactory;

class AutoMapperProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app->tag(
            [PresentationMapperFactory::class],
            [BaseAutoMapperProvider::FACTORY_TAG]
        );
    }
}
