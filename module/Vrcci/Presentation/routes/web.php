<?php

use App\Entity\GuidGenerator;
use Vrcci\Presentation\Http\Controller\PresentationController;

Route::middleware('auth:web')
    ->prefix('/presentations')
    ->name('vrcci.presentation.')
    ->group(
        function (): void {
            Route::get('/{company_guid}/list', PresentationController::class . '@listAction')
                ->name('list')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{company_guid}/upload', PresentationController::class . '@uploadAction')
                ->name('upload')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{presentation_guid}/reupload', PresentationController::class . '@reuploadAction')
                ->name('reupload')
                ->where('presentation_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{presentation_guid}/delete', PresentationController::class . '@deleteAction')
                ->name('delete')
                ->where('presentation_guid', GuidGenerator::GUID_PATTERN);
        }
    );

