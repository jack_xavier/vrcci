<?php

namespace Vrcci\Model\Eloquent\Mapper;

use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\AutoMapper\Operation\MapToRelation;
use App\Mapper\MappingFactoryInterface;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Model\Dto\ModelTo;
use Vrcci\Model\Dto\StandModelTo;
use Vrcci\Model\Dto\TextureTo;
use Vrcci\Model\Eloquent\Entity\StandModel;

class StandModelFactory implements MappingFactoryInterface
{
    /**
     * @param AutoMapperConfig $config
     *
     * @return void
     */
    public function registerMapping(AutoMapperConfig $config): void
    {
        $config->registerMapping(StandModel::class, StandModelTo::class)
            ->forMember('originalModel', new MapOriginal())
            ->forMember('modelFile', new MapToRelation(ModelTo::class))
            ->forMember('textures', new MapToRelation(TextureTo::class))
            ->forMember('company', new MapToRelation(CompanyTo::class));
    }
}
