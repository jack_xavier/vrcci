<?php

namespace Vrcci\Model\Eloquent\Mapper;

use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\MappingFactoryInterface;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\Model\Dto\ModelTo;
use Vrcci\Model\Eloquent\Entity\ModelFile;

class ModelMapperFactory implements MappingFactoryInterface
{
    /**
     * @param AutoMapperConfig $config
     *
     * @return void
     */
    public function registerMapping(AutoMapperConfig $config): void
    {
        $config->registerMapping(ModelFile::class, ModelTo::class)
            ->forMember('originalModel', new MapOriginal())
            ->forMember(
                'url',
                static function (ModelFile $modelFile): string {
                    return $modelFile->getFileUrl();
                }
            )->forMember(
                'filename',
                static function (ModelFile $modelFile): string {
                    return $modelFile->original_filename;
                }
            );
    }
}
