<?php

namespace Vrcci\Model\Eloquent\Mapper;

use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\MappingFactoryInterface;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\Excursion\Dto\ExcursionTo;
use Vrcci\Excursion\Eloquent\Entity\Excursion;
use Vrcci\Model\Dto\TextureTo;
use Vrcci\Model\Eloquent\Entity\TextureFile;

class TextureMapperFactory implements MappingFactoryInterface
{
    /**
     * @param AutoMapperConfig $config
     *
     * @return void
     */
    public function registerMapping(AutoMapperConfig $config): void
    {
        $config->registerMapping(TextureFile::class, TextureTo::class)
            ->forMember('originalModel', new MapOriginal())
            ->forMember(
                'url',
                static function (TextureFile $textureFile): string {
                    return $textureFile->getFileUrl();
                }
            )->forMember(
                'filename',
                static function (TextureFile $textureFile): string {
                    return $textureFile->original_filename;
                }
            );
    }
}
