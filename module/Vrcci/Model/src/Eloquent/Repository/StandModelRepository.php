<?php

namespace Vrcci\Model\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Model\Dto\StandModelTo;
use Vrcci\Model\Eloquent\DataRequest\StandModelDataRequest;
use Vrcci\Model\Eloquent\Entity\StandModel;

class StandModelRepository
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return StandModelDataRequest
     */
    public function fetchAll(): StandModelDataRequest
    {
        $dataRequest = StandModelDataRequest::create(StandModel::with(
            ['company', 'textures', 'modelFile']
        ));
        $dataRequest->withTransformer(
            function (StandModel $standModel) {
                return $this->mapper->mapToObject($standModel, new StandModelTo());
            }
        );

        return $dataRequest;
    }

    /**
     * @param string $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return StandModelTo|null
     */
    public function find(string $guid): ?StandModelTo
    {
        return $this->fetchAll()->byGuid($guid)->first();
    }
}
