<?php

namespace Vrcci\Model\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Model\Dto\ModelTo;
use Vrcci\Model\Eloquent\DataRequest\ModelDataRequest;
use Vrcci\Model\Eloquent\Entity\ModelFile;

class ModelRepository
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return ModelDataRequest
     */
    public function fetchAll(): ModelDataRequest
    {
        $dataRequest = ModelDataRequest::create(ModelFile::with(['standModel']));
        $dataRequest->withTransformer(
            function (ModelFile $model) {
                return $this->mapper->mapToObject($model, new ModelTo());
            }
        );

        return $dataRequest;
    }

    /**
     * @param string $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return ModelTo|null
     */
    public function find(string $guid): ?ModelTo
    {
        return $this->fetchAll()->byGuid($guid)->first();
    }
}
