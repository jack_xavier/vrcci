<?php

namespace Vrcci\Model\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Model\Dto\TextureTo;
use Vrcci\Model\Eloquent\DataRequest\TextureDataRequest;
use Vrcci\Model\Eloquent\Entity\TextureFile;

class TextureRepository
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return TextureDataRequest
     */
    public function fetchAll(): TextureDataRequest
    {
        $dataRequest = TextureDataRequest::create(TextureFile::with(['standModel']));
        $dataRequest->withTransformer(
            function (TextureFile $texture) {
                return $this->mapper->mapToObject($texture, new TextureTo());
            }
        );

        return $dataRequest;
    }

    /**
     * @param string $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return TextureTo|null
     */
    public function find(string $guid): ?TextureTo
    {
        return $this->fetchAll()->byGuid($guid)->first();
    }
}
