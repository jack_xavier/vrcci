<?php

namespace Vrcci\Model\Eloquent\Entity;

use App\Entity\File;
use App\Entity\GuidModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Storage;
use Vrcci\Company\Eloquent\Entity\Company;

/**
 * @property string description
 */
class StandModel extends GuidModel
{
    /**
     * @var string
     */
    protected $table = 'stand_models';

    /**
     * @var string[]
     */
    protected $fillable = [
        'description',
    ];

    public function modelFile(): HasOne
    {
        return $this->hasOne(ModelFile::class, 'stand_model_guid', 'guid');
    }

    public function textures(): HasMany
    {
        return $this->hasMany(TextureFile::class, 'stand_model_guid', 'guid');
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_guid', 'guid');
    }

    public function delete()
    {
        $directory = sprintf('%s/models', $this->company->title);
        if (!Storage::disk(File::VAR_DEFAULT_STORAGE_DISK)->exists($directory)) {
            Storage::disk(File::VAR_DEFAULT_STORAGE_DISK)->deleteDirectory($directory);
        }

        return parent::delete();
    }
}
