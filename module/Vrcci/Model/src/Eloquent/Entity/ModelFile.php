<?php

namespace Vrcci\Model\Eloquent\Entity;

use App\Entity\File;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ModelFile extends File
{
    /**
     * @var string
     */
    protected $table = 'models';

    /**
     * @return BelongsTo
     */
    public function standModel(): BelongsTo
    {
        return $this->belongsTo(StandModel::class, 'stand_model_guid', 'guid');
    }

    /**
     * @return string
     */
    protected function getDirectoryName(): string
    {
        $company = $this->standModel->company->title;

        return sprintf('%s/models', $company);
    }
}
