<?php

namespace Vrcci\Model\Eloquent\DataRequest;

use App\Eloquent\AbstractDataRequest;

class ModelDataRequest extends AbstractDataRequest
{
    /**
     * @param string $standModelGuid
     *
     * @return ModelDataRequest
     */
    public function byStandModelGuid(string $standModelGuid): self
    {
        $this->qb->where('stand_model_guid', $standModelGuid);

        return $this;
    }

    /**
     * @param string $guid
     *
     * @return $this
     */
    public function byGuid(string $guid): self
    {
        $this->qb->where('guid', $guid);

        return $this;
    }
}
