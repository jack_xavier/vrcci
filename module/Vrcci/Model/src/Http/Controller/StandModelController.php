<?php

namespace Vrcci\Model\Http\Controller;

use App\Exceptions\FileUploadingException;
use App\Helper\CurrentUserTrait;
use App\Helper\FileLimitTrait;
use App\Http\Controllers\Controller;
use App\Http\Enum\Messages;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redirect;
use Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Model\Http\Request\StandModelCreateRequest;
use Vrcci\Api\Model\Http\Request\StandModelUpdateRequest;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Model\Dto\StandModelTo;
use Vrcci\Model\Eloquent\Repository\StandModelRepository;
use Vrcci\Model\Service\StandModelCrudService;

class StandModelController extends Controller implements ModuleAliasAwareInstance
{
    use GuardForCompanyGuidTrait;
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;
    use GuardForStandModelGuidHelper;
    use FileLimitTrait;

    /**
     * @var StandModelCrudService
     */
    private $standModelService;

    /**
     * @param StandModelCrudService $standModelService
     * @param StandModelRepository  $standModelRepository
     * @param CompanyRepository     $companyRepository
     */
    public function __construct(
        StandModelCrudService $standModelService,
        StandModelRepository $standModelRepository,
        CompanyRepository $companyRepository
    ) {
        $this->standModelService = $standModelService;
        $this->standModelRepository = $standModelRepository;
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param string      $companyGuid
     * @param string|null $standModelGuid
     *
     * @throws UnregisteredMappingException
     *
     * @return View
     */
    public function updateFormAction(string $companyGuid, string $standModelGuid = null): View
    {
        $company = $this->guardCompanyGuid($companyGuid);

        $standModelTo = $standModelGuid
            ? $this->guardStandModelGuid($standModelGuid)
            : null;

        return $this->makeView('edit-form')
            ->with('standModel', $standModelTo)
            ->with('company', $company);
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     * @return View
     */
    public function createFormAction(string $companyGuid): View
    {
        $company = $this->guardCompanyGuid($companyGuid);

        return $this->makeView('create-form')
            ->with('company', $company);
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     * @return View
     */
    public function listAction(string $companyGuid): View
    {
        $company = $this->guardCompanyGuid($companyGuid);

        $standModels = $this->standModelRepository
            ->fetchAll()
            ->byCompanyGuid($companyGuid)
            ->get();

        return $this->makeView('list')
            ->with('company', $company)
            ->with('standModels', $standModels);
    }

    /**
     * @param string                  $companyGuid
     * @param StandModelCreateRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function createAction(string $companyGuid, StandModelCreateRequest $request): RedirectResponse
    {
        $companyTo = $this->guardCompanyGuid($companyGuid);
        $messages = [];
        $modelsFileLimit = $companyTo->getPreferences()->getModelsFileLimit();
        $attributes = $request->getAttributes()->toArray();

        if (($attributes['model_file']->getSize() / 1024) >= $modelsFileLimit) {
            $messages[] = sprintf(Messages::get('models.file_amount'), $modelsFileLimit / 1024);
        }

        if (!empty($attributes['textures'])) {
            try {
                $this->validateRequestFilesArray(
                    $companyTo->getPreferences()->getTexturesFileLimit(),
                    $companyTo->getPreferences()->getTextures(),
                    $request,
                    'textures'
                );
            } catch (FileUploadingException $exception) {
                $messages[] = $exception->getMessage();
            }
        }

        if (!empty($messages)) {
            return Redirect::back()->withErrors(
                $messages
            );
        }

        try {
            $modelTo = $this->standModelService->create(
                $request->getAttributes(),
                $companyTo,
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при создании модели.',
                ]
            );
        }

        return redirect()->route(
            'vrcci.stand-model.list',
            [
                'company_guid' => $modelTo->getCompany()->getGuid(),
            ]
        );
    }

    /**
     * @param string                  $standModelGuid
     * @param StandModelUpdateRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function updateAction(string $standModelGuid, StandModelUpdateRequest $request): RedirectResponse
    {
        $modelTo = $this->guardStandModelGuid($standModelGuid);
        $companyTo = $modelTo->getCompany();
        $attributes = $request->getAttributes()->toArray();

        $texturesFileLimit = $companyTo->getPreferences()->getTexturesFileLimit();
        $messages = [];

        $modelsFileLimit = $companyTo->getPreferences()->getModelsFileLimit();

        if (!empty($attributes['model_file']) && $attributes['model_file']->getSize() / 1024 >= $modelsFileLimit) {
            $messages[] = sprintf(Messages::get('models.file_amount'), $modelsFileLimit / 1024);
        }

        if (!empty($attributes['textures'])) {
            if (!$this->texturesMemoryLimitIsValid(
                $modelTo,
                $attributes,
                $texturesFileLimit
            )) {
                $messages[] = sprintf(Messages::get('textures.file_amount'), $texturesFileLimit);
            }
        }

        if (!empty($messages)) {
            return Redirect::back()->withErrors(
                $messages
            );
        }

        try {
            $modelTo = $this->standModelService->update(
                $modelTo,
                $request->getAttributes(),
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при обновлении модели.',
                ]
            );
        }

        return redirect()->route(
            'vrcci.stand-model.list',
            [
                'company_guid' => $modelTo->getCompany()->getGuid(),
            ]
        );
    }

    /**
     * @param string $standModelGuid
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function deleteAction(string $standModelGuid): RedirectResponse
    {
        $modelTo = $this->guardStandModelGuid($standModelGuid);

        try {
            $this->standModelService->delete($modelTo);
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при удалении модели.',
                ]
            );
        }

        return redirect()->route(
            'vrcci.stand-model.list',
            [
                'company_guid' => $modelTo->getCompany()->getGuid(),
            ]
        );
    }

    /**
     * @param string $standModelGuid
     *
     * @throws UnregisteredMappingException
     * @return StandModelTo
     */
    protected function guardStandModelGuid(string $standModelGuid): StandModelTo
    {
        $standModelTo = $this->standModelRepository->find($standModelGuid);
        if (empty($standModelTo)) {
            throw new NotFoundHttpException('Модель не найдена');
        }

        return $standModelTo;
    }

    /**
     * @param StandModelTo $modelTo
     * @param array        $attributes
     * @param int          $texturesFileLimit
     *
     * @return bool
     */
    private function texturesMemoryLimitIsValid(
        StandModelTo $modelTo,
        array $attributes,
        int $texturesFileLimit
    ) {
        $textures = $modelTo->getTextures();
        $textureIndices = $attributes['texture_indexes'] ?? [];
        $textureFiles = $attributes['textures'] ?? [];

        if (empty($textureFiles)) {
            return true;
        }

        if (empty($textures)) {
            return true;
        }

        if (empty($textureIndices)) {
            return true;
        }

        $totalMemory = .0;
        foreach ($textures as $texture) {
            if (in_array($texture->getGuid(), $textureIndices)) {
                $totalMemory += $texture->getOriginal()->getFileSize();
            }
        }

        /** @var UploadedFile $textureFile */
        foreach ($textureFiles as $textureFile) {
            $totalMemory += $textureFile->getSize() / 1000;
        }

        return $totalMemory <= $texturesFileLimit;
    }

}
