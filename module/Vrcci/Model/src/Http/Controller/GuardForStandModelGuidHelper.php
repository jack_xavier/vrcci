<?php

namespace Vrcci\Model\Http\Controller;

use AutoMapperPlus\Exception\UnregisteredMappingException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Model\Dto\StandModelTo;
use Vrcci\Model\Eloquent\Repository\StandModelRepository;

trait GuardForStandModelGuidHelper
{
    /**
     * @var StandModelRepository
     */
    protected $standModelRepository;

    /**
     * @param string $standModelGuid
     *
     * @throws UnregisteredMappingException
     * @return StandModelTo
     */
    protected function guardStandModelGuid(string $standModelGuid): StandModelTo
    {
        $standModelTo = $this->standModelRepository->find($standModelGuid);
        if (empty($standModelTo)) {
            throw new NotFoundHttpException('Модель не найдена');
        }

        return $standModelTo;
    }

}
