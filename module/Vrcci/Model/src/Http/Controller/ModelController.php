<?php

namespace Vrcci\Model\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Http\RedirectResponse;
use Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Vrcci\Api\Model\Http\Request\UploadModelRequest;
use Vrcci\Model\Dto\ModelTo;
use Vrcci\Model\Eloquent\Repository\StandModelRepository;
use Vrcci\Model\Eloquent\Repository\ModelRepository;
use Vrcci\Model\Service\ModelFileService;

class ModelController extends Controller implements ModuleAliasAwareInstance
{
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;
    use GuardForStandModelGuidHelper;

    /**
     * @var ModelRepository
     */
    private $modelRepository;

    /**
     * @var ModelFileService
     */
    private $modelService;

    /**
     * @var StandModelRepository
     */
    protected $standModelRepository;

    /**
     * @param ModelRepository    $modelRepository
     * @param ModelFileService   $modelService
     * @param StandModelRepository $standModelRepository
     */
    public function __construct(
        ModelRepository $modelRepository,
        ModelFileService $modelService,
        StandModelRepository $standModelRepository
    ) {
        $this->modelRepository = $modelRepository;
        $this->modelService = $modelService;
        $this->standModelRepository = $standModelRepository;
    }

    /**
     * @param string               $standModelGuid
     * @param UploadModelRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function uploadAction(string $standModelGuid, UploadModelRequest $request): RedirectResponse
    {
        $standModel = $this->guardStandModelGuid($standModelGuid);

        if ($standModel->getOriginal()->modelFile()->count() >= 1) {
            return redirect()->route(
                'vrcci.stand-model.form',
                ['company_guid' => $standModel->getCompany(),]
            )->with('danger', ['Максимальное количество файлов моделей которое можно загрузить - 1']);
        }

        try {
            $this->modelService->create(
                $request->getFile(),
                $standModel->getOriginal(),
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            throw new HttpException(500, 'Возникла ошибка при загрузке файла модели');
        }

        return redirect()->route(
            'vrcci.stand-model.list',
            ['stand_model_guid' => $standModel->getCompany(),]
        );
    }

    /**
     * @param string               $modelGuid
     * @param UploadModelRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function reuploadAction(string $modelGuid, UploadModelRequest $request): RedirectResponse
    {
        $modelTo = $this->guardModelGuid($modelGuid);

        try {
            $modelTo = $this->modelService->update(
                $modelTo,
                $request->getFile(),
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            throw new HttpException(500, 'Возникла ошибка при перезагрузке файла модели');
        }

        return redirect()->route(
            'vrcci.stand-model.list',
            [
                'stand_model_guid' => $modelTo->getStandModelGuid(),
            ]
        );
    }

    /**
     * @param string|null $modelGuid
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function deleteAction(?string $modelGuid): RedirectResponse
    {
        $modelTo = $this->guardModelGuid($modelGuid);

        try {
            $this->modelService->delete($modelTo);
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            throw new HttpException(500, 'Возникла ошибка при удалении файла модели');
        }

        return redirect()->route(
            'vrcci.stand-model.form',
            ['model_guid' => $modelTo->getStandModelGuid(),]
        );
    }

    /**
     * @param string $modelGuid
     *
     * @throws UnregisteredMappingException
     * @return ModelTo
     */
    protected function guardModelGuid(string $modelGuid): ModelTo
    {
        $modelTo = $this->modelRepository->find($modelGuid);
        if (empty($modelTo)) {
            throw new NotFoundHttpException('Файл модели не найден');
        }

        return $modelTo;
    }
}
