<?php

namespace Vrcci\Model\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Http\RedirectResponse;
use Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Vrcci\Api\Model\Http\Request\UploadTextureRequest;
use Vrcci\Model\Dto\TextureTo;
use Vrcci\Model\Eloquent\Repository\StandModelRepository;
use Vrcci\Model\Eloquent\Repository\TextureRepository;
use Vrcci\Model\Service\TextureFileService;

class TextureController extends Controller implements ModuleAliasAwareInstance
{
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;
    use GuardForStandModelGuidHelper;

    /**
     * @var TextureRepository
     */
    private $textureRepository;

    /**
     * @var TextureFileService
     */
    private $textureService;

    /**
     * @var StandModelRepository
     */
    protected $standModelRepository;

    /**
     * @param TextureRepository    $textureRepository
     * @param TextureFileService   $textureService
     * @param StandModelRepository $standModelRepository
     */
    public function __construct(
        TextureRepository $textureRepository,
        TextureFileService $textureService,
        StandModelRepository $standModelRepository
    ) {
        $this->textureRepository = $textureRepository;
        $this->textureService = $textureService;
        $this->standModelRepository = $standModelRepository;
    }

    /**
     * @param string               $standModelGuid
     * @param UploadTextureRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function uploadAction(string $standModelGuid, UploadTextureRequest $request): RedirectResponse
    {
        $standModel = $this->guardStandModelGuid($standModelGuid);

        if ($standModel->getOriginal()->textures()->count() >= 3) {
            return redirect()->route(
                'vrcci.stand-model.form',
                ['company_guid' => $standModel->getCompany(),]
            )->with('danger', ['Максимальное количество файлов текстур которое можно загрузить - 3']);
        }

        try {
            $this->textureService->create(
                $request->getFile(),
                $standModel->getOriginal(),
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            throw new HttpException(500, 'Возникла ошибка при загрузке файла текстуры');
        }

        return redirect()->route(
            'vrcci.stand-model.list',
            ['stand_model_guid' => $standModel->getCompany(),]
        );
    }

    /**
     * @param string               $textureGuid
     * @param UploadTextureRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function reuploadAction(string $textureGuid, UploadTextureRequest $request): RedirectResponse
    {
        $textureTo = $this->guardTextureGuid($textureGuid);

        try {
            $textureTo = $this->textureService->update(
                $textureTo,
                $request->getFile(),
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            throw new HttpException(500, 'Возникла ошибка при перезагрузке файла текстуры');
        }

        return redirect()->route(
            'vrcci.stand-model.list',
            [
                'stand_model_guid' => $textureTo->getStandModelGuid(),
            ]
        );
    }

    /**
     * @param string|null $textureGuid
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function deleteAction(?string $textureGuid): RedirectResponse
    {
        $textureTo = $this->guardTextureGuid($textureGuid);

        try {
            $this->textureService->delete($textureTo);
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            throw new HttpException(500, 'Возникла ошибка при удалении файла текстуры');
        }

        return redirect()->route(
            'vrcci.stand-model.form',
            ['model_guid' => $textureTo->getStandModelGuid(),]
        );
    }

    /**
     * @param string $textureGuid
     *
     * @throws UnregisteredMappingException
     * @return TextureTo
     */
    protected function guardTextureGuid(string $textureGuid): TextureTo
    {
        $textureTo = $this->textureRepository->find($textureGuid);
        if (empty($textureTo)) {
            throw new NotFoundHttpException('Файл текстуры не найден');
        }

        return $textureTo;
    }
}
