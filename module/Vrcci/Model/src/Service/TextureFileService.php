<?php

namespace Vrcci\Model\Service;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Http\UploadedFile;
use Vrcci\Model\Dto\TextureTo;
use Vrcci\Model\Eloquent\Entity\StandModel;
use Vrcci\Model\Eloquent\Entity\TextureFile;
use Vrcci\User\Dto\UserTo;

class TextureFileService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param UploadedFile $file
     * @param StandModel   $standModelTo
     * @param UserTo       $userTo
     *
     * @throws UnregisteredMappingException
     * @return TextureTo
     */
    public function create(UploadedFile $file, StandModel $standModel, UserTo $userTo): TextureTo
    {
        $texture = new TextureFile();
        $texture->standModel()->associate($standModel);
        $texture->setFile($file);

        $texture->uploadedBy()->associate($userTo->getOriginal());
        $texture->save();

        return $this->mapper->mapToObject($texture, new TextureTo());
    }

    /**
     * @param TextureTo    $textureTo
     * @param UploadedFile $file
     * @param UserTo       $userTo
     *
     * @throws UnregisteredMappingException
     * @return TextureTo
     */
    public function update(TextureTo $textureTo, UploadedFile $file, UserTo $userTo): TextureTo
    {
        $texture = $textureTo->getOriginal();
        $texture->setFile($file);

        $texture->uploadedBy()->associate($userTo->getOriginal());
        $texture->save();

        return $this->mapper->mapToObject($texture, $textureTo);
    }

    /**
     * @param TextureTo $textureTo
     *
     * @throws \Exception
     *
     * @return void
     */
    public function delete(TextureTo $textureTo): void
    {
        $textureTo->getOriginal()->delete();
    }
}
