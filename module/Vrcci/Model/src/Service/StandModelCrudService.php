<?php

namespace Vrcci\Model\Service;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Model\Dto\Attributes\ModelAttributesVo;
use Vrcci\Model\Dto\Attributes\UpdateModelAttributesVo;
use Vrcci\Model\Dto\StandModelTo;
use Vrcci\Model\Dto\TextureTo;
use Vrcci\Model\Eloquent\Entity\StandModel;
use Vrcci\Model\Eloquent\Entity\TextureFile;
use Vrcci\User\Dto\UserTo;

class StandModelCrudService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @var ModelFileService
     */
    private $modelFileService;

    /**
     * @var TextureFileService
     */
    private $textureFileService;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(
        AutoMapperInterface $mapper,
        ModelFileService $modelFileService,
        TextureFileService $textureFileService
    ) {
        $this->mapper = $mapper;
        $this->modelFileService = $modelFileService;
        $this->textureFileService = $textureFileService;
    }

    /**
     * @param ModelAttributesVo $attributesVo
     * @param CompanyTo         $companyTo
     * @param UserTo            $userTo
     *
     * @throws UnregisteredMappingException
     *
     * @return StandModelTo
     */
    public function create(ModelAttributesVo $attributesVo, CompanyTo $companyTo, UserTo $userTo): StandModelTo
    {
        $attributes = $attributesVo->toArray();
        $model = (new StandModel())->fill($attributes);
        $model->company()->associate($companyTo->getOriginal());
        $model->save();

        $this->modelFileService->create(
            $attributes['model_file'],
            $model,
            $userTo
        );

        foreach ($attributes['textures'] ?? [] as $texture) {
            $this->textureFileService->create(
                $texture,
                $model,
                $userTo
            );
        }

        $model->save();

        return $this->mapper->mapToObject($model, new StandModelTo());
    }

    /**
     * @param StandModelTo            $standModelTo
     * @param UpdateModelAttributesVo $attributesVo
     *
     * @throws UnregisteredMappingException
     * @return StandModelTo
     */
    public function update(
        StandModelTo $standModelTo,
        UpdateModelAttributesVo $attributesVo,
        UserTo $userTo
    ): StandModelTo {
        $standModel = $standModelTo->getOriginal();
        $attributes = $attributesVo->toArray();

        $modelFilesGuid = $standModelTo->getModelFile()->getGuid();
        if (!in_array($modelFilesGuid, $attributes['model_indexes']) && $attributes['model_file']) {
            $this->modelFileService->delete($standModelTo->getModelFile());
            $this->modelFileService->create(
                $attributes['model_file'],
                $standModel,
                $userTo
            );
        }

        /** @var TextureTo $texture */
        foreach ($standModelTo->getTextures() as $texture) {
            if (!in_array($texture->getGuid(), $attributes['texture_indexes'])) {
                $this->textureFileService->delete($texture);
            }
        }

        foreach ($attributes['textures'] as $textureFile) {
            $this->textureFileService->create(
                $textureFile,
                $standModel,
                $userTo
            );
        }

        $standModel->description = $attributes['description'] ?? '';
        $standModel->save();

        return $this->mapper->mapToObject($standModel, new StandModelTo());
    }

    /**
     * @param StandModelTo $standModelTo
     *
     * @throws \Exception
     *
     * @return void
     */
    public function delete(StandModelTo $standModelTo): void
    {
        $model = $standModelTo->getOriginal();
        $model->modelFile->delete();

        /** @var TextureFile $texture */
        foreach ($model->textures as $texture) {
            $texture->delete();
        }

        $model->delete();
    }
}
