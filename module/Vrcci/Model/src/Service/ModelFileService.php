<?php

namespace Vrcci\Model\Service;

use Vrcci\Model\Eloquent\Entity\ModelFile;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Http\UploadedFile;
use Vrcci\Model\Eloquent\Entity\StandModel;
use Vrcci\Model\Dto\ModelTo;
use Vrcci\User\Dto\UserTo;

class ModelFileService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param UploadedFile $file
     * @param StandModel   $standModel
     * @param UserTo       $userTo
     *
     * @throws UnregisteredMappingException
     * @return ModelTo
     */
    public function create(UploadedFile $file, StandModel $standModel, UserTo $userTo): ModelTo
    {
        $model = new ModelFile();
        $model->standModel()->associate($standModel);
        $model->setFile($file);

        $model->uploadedBy()->associate($userTo->getOriginal());
        $model->save();

        return $this->mapper->mapToObject($model, new ModelTo());
    }

    /**
     * @param ModelTo      $modelTo
     * @param UploadedFile $file
     * @param UserTo       $userTo
     *
     * @throws UnregisteredMappingException
     * @return ModelTo
     */
    public function update(ModelTo $modelTo, UploadedFile $file, UserTo $userTo): ModelTo
    {
        $model = $modelTo->getOriginal();
        $model->setFile($file);

        $model->uploadedBy()->associate($userTo->getOriginal());
        $model->save();

        return $this->mapper->mapToObject($model, $modelTo);
    }

    /**
     * @param ModelTo $modelTo
     *
     * @throws \Exception
     *
     * @return void
     */
    public function delete(ModelTo $modelTo): void
    {
        $modelTo->getOriginal()->delete();
    }
}
