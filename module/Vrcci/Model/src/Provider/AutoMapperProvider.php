<?php

namespace Vrcci\Model\Provider;

use App\Providers\AutoMapperProvider as BaseAutoMapperProvider;
use Illuminate\Support\ServiceProvider;
use Vrcci\Model\Eloquent\Mapper\ModelMapperFactory;
use Vrcci\Model\Eloquent\Mapper\StandModelFactory;
use Vrcci\Model\Eloquent\Mapper\TextureMapperFactory;

class AutoMapperProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app->tag(
            [
                ModelMapperFactory::class,
                TextureMapperFactory::class,
                StandModelFactory::class
            ],
            [BaseAutoMapperProvider::FACTORY_TAG]
        );
    }
}
