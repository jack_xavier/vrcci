<?php

namespace Vrcci\Model\Dto;

use App\Dto\StoresOriginalModel;
use Vrcci\Company\Dto\StoresCompanyToHelper;
use Vrcci\Model\Eloquent\Entity\StandModel;

/**
 * @method StandModel getOriginal()
 */
class StandModelTo
{
    use StoresOriginalModel, StoresCompanyToHelper;

    /**
     * @var string
     */
    private $guid = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var ModelTo
     */
    private $modelFile = null;

    /**
     * @var array|TextureTo
     */
    private $textures = [];

    /**
     * @var string
     */
    private $title = '';

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     *
     * @return StandModelTo
     */
    public function setGuid(string $guid): StandModelTo
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return StandModelTo
     */
    public function setDescription(string $description): StandModelTo
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return ModelTo|null
     */
    public function getModelFile(): ?ModelTo
    {
        return $this->modelFile;
    }

    /**
     * @param ModelTo|null $modelFile
     *
     * @return StandModelTo
     */
    public function setModelFile(?ModelTo $modelFile = null): StandModelTo
    {
        $this->modelFile = $modelFile;

        return $this;
    }

    /**
     * @return array|TextureTo[]
     */
    public function getTextures()
    {
        return $this->textures;
    }

    /**
     * @param array|TextureTo $textures
     *
     * @return StandModelTo
     */
    public function setTextures($textures)
    {
        $this->textures = $textures;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return StandModelTo
     */
    public function setTitle(string $title = ''): StandModelTo
    {
        $this->title = $title;

        return $this;
    }
}
