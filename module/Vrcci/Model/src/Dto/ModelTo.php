<?php

namespace Vrcci\Model\Dto;

use App\Dto\ContainsFileToTrait;
use App\Dto\StoresOriginalModel;
use Vrcci\Company\Dto\StoresCompanyToHelper;

class ModelTo
{
    use StoresOriginalModel, ContainsFileToTrait, StandModelGuidHelper;
}
