<?php

namespace Vrcci\Model\Dto;

use App\Dto\ContainsFileToTrait;
use App\Dto\StoresOriginalModel;

class TextureTo
{
    use StoresOriginalModel, ContainsFileToTrait, StandModelGuidHelper;
}
