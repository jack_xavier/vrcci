<?php

namespace Vrcci\Model\Dto;

trait StandModelGuidHelper
{
    /**
     * @var string
     */
    protected $standModelGuid = '';

    /**
     * @return string
     */
    public function getStandModelGuid(): string
    {
        return $this->standModelGuid;
    }

    /**
     * @param string $standModelGuid
     *
     * @return StandModelGuidHelper
     */
    public function setStandModelGuid(string $standModelGuid = '')
    {
        $this->standModelGuid = $standModelGuid;

        return $this;
    }

}
