<?php

namespace Vrcci\Model\Dto\Attributes;

use App\Dto\Attributes\AbstractAttributesObject;

class ModelAttributesVo extends AbstractAttributesObject
{
    /**
     * Returns available attribute names
     *
     * @return string[]
     */
    public static function attributes(): array
    {
        return [
            'model_file',
            'textures',
            'description'
        ];
    }
}
