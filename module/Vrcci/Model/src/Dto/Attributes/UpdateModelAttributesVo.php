<?php

namespace Vrcci\Model\Dto\Attributes;

use App\Dto\Attributes\AbstractAttributesObject;

class UpdateModelAttributesVo extends AbstractAttributesObject
{
    /**
     * Returns available attribute names
     *
     * @return string[]
     */
    public static function attributes(): array
    {
        return [
            'model_file',
            'model_indexes',
            'textures',
            'texture_indexes',
            'description',
        ];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $attributes = parent::toArray();
        $attributes['texture_indexes'] = $attributes['texture_indexes'] ?? [];
        $attributes['textures'] = $attributes['textures'] ?? [];
        $attributes['model_indexes'] = $attributes['model_indexes'] ?? [];

        return $attributes;
    }
}
