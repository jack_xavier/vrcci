<?php

namespace Vrcci\Model\Dto\Resource;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\Extractor\DefaultResourceExtractor;
use App\Http\Resource\Extractor\ExtractorInterface;
use Vrcci\Feed\Dto\FeedTo;

/**
 * @OA\Schema(
 *     schema="StandModel",
 *     @OA\Property(property="guid", type="string", description="Stand Model guid"),
 *     @OA\Property(property="descripition", type="string", description="The description of a stand model"),
 *     @OA\Property(property="model", type="file", description="Model file"),
 *     @OA\Property(
 *          property="textures",
 *          type="array",
 *          @OA\Items(type="string", format="binary")
 *     ),
 *  )
 */
class StandModelResource extends AbstractExtractableResource
{
    /**
     * @var string[]
     */
    protected $extractable = [
        'guid',
        'model',
        'description',
        'textures'
    ];

    /**
     * @param ExtractorInterface $extractor
     * @param FeedTo             $resource
     *
     * @return mixed[]
     */
    protected function extract(ExtractorInterface $extractor, $resource): array
    {
        if ($extractor instanceof DefaultResourceExtractor) {
            $extractor->properties($this->extractable);
            $extractor->withResources(['model','textures']);

        }

        $extractor->extract($resource);

        return $extractor->toArray();
    }
}
