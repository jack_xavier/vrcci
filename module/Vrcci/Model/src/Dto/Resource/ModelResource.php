<?php

namespace Vrcci\Model\Dto\Resources;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\Extractor\DefaultResourceExtractor;
use App\Http\Resource\Extractor\ExtractorInterface;
use Vrcci\Excursion\Dto\ExcursionTo;

/**
 * @OA\Schema(
 *     schema="Model",
 *     @OA\Property(property="guid", type="string", description="Model guid"),
 *     @OA\Property(property="url", type="string", description="File url"),
 *     @OA\Property(property="filename", type="string", description="File name"),
 *     ),
 * )
 */
class ModelResource extends AbstractExtractableResource
{
    /**
     * @var string[]
     */
    protected $extractable = [
        'guid',
        'url',
        'filename',
    ];

    /**
     * @param ExtractorInterface $extractor
     * @param ExcursionTo     $resource
     *
     * @return mixed[]
     */
    protected function extract(ExtractorInterface $extractor, $resource): array
    {
        if ($extractor instanceof DefaultResourceExtractor) {
            $extractor->properties($this->extractable);
        }

        $extractor->extract($resource);

        return $extractor->toArray();
    }
}
