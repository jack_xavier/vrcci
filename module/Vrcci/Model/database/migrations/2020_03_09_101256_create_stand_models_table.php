<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStandModelsTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create(
            'stand_models',
            function (Blueprint $table): void {
                $table->string('guid');
                $table->text('description')->nullable();
                $table->string('company_guid');
                $table->timestamps();

                $table->primary('guid');

                $table->foreign('company_guid')
                    ->references('guid')
                    ->on('companies')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('stand_models');
    }
}
