<?php

use App\Entity\GuidGenerator;
use Vrcci\Model\Http\Controller\ModelController;
use Vrcci\Model\Http\Controller\StandModelController;
use Vrcci\Model\Http\Controller\TextureController;

Route::middleware('auth:web')
    ->prefix('/stand-models')
    ->name('vrcci.stand-model.')
    ->group(
        function (): void {
            Route::get('/{company_guid}/create-form/', StandModelController::class . '@createFormAction')
                ->name('create-form')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{company_guid}/update-form/{stand_model_guid}', StandModelController::class . '@updateFormAction')
                ->name('update-form')
                ->where('company_guid', GuidGenerator::GUID_PATTERN)
                ->where('stand_model_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{company_guid}/list', StandModelController::class . '@listAction')
                ->name('list')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{stand_model_guid}/update', StandModelController::class . '@updateAction')
                ->name('update')
                ->where('stand_model_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{company_guid}/create', StandModelController::class . '@createAction')
                ->name('create')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{stand_model_guid}/delete', StandModelController::class . '@deleteAction')
                ->name('delete')
                ->where('stand_model_guid', GuidGenerator::GUID_PATTERN);
        }
    );

Route::middleware('auth:web')
    ->prefix('/model-textures')
    ->name('vrcci.texture.')
    ->group(
        function (): void {
            Route::post('/{stand_model_guid}/upload', TextureController::class . '@uploadAction')
                ->name('update')
                ->where('stand_model_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{texture_guid}/delete', StandModelController::class . '@deleteAction')
                ->name('delete')
                ->where('texture_guid', GuidGenerator::GUID_PATTERN);
        }
    );

Route::middleware('auth:web')
    ->prefix('/models')
    ->name('vrcci.model.')
    ->group(
        function (): void {
            Route::post('/{stand_model_guid}/upload', ModelController::class . '@uploadAction')
                ->name('update')
                ->where('stand_model_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{model_guid}/delete', ModelController::class . '@deleteAction')
                ->name('delete')
                ->where('model_guid', GuidGenerator::GUID_PATTERN);
        }
    );
