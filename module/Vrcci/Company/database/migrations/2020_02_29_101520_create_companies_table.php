<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create(
            'companies',
            function (Blueprint $table): void {
                $table->string('guid');
                $table->string('title');
                $table->string('bm_link')->nullable();
                $table->unsignedInteger('created_by_id');
                $table->timestamps();

                $table->primary('guid');

                $table->foreign('created_by_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            }


        );
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('companies');
    }
}
