<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanySettingsTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create(
            'company_settings',
            function (Blueprint $table): void {
                $table->string('guid');
                $table->string('company_email')->nullable();
                $table->string('website_link')->nullable();
                $table->string('fb_link')->nullable();
                $table->string('vk_link')->nullable();
                $table->string('instagram_link')->nullable();
                $table->string('company_guid');
                $table->timestamps();

                $table->primary('guid');

                $table->foreign('company_guid')
                    ->references('guid')
                    ->on('companies')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('company_settings');
    }
}
