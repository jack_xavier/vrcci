<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompanyPreferences extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::table(
            'companies',
            function (Blueprint $table): void {
                $table->json('preferences')->nullable();
            }
        );
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::table(
            'companies',
            function (Blueprint $table): void {
                $table->dropColumn('preferencies');
            }
        );
    }
}
