<?php

namespace Vrcci\Company\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Company\Http\Request\CompanyPreferencesEditRequest;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Company\Service\CompanyCrudService;

class CompanyPreferencesController extends Controller implements ModuleAliasAwareInstance
{
    use GuardForCompanyGuidTrait;
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;

    /**
     * @var CompanyCrudService
     */
    private $companyService;

    /**
     * @param CompanyRepository  $companyRepository
     * @param CompanyCrudService $companyService
     */
    public function __construct(CompanyRepository $companyRepository, CompanyCrudService $companyService)
    {
        $this->companyRepository = $companyRepository;
        $this->companyService = $companyService;
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     * @return View
     */
    public function formAction(string $companyGuid): View
    {
        $companyTo = $this->guardCompanyGuid($companyGuid);

        return $this->makeView('preferences-form')
            ->with('company', $companyTo);
    }

    /**
     * @param string                        $companyGuid
     * @param CompanyPreferencesEditRequest $request
     *
     * @return RedirectResponse
     */
    public function updateAction(
        string $companyGuid,
        CompanyPreferencesEditRequest $request
    ): RedirectResponse {
        try {
            $this->companyService->updatePreferences(
                $this->guardCompanyGuid($companyGuid),
                $request->toArray()
            );
        } catch (\Throwable $throwable) {
            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при обновлении настроек компании',
                ]
            );
        }

        return redirect()->route('vrcci.companies.list');
    }
}
