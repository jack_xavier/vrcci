<?php

namespace Vrcci\Company\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Company\Http\Request\CompanySettingsEditRequest;
use Vrcci\Company\Dto\CompanySettingsTo;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Company\Eloquent\Repository\CompanySettingsRepository;
use Vrcci\Company\Service\CompanySettingsCrudService;

class CompanySettingsCrudController extends Controller implements ModuleAliasAwareInstance
{
    use GuardForCompanyGuidTrait;
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;

    /**
     * @var CompanySettingsCrudService
     */
    private $service;

    /**
     * @var CompanySettingsRepository
     */
    private $repository;

    /**
     * @param CompanySettingsCrudService $service
     * @param CompanySettingsRepository  $repository
     * @param CompanyRepository          $companyRepository
     */
    public function __construct(
        CompanySettingsCrudService $service,
        CompanySettingsRepository $repository,
        CompanyRepository $companyRepository
    ) {
        $this->service = $service;
        $this->repository = $repository;
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     * @return View
     */
    public function formAction(string $companyGuid): View
    {
        $companyTo = $this->guardCompanyGuid($companyGuid);
        $settingsTo = $this->guardCompanySettingsGuid($companyTo);

        return $this->makeView('settings-form')
            ->with('company', $companyTo)
            ->with('settings', $settingsTo);
    }

    /**
     * @param string                     $companyGuid
     * @param CompanySettingsEditRequest $request
     *
     * @throws UnregisteredMappingException
     * @return View
     */
    public function updateAction(string $companyGuid, CompanySettingsEditRequest $request): RedirectResponse
    {
        $companyTo = $this->guardCompanyGuid($companyGuid);
        $settingsTo = $this->guardCompanySettingsGuid($companyTo);

        try {
            $this->service->update(
                $settingsTo,
                $request->getAttributes()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при измении E-mail и сайтов компании.',
                ]
            );
        }

        return redirect()->route(
            'vrcci.company-settings.get',
            [
                'company_guid' => $companyTo->getGuid(),
            ]
        );
    }

    /**
     * @param CompanyTo $companyTo
     *
     * @return CompanySettingsTo
     */
    protected function guardCompanySettingsGuid(CompanyTo $companyTo): CompanySettingsTo
    {
        $companySettingsTo = $this->repository
            ->fetchAll()
            ->byCompanyGuid($companyTo->getGuid())
            ->first();

        if (empty($companySettingsTo)) {
            throw new NotFoundHttpException('Настроек компании не найдено');
        }

        return $companySettingsTo;
    }
}
