<?php

namespace Vrcci\Company\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Company\Http\Request\CompanyEditRequest;
use Vrcci\Api\Company\Http\Request\CompanyFilterRequest;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Company\Service\CompanyCrudService;

class CompanyCrudController extends Controller implements ModuleAliasAwareInstance
{
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;
    use GuardForCompanyGuidTrait;

    /**
     * @var CompanyCrudService
     */
    private $companyService;

    /**
     * @param CompanyCrudService $companyService
     * @param CompanyRepository  $companyRepository
     */
    public function __construct(CompanyCrudService $companyService, CompanyRepository $companyRepository)
    {
        $this->companyService = $companyService;
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param CompanyEditRequest $request
     *
     * @throws HttpException
     * @return RedirectResponse
     */
    public function createAction(CompanyEditRequest $request): RedirectResponse
    {
        try {
            $companyTo = $this->companyService->create($request->getAttributes(), $this->getCurrentUser());
        } catch (Throwable $throwable) {
            Log::error($throwable);

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при создании компании.',
                ]
            );
        }

        return redirect()->route('vrcci.companies.list');
    }

    /**
     * @param string             $guid
     * @param CompanyEditRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function updateAction(string $guid, CompanyEditRequest $request): RedirectResponse
    {
        $companyTo = $this->guardCompanyGuid($guid);

        try {
            $companyTo = $this->companyService->update($companyTo, $request->getAttributes());
        } catch (Throwable $throwable) {
            Log::error($throwable);

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при обновлении компании.',
                ]
            );
        }

        return redirect()->route('vrcci.companies.list');
    }

    /**
     * @param string $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return RedirectResponse
     */
    public function deleteAction(string $guid): RedirectResponse
    {
        $companyTo = $this->guardCompanyGuid($guid);

        try {
            $this->companyService->delete($companyTo);
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при удалении компании.',
                ]
            );
        }

        return redirect()->route('vrcci.companies.list');
    }

    /**
     * @param string|null|null $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return View
     */
    public function formAction(?string $guid = null): View
    {
        $companyTo = $guid
            ? $this->guardCompanyGuid($guid)
            : null;

        return $this->makeView('edit-form')
            ->with('company', $companyTo);
    }

    /**
     * @param CompanyFilterRequest $request
     *
     * @return View
     */
    public function listAction(CompanyFilterRequest $request): View
    {
        $dataRequest = $this->companyRepository
            ->fetchAll();

        if (null !== $title = $request->getTitle()) {
            $dataRequest->byTitleLike($request->getTitle());
        }

        $paginator = $dataRequest->withPage(
            $request->getPage(),
            $request::VAR_DEFAULT_LIMIT
        );

        $accessToken = $this->getCurrentUser()->getOriginal()->accessToken->token;

        return $this->makeView('list')
            ->with('companies', $paginator)
            ->with('access_token', $accessToken);
    }
}
