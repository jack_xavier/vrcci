<?php

namespace Vrcci\Company\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Throwable;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Company\Http\Request\CompanyBLRequest;
use Vrcci\Company\Dto\Resources\CompanyResource;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Company\Service\CompanyCrudService;

class CompanyBMLinkController extends Controller implements ModuleAliasAwareInstance
{
    use GuardForCompanyGuidTrait;
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;

    /**
     * @var CompanyCrudService
     */
    private $companyService;

    /**
     * @param CompanyRepository  $companyRepository
     * @param CompanyCrudService $companyService
     */
    public function __construct(CompanyRepository $companyRepository, CompanyCrudService $companyService)
    {
        $this->companyRepository = $companyRepository;
        $this->companyService = $companyService;
    }

    /**
     * @param string|null|null $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return View
     */
    public function formAction(?string $guid = null): View
    {
        $companyTo = $guid
            ? $this->guardCompanyGuid($guid)
            : null;

        return $this->makeView('bmlink-form')
            ->with('company', $companyTo);
    }

    /**
     * @param string           $companyGuid
     * @param CompanyBLRequest $request
     *
     * @throws UnregisteredMappingException
     * @return CompanyResource
     */
    public function updateAction(string $companyGuid, CompanyBLRequest $request): RedirectResponse
    {
        try {
            $companyTo = $this->companyService->attachBMLink(
                $this->guardCompanyGuid($companyGuid),
                $request->getBMLink()
            );
        } catch (Throwable $throwable) {
            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при обновлении ссылки на Бизнес-Маркет.',
                ]
            );
        }

        return redirect()->route('vrcci.company-bmlink.get', ['company_guid' => $companyTo->getGuid()]);
    }
}
