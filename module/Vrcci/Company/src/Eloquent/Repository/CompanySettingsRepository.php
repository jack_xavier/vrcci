<?php

namespace Vrcci\Company\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Company\Dto\CompanySettingsTo;
use Vrcci\Company\Eloquent\DataRequest\CompanySettingsDataRequest;
use Vrcci\Company\Eloquent\Entity\CompanySettings;

class CompanySettingsRepository
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return CompanySettingsDataRequest
     */
    public function fetchAll(): CompanySettingsDataRequest
    {
        $dataRequest = CompanySettingsDataRequest::create(CompanySettings::with(['company']));
        $dataRequest->withTransformer(
            function (CompanySettings $company) {
                return $this->mapper->mapToObject($company, new CompanySettingsTo());
            }
        );

        return $dataRequest;
    }

    /**
     * @param string $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return CompanySettingsTo|null
     */
    public function find(string $guid): ?CompanySettingsTo
    {
        return $this->fetchAll()->byGuid($guid)->first();
    }
}
