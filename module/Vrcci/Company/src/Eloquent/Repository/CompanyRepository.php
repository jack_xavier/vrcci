<?php

namespace Vrcci\Company\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Company\Eloquent\DataRequest\CompanyDataRequest;
use Vrcci\Company\Eloquent\Entity\Company;

class CompanyRepository
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return CompanyDataRequest
     */
    public function fetchAll(): CompanyDataRequest
    {
        $dataRequest = CompanyDataRequest::create(Company::with([]));
        $dataRequest->withTransformer(
            function (Company $company) {
                return $this->mapper->mapToObject($company, new CompanyTo());
            }
        );

        return $dataRequest;
    }

    /**
     * @param string $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return CompanyTo|null
     */
    public function find(string $guid): ?CompanyTo
    {
        return $this->fetchAll()->byGuid($guid)->first();
    }
}
