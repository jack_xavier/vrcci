<?php

namespace Vrcci\Company\Eloquent\DataRequest;

use App\Eloquent\AbstractDataRequest;

class CompanyDataRequest extends AbstractDataRequest
{
    /**
     * @param string $companyId
     *
     * @return CompanyDataRequest
     */
    public function byGuid(string $companyId): self
    {
        $this->qb->where('guid', $companyId);

        return $this;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function byTitleLike(string $title): self
    {
        $this->qb->where(
            'title',
            'like',
            sprintf(
                '%%%s%%',
                $title
            )
        );

        return $this;
    }

    public function byToken(string $token)
    {

    }
}
