<?php

namespace Vrcci\Company\Eloquent\DataRequest;

use App\Eloquent\AbstractDataRequest;

class CompanySettingsDataRequest extends AbstractDataRequest
{
    /**
     * @param string $companyGuid
     *
     * @return CompanySettingsDataRequest
     *
     */
    public function byGuid(string $companyGuid): self
    {
        $this->qb->where('guid', $companyGuid);

        return $this;
    }

    /**
     * @param string $companyGuid
     *
     * @return $this
     */
    public function byCompanyGuid(string $companyGuid): self
    {
        $this->qb->where('company_guid', $companyGuid);

        return $this;
    }
}
