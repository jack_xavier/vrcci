<?php

namespace Vrcci\Company\Eloquent\Mapper;

use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\MappingFactoryInterface;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\Company\Dto\CompanyPreferencesTo;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Company\Eloquent\Entity\Company;

class CompanyMapperFactory implements MappingFactoryInterface
{
    /**
     * @param AutoMapperConfig $config
     *
     * @return void
     */
    public function registerMapping(AutoMapperConfig $config): void
    {
        $config->registerMapping(Company::class, CompanyTo::class)
            ->forMember('originalModel', new MapOriginal())
            ->forMember(
                'preferences',
                function (Company $company) {
                    return (new CompanyPreferencesTo())->fromArray($company->preferences);
                }
            );
    }
}
