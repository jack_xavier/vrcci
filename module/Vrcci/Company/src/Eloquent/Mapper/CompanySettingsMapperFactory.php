<?php

namespace Vrcci\Company\Eloquent\Mapper;

use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\MappingFactoryInterface;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\Company\Dto\CompanySettingsTo;
use Vrcci\Company\Eloquent\Entity\CompanySettings;

class CompanySettingsMapperFactory implements MappingFactoryInterface
{
    /**
     * @param AutoMapperConfig $config
     *
     * @return void
     */
    public function registerMapping(AutoMapperConfig $config): void
    {
        $config->registerMapping(CompanySettings::class, CompanySettingsTo::class)
            ->forMember('originalModel', new MapOriginal());
    }
}
