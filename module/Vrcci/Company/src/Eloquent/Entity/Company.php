<?php

namespace Vrcci\Company\Eloquent\Entity;

use App\Entity\File;
use App\Entity\GuidModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Storage;
use Vrcci\Certificate\Eloquent\Entity\Certificate;
use Vrcci\Excursion\Eloquent\Entity\Excursion;
use Vrcci\Feed\Eloquent\Entity\Feed;
use Vrcci\Model\Eloquent\Entity\StandModel;
use Vrcci\Presentation\Eloquent\Entity\Presentation;
use Vrcci\Sphere360\Eloquent\Entity\Sphere360;
use Vrcci\User\Entity\User;

/**
 * @property string $title
 * @property string $bm_link
 * @property array $preferences
 */
class Company extends GuidModel
{
    /**
     * @var string
     */
    protected $table = 'companies';

    /**
     * @var string[]
     */
    protected $fillable = [
        'title',
        'bm_link',
        'preferences'
    ];

    protected $casts = [
        'preferences' => 'array',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by_id', 'id');
    }

    public function models(): HasMany
    {
        return $this->hasMany(StandModel::class, 'company_guid', 'guid');
    }

    public function feed(): HasMany
    {
        return $this->hasMany(Feed::class, 'company_guid', 'guid');
    }

    public function certificates(): HasMany
    {
        return $this->hasMany(Certificate::class, 'company_guid', 'guid');
    }

    public function spheres(): HasMany
    {
        return $this->hasMany(Sphere360::class, 'company_guid', 'guid');
    }

    public function presentations(): HasMany
    {
        return $this->hasMany(Presentation::class, 'company_guid', 'guid');
    }

    public function excursions(): HasMany
    {
        return $this->hasMany(Excursion::class, 'company_guid', 'guid');
    }

    public function settings(): HasOne
    {
        return $this->hasOne(CompanySettings::class, 'company_guid', 'guid');
    }

    /**
     * @throws \Exception
     *
     * @return bool|null
     */
    public function delete()
    {
        if (!Storage::disk(File::VAR_DEFAULT_STORAGE_DISK)->exists($this->title)) {
            Storage::disk(File::VAR_DEFAULT_STORAGE_DISK)->deleteDirectory($this->title);
        }

        return parent::delete();
    }
}
