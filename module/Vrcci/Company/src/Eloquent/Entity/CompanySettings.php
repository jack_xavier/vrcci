<?php

namespace Vrcci\Company\Eloquent\Entity;

use App\Entity\GuidModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
/**
 * @property string $company_email
 * @property string $website_link
 * @property string $fb_link
 * @property string $vk_link
 * @property string $instagram_link
 */
class CompanySettings extends GuidModel
{
    /**
     * @var string
     */
    protected $table = 'company_settings';

    /**
     * @var string[]
     */
    protected $fillable = [
        'company_email',
        'website_link',
        'fb_link',
        'vk_link',
        'instagram_link'
    ];

    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_guid', 'guid');
    }
}
