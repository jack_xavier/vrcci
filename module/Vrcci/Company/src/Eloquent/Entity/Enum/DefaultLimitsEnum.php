<?php

namespace Vrcci\Company\Eloquent\Entity\Enum;

class DefaultLimitsEnum
{
    public const DEFAULT_CERTIFICATES = 3;
    public const DEFAULT_CERTIFICATE_FILE_LIMIT = 2048;
    public const DEFAULT_PRESENTATIONS = 3;
    public const DEFAULT_PRESENTATION_FILE_LIMIT = 20480;
    public const DEFAULT_MODELS = 2;
    public const DEFAULT_MODEL_FILE_LIMIT = 2048;
    public const DEFAULT_TEXTURES = 3;
    public const DEFAULT_TEXTURE_FILE_LIMIT = 30720;
    public const DEFAULT_SPHERES360 = 1;
    public const DEFAULT_SPHERES360_FILE_LIMIT = 40960;
    public const DEFAULT_EXCURSION = 1;
    public const DEFAULT_EXCURSION_FILE_LIMIT = 5;
}
