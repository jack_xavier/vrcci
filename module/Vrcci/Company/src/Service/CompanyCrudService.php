<?php

namespace Vrcci\Company\Service;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Exception;
use Vrcci\Company\Dto\Attributes\CompanyAttributesVo;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Company\Eloquent\Entity\Company;
use Vrcci\Company\Eloquent\Entity\CompanySettings;
use Vrcci\User\Dto\UserTo;

class CompanyCrudService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param CompanyAttributesVo $attributes
     * @param UserTo              $user
     *
     * @throws UnregisteredMappingException
     *
     * @return CompanyTo
     */
    public function create(CompanyAttributesVo $attributes, UserTo $user): CompanyTo
    {
        $company = (new Company())->fill($attributes->toArray());
        $company->user()->associate($user->getOriginal());
        $company->save();

        $companySettings = new CompanySettings();
        $companySettings->company()->associate($company);
        $companySettings->save();

        return $this->mapper->mapToObject($company, new CompanyTo());
    }

    /**
     * @param CompanyTo           $companyTo
     * @param CompanyAttributesVo $attributes
     *
     * @throws UnregisteredMappingException
     *
     * @return CompanyTo
     */
    public function update(CompanyTo $companyTo, CompanyAttributesVo $attributes): CompanyTo
    {
        $original = $companyTo->getOriginal();
        $original->fill($attributes->toArray());
        $original->save();

        return $this->mapper->mapToObject($original, $companyTo);
    }

    /**
     * @param CompanyTo $companyTo
     *
     * @throws Exception
     *
     * @return void
     */
    public function delete(CompanyTo $companyTo): void
    {
        $companyTo->getOriginal()->delete();
    }

    /**
     * @param CompanyTo $companyTo
     * @param string    $bmLink
     *
     * @throws UnregisteredMappingException
     * @return mixed
     */
    public function attachBMLink(CompanyTo $companyTo, ?string $bmLink): CompanyTo
    {
        $original = $companyTo->getOriginal();
        $original->bm_link = $bmLink;
        $original->save();

        return $this->mapper->mapToObject($original, $companyTo);
    }

    /**
     * @param CompanyTo $companyTo
     *
     * @throws UnregisteredMappingException
     * @return mixed
     */
    public function detachBMLink(CompanyTo $companyTo): CompanyTo
    {
        $original = $companyTo->getOriginal();
        $original->bm_link = null;
        $original->save();

        return $this->mapper->mapToObject($original, $companyTo);
    }

    /**
     * @param CompanyTo $companyTo
     * @param array     $preferencies
     *
     * @throws UnregisteredMappingException
     * @return CompanyTo
     */
    public function updatePreferences(CompanyTo $companyTo, array $preferencies): CompanyTo
    {
        $original = $companyTo->getOriginal();

        $original->preferences = $preferencies;
        $original->save();

        return $this->mapper->mapToObject($original, $companyTo);
    }
}
