<?php

namespace Vrcci\Company\Service;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Exception;
use Vrcci\Company\Dto\Attributes\CompanyAttributesVo;
use Vrcci\Company\Dto\Attributes\CompanySettingsAttributesVo;
use Vrcci\Company\Dto\CompanySettingsTo;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Company\Eloquent\Entity\CompanySettings;

class CompanySettingsCrudService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param CompanySettingsTo   $companySettingsTo
     * @param CompanyAttributesVo $attributes
     *
     * @throws UnregisteredMappingException
     *
     * @return CompanyTo
     */
    public function update(
        CompanySettingsTo $companySettingsTo,
        CompanySettingsAttributesVo $attributes
    ): CompanySettingsTo {
        $original = $companySettingsTo->getOriginal();
        $original->fill($attributes->toArray());
        $original->save();

        return $this->mapper->mapToObject($original, $companySettingsTo);
    }

    /**
     * @param CompanyTo $companySettingsTo
     *
     * @throws Exception
     *
     * @return void
     */
    public function delete(CompanySettingsTo $companySettingsTo): void
    {
        $companySettingsTo->getOriginal()->delete();
    }
}
