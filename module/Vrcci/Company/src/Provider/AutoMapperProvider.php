<?php

namespace Vrcci\Company\Providers;

use App\Providers\AutoMapperProvider as BaseAutoMapperProvider;
use Illuminate\Support\ServiceProvider;
use Vrcci\Company\Eloquent\Mapper\CompanyMapperFactory;
use Vrcci\Company\Eloquent\Mapper\CompanySettingsMapperFactory;

class AutoMapperProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app->tag(
            [CompanyMapperFactory::class, CompanySettingsMapperFactory::class],
            [BaseAutoMapperProvider::FACTORY_TAG]
        );
    }
}
