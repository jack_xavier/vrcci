<?php

namespace Vrcci\Company\Dto;

trait StoresCompanyToHelper
{
    /**
     * @var CompanyTo
     */
    protected $company = null;

    /**
     * @return CompanyTo
     */
    public function getCompany(): CompanyTo
    {
        return $this->company;
    }

    /**
     * @param CompanyTo $company
     *
     * @return self
     */
    public function setCompany(?CompanyTo $company)
    {
        $this->company = $company;

        return $this;
    }
}
