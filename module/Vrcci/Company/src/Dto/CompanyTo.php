<?php

namespace Vrcci\Company\Dto;

use App\Dto\StoresOriginalModel;
use Vrcci\Company\Eloquent\Entity\Company;

/**
 * @method Company getOriginal()
 */
class CompanyTo
{
    use StoresOriginalModel;

    /**
     * @var string
     */
    private $guid = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $bmLink = '';

    /**
     * @var CompanyPreferencesTo
     */
    private $preferences = null;

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     *
     * @return CompanyTo
     */
    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return CompanyTo
     */
    public function setTitle(string $title = ''): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getBmLink(): string
    {
        return $this->bmLink;
    }

    /**
     * @param string $bmLink
     *
     * @return CompanyTo
     */
    public function setBmLink(string $bmLink = ''): CompanyTo
    {
        $this->bmLink = $bmLink;

        return $this;
    }

    /**
     * @return CompanyPreferencesTo
     */
    public function getPreferences(): CompanyPreferencesTo
    {
        return $this->preferences;
    }

    /**
     * @param CompanyPreferencesTo $preferences
     *
     * @return CompanyTo
     */
    public function setPreferences(CompanyPreferencesTo $preferences = null): CompanyTo
    {
        $this->preferences = $preferences;

        return $this;
    }
}
