<?php

namespace Vrcci\Company\Dto\Resources;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\Extractor\DefaultResourceExtractor;
use App\Http\Resource\Extractor\ExtractorInterface;
use Vrcci\Company\Dto\CompanyTo;

/**
 * @OA\Schema(
 *     schema="Company",
 *     @OA\Property(property="guid", type="string", description="Company GUID"),
 *     @OA\Property(property="title", type="string", description="Company name"),
 *     ),
 * )
 */
class CompanyResource extends AbstractExtractableResource
{
    /**
     * @var string[]
     */
    protected $extractable = [
        'guid',
        'title',
    ];

    /**
     * @param ExtractorInterface $extractor
     * @param CompanyTo          $resource
     *
     * @return mixed[]
     */
    protected function extract(ExtractorInterface $extractor, $resource): array
    {
        if ($extractor instanceof DefaultResourceExtractor) {
            $extractor->properties($this->extractable);
        }

        $extractor->extract($resource);

        return $extractor->toArray();
    }
}
