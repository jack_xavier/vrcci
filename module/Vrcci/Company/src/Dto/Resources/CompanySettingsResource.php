<?php

namespace Vrcci\Company\Dto\Resources;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\Extractor\DefaultResourceExtractor;
use App\Http\Resource\Extractor\ExtractorInterface;
use Vrcci\Company\Dto\CompanySettingsTo;

/**
 * @OA\Schema(
 *     schema="CompanySettings",
 *     @OA\Property(property="guid", type="string", description="Company GUID"),
 *     @OA\Property(property="company_email", type="string", description="Company email"),
 *     @OA\Property(property="website_link", type="string", description="Company link"),
 *     @OA\Property(property="fb_link", type="string", description="Company Facebook link"),
 *     @OA\Property(property="vk_link", type="string", description="Company VK link"),
 *     @OA\Property(property="instagram_link", type="string", description="Company Instagram link"),
 *     ),
 * )
 */
class CompanySettingsResource extends AbstractExtractableResource
{
    /**
     * @var string[]
     */
    protected $extractable = [
        'guid',
        'companyEmail',
        'websiteLink',
        'fbLink',
        'vkLink',
        'instagramLink',
    ];

    /**
     * @param ExtractorInterface $extractor
     * @param CompanySettingsTo  $resource
     *
     * @return mixed[]
     */
    protected function extract(ExtractorInterface $extractor, $resource): array
    {
        if ($extractor instanceof DefaultResourceExtractor) {
            $extractor->properties($this->extractable);
        }

        $extractor->extract($resource);

        return $extractor->toArray();
    }
}
