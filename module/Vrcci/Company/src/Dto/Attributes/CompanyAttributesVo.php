<?php

namespace Vrcci\Company\Dto\Attributes;

use App\Dto\Attributes\AbstractAttributesObject;

class CompanyAttributesVo extends AbstractAttributesObject
{
    /**
     * Returns available attribute names
     *
     * @return string[]
     */
    public static function attributes(): array
    {
        return [
            'title',
        ];
    }
}
