<?php

namespace Vrcci\Company\Dto\Attributes;

use App\Dto\Attributes\AbstractAttributesObject;

class CompanySettingsAttributesVo extends AbstractAttributesObject
{
    /**
     * Returns available attribute names
     *
     * @return string[]
     */
    public static function attributes(): array
    {
        return [
            'company_email',
            'website_link',
            'fb_link',
            'vk_link',
            'instagram_link',
        ];
    }
}
