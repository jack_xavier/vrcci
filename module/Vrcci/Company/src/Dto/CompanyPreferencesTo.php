<?php

namespace Vrcci\Company\Dto;

use Vrcci\Company\Eloquent\Entity\Enum\DefaultLimitsEnum;

class CompanyPreferencesTo
{
    /**
     * @var int
     */
    private $certificates;

    /**
     * @var int
     */
    private $certificatesFileLimit;

    /**
     * @var int
     */
    private $presentations;

    /**
     * @var int
     */
    private $presentationsFileLimit;

    /**
     * @var int
     */
    private $models;

    /**
     * @var int
     */
    private $modelsFileLimit;

    /**
     * @var int
     */
    private $textures;

    /**
     * @var int
     */
    private $texturesFileLimit;

    /**
     * @var int
     */
    private $spheres360;

    /**
     * @var int
     */
    private $spheres360FileLimit;

    /**
     * @var int
     */
    private $excursion;

    /**
     * @var int
     */
    private $excursionFileLimit;

    /**
     * @return int
     */
    public function getCertificates(): int
    {
        return $this->certificates;
    }

    /**
     * @param int $certificates
     *
     * @return CompanyPreferencesTo
     */
    public function setCertificates(int $certificates): CompanyPreferencesTo
    {
        $this->certificates = $certificates;

        return $this;
    }

    /**
     * @return int
     */
    public function getCertificatesFileLimit(): int
    {
        return $this->certificatesFileLimit;
    }

    /**
     * @param int $certificatesFileLimit
     *
     * @return CompanyPreferencesTo
     */
    public function setCertificatesFileLimit(int $certificatesFileLimit): CompanyPreferencesTo
    {
        $this->certificatesFileLimit = $certificatesFileLimit;

        return $this;
    }

    /**
     * @return int
     */
    public function getPresentations(): int
    {
        return $this->presentations;
    }

    /**
     * @param int $presentations
     *
     * @return CompanyPreferencesTo
     */
    public function setPresentations(int $presentations): CompanyPreferencesTo
    {
        $this->presentations = $presentations;

        return $this;
    }

    /**
     * @return int
     */
    public function getPresentationsFileLimit(): int
    {
        return $this->presentationsFileLimit;
    }

    /**
     * @param int $presentationsFileLimit
     *
     * @return CompanyPreferencesTo
     */
    public function setPresentationsFileLimit(int $presentationsFileLimit): CompanyPreferencesTo
    {
        $this->presentationsFileLimit = $presentationsFileLimit;

        return $this;
    }

    /**
     * @return int
     */
    public function getModels(): int
    {
        return $this->models;
    }

    /**
     * @param int $models
     *
     * @return CompanyPreferencesTo
     */
    public function setModels(int $models): CompanyPreferencesTo
    {
        $this->models = $models;

        return $this;
    }

    /**
     * @return int
     */
    public function getModelsFileLimit(): int
    {
        return $this->modelsFileLimit;
    }

    /**
     * @param int $modelsFileLimit
     *
     * @return CompanyPreferencesTo
     */
    public function setModelsFileLimit(int $modelsFileLimit): CompanyPreferencesTo
    {
        $this->modelsFileLimit = $modelsFileLimit;

        return $this;
    }

    /**
     * @return int
     */
    public function getTextures(): int
    {
        return $this->textures;
    }

    /**
     * @param int $textures
     *
     * @return CompanyPreferencesTo
     */
    public function setTextures(int $textures): CompanyPreferencesTo
    {
        $this->textures = $textures;

        return $this;
    }

    /**
     * @return int
     */
    public function getTexturesFileLimit(): int
    {
        return $this->texturesFileLimit;
    }

    /**
     * @param int $texturesFileLimit
     *
     * @return CompanyPreferencesTo
     */
    public function setTexturesFileLimit(int $texturesFileLimit): CompanyPreferencesTo
    {
        $this->texturesFileLimit = $texturesFileLimit;

        return $this;
    }

    /**
     * @return int
     */
    public function getSpheres360(): int
    {
        return $this->spheres360;
    }

    /**
     * @param int $spheres360
     *
     * @return CompanyPreferencesTo
     */
    public function setSpheres360(int $spheres360): CompanyPreferencesTo
    {
        $this->spheres360 = $spheres360;

        return $this;
    }

    /**
     * @return int
     */
    public function getSpheres360FileLimit(): int
    {
        return $this->spheres360FileLimit;
    }

    /**
     * @param int $spheres360FileLimit
     *
     * @return CompanyPreferencesTo
     */
    public function setSpheres360FileLimit(int $spheres360FileLimit): CompanyPreferencesTo
    {
        $this->spheres360FileLimit = $spheres360FileLimit;

        return $this;
    }

    /**
     * @return int
     */
    public function getExcursion(): int
    {
        return $this->excursion;
    }

    /**
     * @param int $excursion
     *
     * @return CompanyPreferencesTo
     */
    public function setExcursion(int $excursion): CompanyPreferencesTo
    {
        $this->excursion = $excursion;

        return $this;
    }

    /**
     * @return int
     */
    public function getExcursionFileLimit(): int
    {
        return $this->excursionFileLimit;
    }

    /**
     * @param int $excursionFileLimit
     *
     * @return CompanyPreferencesTo
     */
    public function setExcursionFileLimit(int $excursionFileLimit): CompanyPreferencesTo
    {
        $this->excursionFileLimit = $excursionFileLimit;

        return $this;
    }

    /**
     * @param array $array
     *
     * @return $this
     */
    public function fromArray(?array $array = []): self
    {
        $this->setCertificates($array['certificates'] ?? DefaultLimitsEnum::DEFAULT_CERTIFICATES);
        $this->setCertificatesFileLimit(
            $array['certificatesFileLimit'] ?? DefaultLimitsEnum::DEFAULT_CERTIFICATE_FILE_LIMIT
        );

        $this->setPresentations($array['presentations'] ?? DefaultLimitsEnum::DEFAULT_PRESENTATIONS);
        $this->setPresentationsFileLimit(
            $array ['presentationsFileLimit'] ?? DefaultLimitsEnum::DEFAULT_PRESENTATION_FILE_LIMIT
        );

        $this->setModels($array['models'] ?? DefaultLimitsEnum::DEFAULT_MODELS);
        $this->setModelsFileLimit(
            $array['modelsFileLimit'] ?? DefaultLimitsEnum::DEFAULT_MODEL_FILE_LIMIT
        );

        $this->setTextures($array['textures'] ?? DefaultLimitsEnum::DEFAULT_TEXTURES);
        $this->setTexturesFileLimit(
            $array['texturesFileLimit'] ?? DefaultLimitsEnum::DEFAULT_TEXTURE_FILE_LIMIT
        );

        $this->setSpheres360($array['spheres360'] ?? DefaultLimitsEnum::DEFAULT_SPHERES360);
        $this->setSpheres360FileLimit(
            $array['spheres360FileLimit'] ?? DefaultLimitsEnum::DEFAULT_SPHERES360_FILE_LIMIT
        );

        $this->setExcursion($array['excursion'] ?? DefaultLimitsEnum::DEFAULT_EXCURSION);
        $this->setExcursionFileLimit(
            $array['excursionFileLimit'] ?? DefaultLimitsEnum::DEFAULT_EXCURSION_FILE_LIMIT
        );

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'certificates' => $this->certificates,
            'certificatesFileLimit' => $this->certificatesFileLimit,
            'presentations' => $this->presentations,
            'presentationsFileLimit' => $this->presentationsFileLimit,
            'models' => $this->models,
            'modelsFileLimit' => $this->modelsFileLimit,
            'textures' => $this->textures,
            'texturesFileLimit' => $this->texturesFileLimit,
            'spheres360' => $this->spheres360,
            'spheres360FileLimit' => $this->spheres360FileLimit,
            'excursion' => $this->excursion,
            'excursionFileLimit' => $this->excursionFileLimit,
        ];
    }

    /**
     * @param string $keyWord
     *
     * @return int[]
     */
    public function getByKeyWord(string $keyWord): array
    {
        $result = [];

        foreach ($this->toArray() as $key => $item) {
            if ($key === $keyWord) {
                $result['totalLimit'] = $item;
                continue;
            }

            $limitKey = sprintf('%sFileLimit', $keyWord);
            if ($limitKey === $key) {
                $result['totalAmount'] = $item;
                continue;
            }
        }

        return $result;
    }
}



