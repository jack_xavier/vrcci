<?php

namespace Vrcci\Company\Dto;

use App\Dto\StoresOriginalModel;
use Vrcci\Company\Eloquent\Entity\Company;

/**
 * @method Company getOriginal()
 */
class CompanySettingsTo
{
    use StoresOriginalModel;

    /**
     * @var string
     */
    private $guid;

    /**
     * @var string
     */
    private $companyEmail = '';

    /**
     * @var string
     */
    private $websiteLink = '';

    /**
     * @var string
     */
    private $fbLink = '';

    /**
     * @var string
     */
    private $vkLink = '';

    /**
     * @var string
     */
    private $instagramLink = '';

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     *
     * @return CompanySettingsTo
     */
    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyEmail(): string
    {
        return $this->companyEmail;
    }

    /**
     * @param string $companyEmail
     *
     * @return CompanySettingsTo
     */
    public function setCompanyEmail(?string $companyEmail): CompanySettingsTo
    {
        $this->companyEmail = $companyEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getWebsiteLink(): ?string
    {
        return $this->websiteLink;
    }

    /**
     * @param string $websiteLink
     *
     * @return CompanySettingsTo
     */
    public function setWebsiteLink(?string $websiteLink): CompanySettingsTo
    {
        $this->websiteLink = $websiteLink;

        return $this;
    }

    /**
     * @return string
     */
    public function getFbLink(): ?string
    {
        return $this->fbLink;
    }

    /**
     * @param string $fbLink
     *
     * @return CompanySettingsTo
     */
    public function setFbLink(?string $fbLink): CompanySettingsTo
    {
        $this->fbLink = $fbLink;

        return $this;
    }

    /**
     * @return string
     */
    public function getVkLink(): ?string
    {
        return $this->vkLink;
    }

    /**
     * @param string $vkLink
     *
     * @return CompanySettingsTo
     */
    public function setVkLink(?string $vkLink): CompanySettingsTo
    {
        $this->vkLink = $vkLink;

        return $this;
    }

    /**
     * @return string
     */
    public function getInstagramLink(): ?string
    {
        return $this->instagramLink;
    }

    /**
     * @param string $instagramLink
     *
     * @return CompanySettingsTo
     */
    public function setInstagramLink(?string $instagramLink): CompanySettingsTo
    {
        $this->instagramLink = $instagramLink;

        return $this;
    }
}
