<?php

use App\Entity\GuidGenerator;
use Vrcci\Company\Http\Controller\CompanyBMLinkController;
use Vrcci\Company\Http\Controller\CompanyCrudController;
use Vrcci\Company\Http\Controller\CompanyPreferencesController;
use Vrcci\Company\Http\Controller\CompanySettingsCrudController;

Route::middleware('auth:web')
    ->prefix('/companies')
    ->name('vrcci.companies.')
    ->group(
        function (): void {
            Route::get('/{company_guid?}', CompanyCrudController::class . '@formAction')
                ->name('form')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/list', CompanyCrudController::class . '@listAction')
                ->name('list');
            Route::post('/{company_guid}/update', CompanyCrudController::class . '@updateAction')
                ->name('update')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/create', CompanyCrudController::class . '@createAction')
                ->name('create');
            Route::get('/{company_guid}/delete', CompanyCrudController::class . '@deleteAction')
                ->name('delete')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
        }
    );

Route::middleware('auth:web')
    ->prefix('/company-settings')
    ->name('vrcci.company-settings.')
    ->group(
        function (): void {
            Route::get('/get/{company_guid}/form', CompanySettingsCrudController::class . '@formAction')
                ->name('get')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{company_guid}/update', CompanySettingsCrudController::class . '@updateAction')
                ->name('set')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
        }
    );

Route::middleware('auth:web')
    ->prefix('/company-bm-link')
    ->name('vrcci.company-bmlink.')
    ->group(
        function (): void {
            Route::get('/{company_guid}/form', CompanyBMLinkController::class . '@formAction')
                ->name('get')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{company_guid}/update', CompanyBMLinkController::class . '@updateAction')
                ->name('set')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
        }
    );

Route::middleware('auth:web')
    ->prefix('/company-preferences')
    ->name('vrcci.company-preferences.')
    ->group(
        function (): void {
            Route::get('/{company_guid}/form', CompanyPreferencesController::class . '@formAction')
                ->name('get')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{company_guid}/update', CompanyPreferencesController::class . '@updateAction')
                ->name('set')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
        }
    );
