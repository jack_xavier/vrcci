<?php

namespace Vrcci\Api\Company\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\Company\Http\Request\CompanySettingsEditRequest;
use Vrcci\Api\Company\Http\Request\CompanySettingsFilterRequest;
use Vrcci\Company\Dto\CompanySettingsTo;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Company\Dto\Resources\CompanyResource;
use Vrcci\Company\Dto\Resources\CompanySettingsResource;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Company\Eloquent\Repository\CompanySettingsRepository;
use Vrcci\Company\Service\CompanySettingsCrudService;

class CompanySettingsCrudController extends Controller
{
    use CurrentUserTrait, GuardForCompanyGuidTrait;

    /**
     * @var CompanySettingsCrudService
     */
    private $service;

    /**
     * @var CompanySettingsRepository
     */
    private $repository;

    /**
     * @var CompanyRepository
     */
    protected $companyRepository;

    /**
     * @var ResourcesMap
     */
    private $resourcesMap;

    /**
     * @param CompanySettingsCrudService $service
     * @param CompanySettingsRepository  $repository
     * @param CompanyRepository          $companyRepository
     * @param ResourcesMap               $resourcesMap
     */
    public function __construct(
        CompanySettingsCrudService $service,
        CompanySettingsRepository $repository,
        CompanyRepository $companyRepository,
        ResourcesMap $resourcesMap
    ) {
        $this->service = $service;
        $this->repository = $repository;
        $this->companyRepository = $companyRepository;
        $this->resourcesMap = $resourcesMap;
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     *
     * @return CompanyResource
     *
     * @OA\Get(
     *   path="/api/company-settings/",
     *   description="Get Company settings",
     *   operationId="company-settings-get",
     *   tags={"company-settings"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          ref="#/components/schemas/CompanySettings"
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Company was not found"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function getAction(CompanySettingsFilterRequest $request): CompanySettingsResource
    {
        $company = $this->guardCompanyGuid($request->getCompanyGuid());
        $settings = $this->guardCompanySettingsGuid($company);

        /** @var CompanySettingsResource $resource */
        $resource = $this->resourcesMap->resource($settings);

        return $resource;
    }

    /**
     * @param CompanySettingsEditRequest $request
     *
     * @throws UnregisteredMappingException
     *
     * @return CompanySettingsResource
     *
     * @OA\Put(
     *   path="/api/company-settings/",
     *   description="Set company settings",
     *   operationId="company-settings-set",
     *   tags={"company-settings"},
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\JsonContent(
     *        ref="#/components/schemas/CompanySettingsEditRequest"
     *      )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/CompanySettings"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Company settings successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Company is not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function setAction(CompanySettingsEditRequest $request): CompanySettingsResource
    {
        $companyTo = $this->guardCompanyGuid($request->getCompanyGuid());
        $settingsTo = $this->guardCompanySettingsGuid($companyTo);

        $settingsTo = $this->service->update(
            $settingsTo,
            $request->getAttributes()
        );

        /** @var CompanySettingsResource $resource */
        $resource = $this->resourcesMap->resource($settingsTo);
        $resource->isUpdatedResource();
        $resource->withMessage('Company settings successfully updated');

        return $resource;
    }

    /**
     * @param CompanyTo $companyTo
     *
     * @throws AccessDeniedHttpException
     * @throws NotFoundHttpException
     * @throws UnregisteredMappingException
     *
     * @return CompanySettingsTo
     */
    protected function guardCompanySettingsGuid(CompanyTo $companyTo): CompanySettingsTo
    {
        $companySettingsTo = $this->repository
            ->fetchAll()
            ->byCompanyGuid($companyTo->getGuid())
            ->first();

        if (empty($companySettingsTo)) {
            throw new NotFoundHttpException('Company Settings were not found');
        }

        return $companySettingsTo;
    }
}
