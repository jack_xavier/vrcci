<?php

namespace Vrcci\Api\Company\Http\Controller;

use AutoMapperPlus\Exception\UnregisteredMappingException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;

trait GuardForCompanyGuidTrait
{
    /**
     * @var CompanyRepository
     */
    protected $companyRepository;

    /**
     * @param string $companyGuid
     *
     * @throws AccessDeniedHttpException
     * @throws NotFoundHttpException
     * @throws UnregisteredMappingException
     *
     * @return CompanyTo
     */
    protected function guardCompanyGuid(string $companyGuid): CompanyTo
    {
        $companyTo = $this->companyRepository->find($companyGuid);
        if (empty($companyTo)) {
            throw new NotFoundHttpException('Company was not found');
        }

        return $companyTo;
    }
}
