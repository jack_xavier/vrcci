<?php

namespace Vrcci\Api\Company\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Auth\AuthenticationException;
use Vrcci\Api\Company\Http\Request\CompanyBLRequest;
use Vrcci\Company\Dto\Resources\CompanyResource;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Company\Service\CompanyCrudService;

class CompanyBLController extends Controller
{
    use CurrentUserTrait, GuardForCompanyGuidTrait;

    /**
     * @var ResourcesMap
     */
    private $resourcesMap;

    /**
     * @var CompanyRepository
     */
    protected $companyRepository;

    /**
     * @var CompanyCrudService
     */
    private $companyService;

    /**
     * @param ResourcesMap       $resourcesMap
     * @param CompanyRepository  $companyRepository
     * @param CompanyCrudService $companyService
     */
    public function __construct(
        ResourcesMap $resourcesMap,
        CompanyRepository $companyRepository,
        CompanyCrudService $companyService
    ) {
        $this->resourcesMap = $resourcesMap;
        $this->companyRepository = $companyRepository;
        $this->companyService = $companyService;
    }

    /**
     * @param string           $companyGuid
     * @param CompanyBLRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return CompanyResource
     *
     * @OA\Patch(
     *   path="/api/company-bm-link/{company_guid}/attach",
     *   description="Attach BL to a company",
     *   operationId="company-attach-bm-link",
     *   tags={"Companies"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\JsonContent(
     *        ref="#/components/schemas/CompanyBLRequest"
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Company"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Company successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Company was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function attachAction(string $companyGuid, CompanyBLRequest $request): CompanyResource
    {
        $companyTo = $this->companyService->attachBMLink(
            $this->guardCompanyGuid($companyGuid),
            $request->getBMLink()
        );

        /** @var CompanyResource $companyResource */
        $companyResource = $this->resourcesMap->resource($companyTo);

        return $companyResource;
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return CompanyResource
     *
     * @OA\Patch(
     *   path="/api/company-bm-link/{company_guid}/detach",
     *   description="Detach BL to a company",
     *   operationId="company-detach-bm-link",
     *   tags={"Companies"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Company"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Company successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Company was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function detachAction(string $companyGuid): CompanyResource
    {
        $companyTo = $this->companyService->detachBMLink(
            $this->guardCompanyGuid($companyGuid)
        );

        /** @var CompanyResource $companyResource */
        $companyResource = $this->resourcesMap->resource($companyTo);

        return $companyResource;
    }
}
