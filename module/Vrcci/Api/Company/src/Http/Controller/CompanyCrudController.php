<?php

namespace Vrcci\Api\Company\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourceCollection;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\Company\Http\Request\CompanyEditRequest;
use Vrcci\Api\Company\Http\Request\CompanyFilterRequest;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Company\Dto\Resources\CompanyResource;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Company\Service\CompanyCrudService;

class CompanyCrudController extends Controller
{
    use CurrentUserTrait;

    /**
     * @var CompanyCrudService
     */
    private $service;

    /**
     * @var CompanyRepository
     */
    private $repository;

    /**
     * @var ResourcesMap
     */
    private $resourcesMap;

    /**
     * @param CompanyCrudService $service
     * @param CompanyRepository  $repository
     * @param ResourcesMap       $resourcesMap
     */
    public function __construct(
        CompanyCrudService $service,
        CompanyRepository $repository,
        ResourcesMap $resourcesMap
    ) {
        $this->service = $service;
        $this->repository = $repository;
        $this->resourcesMap = $resourcesMap;
    }

    /**
     * @param CompanyFilterRequest $request
     *
     * @throws AuthenticationException
     * @throws UnregisteredMappingException
     *
     * @return ResourceCollection
     *
     * @OA\Get(
     *   path="/api/companies",
     *   description="Get company list",
     *   operationId="company-list",
     *   tags={"Companies"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="title",
     *     in="query",
     *     required=false,
     *     description="Company name",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/Company"
     *              )
     *          )
     *        ),
     *        @OA\Property(
     *          property="links",
     *          ref="#/components/schemas/CollectionLinks"
     *        ),
     *        @OA\Property(
     *          property="meta",
     *          ref="#/components/schemas/CollectionMeta"
     *        ),
     *     )
     *   ),
     *   @OA\Response(response=500, description="Internal server error"),
     *   @OA\Response(response=404, description="Company was not found"),
     * )
     */
    public function listAction(CompanyFilterRequest $request): ResourceCollection
    {
        $dataRequest = $this->repository
            ->fetchAll();

        if (null !== $title = $request->getTitle()) {
            $dataRequest->byTitleLike($request->getTitle());
        }

        $companies = $dataRequest->paginate($request->get('limit', 30));

        return $this->resourcesMap->collection($companies);
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     *
     * @return CompanyResource
     *
     * @OA\Get(
     *   path="/api/companies/{company_guid}",
     *   description="Get Company",
     *   operationId="company-find",
     *   tags={"Companies"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          ref="#/components/schemas/Company"
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Company was not found"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function findAction(string $companyGuid): CompanyResource
    {
        $companyTo = $this->guardCompanyId($companyGuid);
        /** @var CompanyResource $companyResource */
        $companyResource = $this->resourcesMap->resource($companyTo);

        return $companyResource;
    }

    /**
     * @param CompanyEditRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return CompanyResource
     *
     * @OA\Post(
     *   path="/api/companies",
     *   description="Create company",
     *   operationId="company-create",
     *   tags={"Companies"},
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\JsonContent(
     *        ref="#/components/schemas/CompanyEditRequest"
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Company"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Company successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Company was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function createAction(CompanyEditRequest $request): CompanyResource
    {
        $companyTo = $this->service->create($request->getAttributes(), $this->getCurrentUser());

        /** @var CompanyResource $resource */
        $resource = $this->resourcesMap->resource($companyTo);
        $resource->isCreatedResource();
        $resource->withMessage('Company successfully created');

        return $resource;
    }

    /**
     * @param string             $companyGuid
     * @param CompanyEditRequest $request
     *
     * @throws UnregisteredMappingException
     *
     * @return CompanyResource
     *
     * @OA\Put(
     *   path="/api/companies/{company_guid}",
     *   description="Update company",
     *   operationId="company-update",
     *   tags={"Companies"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\JsonContent(
     *        ref="#/components/schemas/CompanyEditRequest"
     *      )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Company"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Company successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Company is not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function updateAction(string $companyGuid, CompanyEditRequest $request): CompanyResource
    {
        $company = $this->guardCompanyId($companyGuid);

        $company = $this->service->update(
            $company,
            $request->getAttributes()
        );

        /** @var CompanyResource $resource */
        $resource = $this->resourcesMap->resource($company);
        $resource->isUpdatedResource();
        $resource->withMessage('Company successfully updated');

        return $resource;
    }

    /**
     * @param string $companyGuid
     *
     * @throws Exception
     * @throws ConflictHttpException
     *
     * @return CompanyResource
     *
     * @OA\Delete(
     *   path="/api/companies/{company_guid}",
     *   description="Delete company",
     *   operationId="company-delete",
     *   tags={"Companies"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Company"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Company successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Company is not found"),
     *   @OA\Response(response=409, description="Conflict"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function deleteAction(string $companyGuid): CompanyResource
    {
        $companyTo = $this->guardCompanyId($companyGuid);

        $this->service->delete($companyTo);

        /** @var CompanyResource $resource */
        $resource = $this->resourcesMap->resource($companyTo);
        $resource->isDeletedResource();
        $resource->withMessage('Company successfully deleted');

        return $resource;
    }

    /**
     * @param string $companyGuid
     *
     * @throws AccessDeniedHttpException
     * @throws NotFoundHttpException
     * @throws UnregisteredMappingException
     *
     * @return CompanyTo
     */
    protected function guardCompanyId(string $companyGuid): CompanyTo
    {
        $companyTo = $this->repository->find($companyGuid);
        if (empty($companyTo)) {
            throw new NotFoundHttpException('Company was not found');
        }

        return $companyTo;
    }
}
