<?php

namespace Vrcci\Api\Company\Http\Request;

/**
 * @OA\Schema(
 *     schema="CompanySettingsFilterRequest",
 *     @OA\Property(property="company_guid", type="string", description="Company GUID"),
 * )
 */
class CompanySettingsFilterRequest extends CompanyGuidFilterRequest
{
}
