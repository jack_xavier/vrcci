<?php

namespace Vrcci\Api\Company\Http\Request;

use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *     schema="CompanyPreferencesEditRequest",
 *     @OA\Property(property="certificates", type="integer"),
 *     @OA\Property(property="certificatesFileLimit", type="integer"),
 *     @OA\Property(property="presentations", type="integer"),
 *     @OA\Property(property="presentationsFileLimit", type="integer"),
 *     @OA\Property(property="models", type="integer"),
 *     @OA\Property(property="modelsFileLimit", type="integer"),
 *     @OA\Property(property="textures", type="integer"),
 *     @OA\Property(property="texturesFileLimit", type="integer"),
 *     @OA\Property(property="spheres360", type="integer"),
 *     @OA\Property(property="spheres360FileLimit", type="integer"),
 *     @OA\Property(property="excursion", type="integer"),
 *     @OA\Property(property="excursionFileLimit", type="integer"),
 * )
 */
class CompanyPreferencesEditRequest extends FormRequest
{
    use AuthorizedRequest;

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            'certificates' => ['required', 'integer', 'min:1'],
            'certificatesFileLimit' => ['required', 'integer', 'min:1'],
            'presentations' => ['required', 'integer', 'min:1'],
            'presentationsFileLimit' => ['required', 'integer', 'min:1'],
            'models' => ['required', 'integer', 'min:1'],
            'modelsFileLimit' => ['required', 'integer', 'min:1'],
            'textures' => ['required', 'integer', 'min:1'],
            'texturesFileLimit' => ['required', 'integer', 'min:1'],
            'spheres360' => ['required', 'integer', 'min:1'],
            'spheres360FileLimit' => ['required', 'integer', 'min:1'],
            'excursion' => ['required', 'integer', 'min:1'],
            'excursionFileLimit' => ['required', 'integer', 'min:1'],
        ];
    }

    public function toArray(): array
    {
        $attributes = parent::toArray();

        $multiplierMap = [
            'certificatesFileLimit',
            'presentationsFileLimit',
            'modelsFileLimit',
            'texturesFileLimit',
            'spheres360FileLimit',
        ];

        foreach ($attributes as $key => $attribute) {
            if (in_array($key, $multiplierMap)) {
                $attributes[$key] *= 1024;
            }
        }

        return $attributes;
    }
}
