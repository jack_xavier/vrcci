<?php

namespace Vrcci\Api\Company\Http\Request;

use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *     schema="CompanyFilterRequest",
 *     @OA\Property(property="title", type="string", description="Company name"),
 * )
 */
class CompanyFilterRequest extends FormRequest
{
    use AuthorizedRequest;

    public const VAR_DEFAULT_LIMIT = 100;

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            'title' => ['string'],
        ];
    }

    public function messages(): array
    {
        return [
            'title.string' => 'Необходимо ввести символы в поисковую строку.',
        ];
    }

    /**
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->get('page');
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->get('title');
    }
}
