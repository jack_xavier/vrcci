<?php

namespace Vrcci\Api\Company\Http\Request;

use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *     schema="CompanyBLRequest",
 *     @OA\Property(property="bmLink", type="string", description="Company bussiness link"),
 * )
 */
class CompanyBLRequest extends FormRequest
{
    use AuthorizedRequest;

    public const VALUE_BM_LINK = 'bmLink';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::VALUE_BM_LINK => ['nullable', 'string', 'url', 'max:128'],
        ];
    }

    /**
     * @return mixed
     */
    public function getBMLink(): ?string
    {
        return $this->get(self::VALUE_BM_LINK);
    }
}
