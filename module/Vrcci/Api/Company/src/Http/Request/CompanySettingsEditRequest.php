<?php

namespace Vrcci\Api\Company\Http\Request;

use App\Dto\Attributes\AbstractAttributesObject;
use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Vrcci\Company\Dto\Attributes\CompanySettingsAttributesVo;

/**
 * @OA\Schema(
 *     schema="CompanySettingsEditRequest",
 *     @OA\Property(property="companyGuid", type="string", description="Company GUID"),
 *     @OA\Property(property="companyEmail", type="string", description="Company email"),
 *     @OA\Property(property="websiteLink", type="string", description="Company website link"),
 *     @OA\Property(property="fbLink", type="string", description="Company fb link"),
 *     @OA\Property(property="vkLink", type="string", description="Company vk link"),
 *     @OA\Property(property="instagramLink", type="string", description="Company instagram link"),
 * )
 */
class CompanySettingsEditRequest extends FormRequest
{
    use AuthorizedRequest;

    public const INPUT_COMPANY_GUID = 'companyGuid';
    public const INPUT_EMAIL = 'companyEmail';
    public const INPUT_WEBSITE_LINK = 'websiteLink';
    public const INPUT_FB_LINK = 'fbLink';
    public const INPUT_VK_LINK = 'vkLink';
    public const INPUT_INSTAGRAM_LINK = 'instagramLink';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::INPUT_EMAIL => ['string', 'max:128'],
            self::INPUT_WEBSITE_LINK => ['nullable', 'string', 'url', 'max:128'],
            self::INPUT_FB_LINK => ['nullable','string', 'url', 'max:128'],
            self::INPUT_VK_LINK => ['nullable','string', 'url', 'max:128'],
            self::INPUT_INSTAGRAM_LINK => ['nullable','string', 'url', 'max:128'],
            self::INPUT_COMPANY_GUID => [
                Rule::exists('companies', 'guid'),
            ],
        ];
    }

    /**
     * @return CompanySettingsAttributesVo|AbstractAttributesObject
     */
    public function getAttributes(): CompanySettingsAttributesVo
    {
        return CompanySettingsAttributesVo::fromArray($this->validated());
    }

    /**
     * @return string|null
     */
    public function getCompanyGuid(): ?string
    {
        return $this->get(self::INPUT_COMPANY_GUID);
    }

    /**
     * @return array
     */
    public function messages() : array
    {
        return
            array_merge(
                parent::messages(),
                [
                    'string' => 'Поле ":attribute" не может быть пустым.',
                ]
            );
    }
}
