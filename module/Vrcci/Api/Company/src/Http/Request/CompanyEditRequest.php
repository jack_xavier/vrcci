<?php

namespace Vrcci\Api\Company\Http\Request;

use App\Dto\Attributes\AbstractAttributesObject;
use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Vrcci\Company\Dto\Attributes\CompanyAttributesVo;

/**
 * @OA\Schema(
 *     schema="CompanyEditRequest",
 *     @OA\Property(property="title", type="string", description="Company name"),
 * )
 */
class CompanyEditRequest extends FormRequest
{
    use AuthorizedRequest;

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'max:256', 'min:3'],
        ];
    }

    /**
     * @return CompanyAttributesVo|AbstractAttributesObject
     */
    public function getAttributes(): AbstractAttributesObject
    {
        return CompanyAttributesVo::fromArray($this->validated());
    }
}
