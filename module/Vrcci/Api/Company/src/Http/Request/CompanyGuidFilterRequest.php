<?php

namespace Vrcci\Api\Company\Http\Request;

use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;

class CompanyGuidFilterRequest extends FormRequest
{
    use AuthorizedRequest;

    public const INPUT_COMPANY_GUID = 'company_guid';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::INPUT_COMPANY_GUID => [
                'required',
                'string',
                Rule::exists('companies', 'guid'),
            ],
        ];
    }

    /**
     * @return UploadedFile|null
     */
    public function getCompanyGuid(): ?string
    {
        return $this->get(self::INPUT_COMPANY_GUID);
    }
}
