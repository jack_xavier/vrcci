<?php

use Vrcci\Api\Company\Http\Controller\CompanyBLController;
use Vrcci\Api\Company\Http\Controller\CompanyCrudController;
use Vrcci\Api\Company\Http\Controller\CompanySettingsCrudController;

Route::prefix('/companies')
    ->name('api.companies.')
    ->middleware('auth:api')
    ->middleware('auth:access_token')
    ->group(
        function (): void {
            Route::get('/', CompanyCrudController::class . '@listAction')
                ->name('list');
            Route::get('/{company_guid}', CompanyCrudController::class . '@findAction')
                ->name('find');
            Route::post('/', CompanyCrudController::class . '@createAction')
                ->name('create');
            Route::put('/{company_guid}', CompanyCrudController::class . '@updateAction')
                ->name('update');
            Route::delete('/{company_guid}', CompanyCrudController::class . '@deleteAction')
                ->name('delete');
        }
    );

Route::prefix('/company-settings')
    ->name('api.company-settings.')
    ->middleware('auth:api')
    ->group(
        function (): void {
            Route::get('/', CompanySettingsCrudController::class . '@getAction')
                ->name('get');
            Route::put('/', CompanySettingsCrudController::class . '@setAction')
                ->name('set');
        }
    );

Route::prefix('/company-bm-link')
    ->name('api.company-settings.')
    ->middleware('auth:api')
    ->group(
        function (): void {
            Route::patch('/{company_guid}/attach', CompanyBLController::class . '@attachAction')
                ->name('attach');
            Route::patch('/{company_guid}/detach', CompanyBLController::class . '@detachAction')
                ->name('detach');
        }
    );
