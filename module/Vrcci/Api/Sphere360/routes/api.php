<?php

use Vrcci\Api\Sphere360\Http\Controller\Sphere360CrudController;

Route::prefix('/spheres360')
    ->name('api.spheres360.')
    ->middleware('auth:api')
    ->group(
        function (): void {
            Route::get('/', Sphere360CrudController::class . '@listAction')
                ->name('list');
            Route::post('/', Sphere360CrudController::class . '@createAction')
                ->name('create');
            Route::put('/{sphere360_guid}', Sphere360CrudController::class . '@updateAction')
                ->name('update');
            Route::delete('/{sphere360_guid}', Sphere360CrudController::class . '@deleteAction')
                ->name('delete');
        }
    );
