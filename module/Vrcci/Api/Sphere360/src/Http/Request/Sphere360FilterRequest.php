<?php

namespace Vrcci\Api\Sphere360\Http\Request;

use Vrcci\Api\Company\Http\Request\CompanyGuidFilterRequest;

/**
 * @OA\Schema(
 *     schema="Sphere360FilterRequest",
 *     @OA\Property(property="company_guid", type="string", description="Company GUID"),
 * )
 */
class Sphere360FilterRequest extends CompanyGuidFilterRequest
{
}
