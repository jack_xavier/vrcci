<?php

namespace Vrcci\Api\Sphere360\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourceCollection;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Sphere360\Http\Request\Sphere360FilterRequest;
use Vrcci\Api\Sphere360\Http\Request\UploadSphere360Request;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Sphere360\Dto\Resources\Sphere360Resource;
use Vrcci\Sphere360\Dto\Sphere360To;
use Vrcci\Sphere360\Eloquent\Repository\Sphere360Repository;
use Vrcci\Sphere360\Service\Sphere360CrudService;

class Sphere360CrudController extends Controller
{
    use CurrentUserTrait,
        GuardForCompanyGuidTrait;

    /**
     * @var Sphere360Repository
     */
    protected $sphere360Repository;

    /**
     * @var Sphere360CrudService
     */
    protected $sphereService;

    /**
     * @var CompanyRepository
     */
    protected $companyRepository;

    /**
     * @var ResourcesMap
     */
    protected $resourcesMap;

    /**
     * @param Sphere360Repository  $sphere360Repository
     * @param Sphere360CrudService $sphere360Service
     * @param CompanyRepository    $companyRepository
     * @param ResourcesMap         $resourcesMap
     */
    public function __construct(
        Sphere360Repository $sphere360Repository,
        Sphere360CrudService $sphere360Service,
        CompanyRepository $companyRepository,
        ResourcesMap $resourcesMap
    ) {
        $this->sphere360Repository = $sphere360Repository;
        $this->sphereService = $sphere360Service;
        $this->companyRepository = $companyRepository;
        $this->resourcesMap = $resourcesMap;
    }

    /**
     * @param Sphere360FilterRequest $filterRequest
     *
     * @throws AuthenticationException
     * @throws UnregisteredMappingException
     *
     * @return ResourceCollection
     *
     * @OA\Get(
     *   path="/api/spheres360",
     *   description="Get sphere360s list",
     *   operationId="sphere360-list",
     *   tags={"Spheres"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="query",
     *     required=true,
     *     description="Company Guid",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/Sphere360"
     *              )
     *          )
     *        ),
     *        @OA\Property(
     *          property="links",
     *          ref="#/components/schemas/CollectionLinks"
     *        ),
     *        @OA\Property(
     *          property="meta",
     *          ref="#/components/schemas/CollectionMeta"
     *        ),
     *     )
     *   ),
     *   @OA\Response(response=500, description="Internal server error"),
     * )
     */
    public function listAction(Sphere360FilterRequest $filterRequest): ResourceCollection
    {
        $dataRequest = $this->sphere360Repository
            ->fetchAll()
            ->byCompanyGuid($filterRequest->getCompanyGuid());

        $sphere360s = $dataRequest->paginate(30);

        return $this->resourcesMap->collection($sphere360s);
    }

    /**
     * @param UploadSphere360Request $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return Sphere360Resource
     *
     * @OA\Post(
     *   path="/api/spheres360",
     *   description="Create sphere360",
     *   operationId="sphere360-create",
     *   tags={"Spheres"},
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadSphere360Request"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Sphere360"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Sphere360 successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Sphere360 was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function createAction(UploadSphere360Request $request): Sphere360Resource
    {
        $sphere360To = $this->sphereService->create(
            $request->getFile(),
            $this->guardCompanyGuid($request->getCompanyGuid()),
            $this->getCurrentUser()
        );

        /** @var Sphere360Resource $resource */
        $resource = $this->resourcesMap->resource($sphere360To);
        $resource->isCreatedResource();
        $resource->withMessage('Sphere360 was successfully created');

        return $resource;
    }

    /**
     * @param string                 $sphere360Id
     * @param UploadSphere360Request $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return Sphere360Resource
     *
     * @OA\Put(
     *   path="/api/spheres360",
     *   description="Update sphere360",
     *   operationId="sphere360-update",
     *   tags={"Spheres"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="sphere360_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadSphere360Request"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Sphere360"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Sphere360 successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Sphere360 is not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function updateAction(string $sphere360Id, UploadSphere360Request $request): Sphere360Resource
    {
        $sphere360To = $this->sphereService->update(
            $this->guardForSphere360Id($sphere360Id),
            $request->getFile(),
            $this->getCurrentUser()
        );

        /** @var Sphere360Resource $resource */
        $resource = $this->resourcesMap->resource($sphere360To);
        $resource->isUpdatedResource();
        $resource->withMessage('Sphere360 was successfully updated');

        return $resource;
    }

    /**
     * @param string $sphere360Id
     *
     * @throws ConflictHttpException
     * @throws UnregisteredMappingException
     *
     * @return Sphere360Resource
     *
     * @OA\Delete(
     *   path="/api/spheres360/{sphere360_guid}",
     *   description="Delete sphere360",
     *   operationId="sphere360-delete",
     *   tags={"Spheres"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="sphere360_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Sphere360"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Sphere360 successfully deleted")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Sphere360 was not found"),
     *   @OA\Response(response=409, description="Conflict"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function deleteAction(string $sphere360Id): Sphere360Resource
    {
        $this->sphereService->delete(
            $sphere360To = $this->guardForSphere360Id($sphere360Id)
        );

        /** @var Sphere360Resource $resource */
        $resource = $this->resourcesMap->resource($sphere360To);
        $resource->isDeletedResource();
        $resource->withMessage('Sphere360 was successfully deleted');

        return $resource;
    }

    /**
     * @param string $sphere360Id
     *
     * @throws UnregisteredMappingException
     *
     * @return Sphere360To
     */
    private function guardForSphere360Id(string $sphere360Id): Sphere360To
    {
        $sphere360To = $this->sphere360Repository->find($sphere360Id);
        if (empty($sphere360To)) {
            throw new NotFoundHttpException('Sphere360 file was not found');
        }

        return $sphere360To;
    }
}
