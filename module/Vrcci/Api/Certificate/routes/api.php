<?php

use Vrcci\Api\Certificate\Http\Controller\CertificateCrudController;

Route::prefix('/certificates')
    ->name('api.certificate.')
    ->middleware('auth:api')
    ->group(
        function (): void {
            Route::get('/', CertificateCrudController::class . '@listAction')
                ->name('list');
            Route::post('/', CertificateCrudController::class . '@createAction')
                ->name('create');
            Route::put('/{certificate_guid}', CertificateCrudController::class . '@updateAction')
                ->name('update');
            Route::delete('/{certificate_guid}', CertificateCrudController::class . '@deleteAction')
                ->name('delete');
        }
    );
