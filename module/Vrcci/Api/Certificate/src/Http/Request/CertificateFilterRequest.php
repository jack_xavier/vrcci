<?php

namespace Vrcci\Api\Certificate\Http\Request;

use Vrcci\Api\Company\Http\Request\CompanyGuidFilterRequest;

/**
 * @OA\Schema(
 *     schema="CertificateFilterRequest",
 *     @OA\Property(property="company_guid", type="string", description="Company GUID"),
 * )
 */
class CertificateFilterRequest extends CompanyGuidFilterRequest
{
}

