<?php

namespace Vrcci\Api\Certificate\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourceCollection;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\Certificate\Http\Request\CertificateFilterRequest;
use Vrcci\Api\Certificate\Http\Request\UploadCertificateRequest;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Certificate\Dto\CertificateTo;
use Vrcci\Certificate\Dto\Resources\CertificateResource;
use Vrcci\Certificate\Eloquent\Repository\CertificateRepository;
use Vrcci\Certificate\Service\CertificateCrudService;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;

class CertificateCrudController extends Controller
{
    use CurrentUserTrait,
        GuardForCompanyGuidTrait;

    /**
     * @var CertificateRepository
     */
    protected $certificateRepository;

    /**
     * @var CertificateCrudService
     */
    protected $certificateService;

    /**
     * @var CompanyRepository
     */
    protected $companyRepository;

    /**
     * @var ResourcesMap
     */
    protected $resourcesMap;

    /**
     * @param CertificateRepository  $certificateRepository
     * @param CertificateCrudService $certificateService
     * @param CompanyRepository      $companyRepository
     * @param ResourcesMap           $resourcesMap
     */
    public function __construct(
        CertificateRepository $certificateRepository,
        CertificateCrudService $certificateService,
        CompanyRepository $companyRepository,
        ResourcesMap $resourcesMap
    ) {
        $this->certificateRepository = $certificateRepository;
        $this->certificateService = $certificateService;
        $this->companyRepository = $companyRepository;
        $this->resourcesMap = $resourcesMap;
    }

    /**
     * @param CertificateFilterRequest $filterRequest
     *
     * @throws AuthenticationException
     * @throws UnregisteredMappingException
     *
     * @return ResourceCollection
     *
     * @OA\Get(
     *   path="/api/certificates",
     *   description="Get certificates list",
     *   operationId="certificate-list",
     *   tags={"Certificates"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="query",
     *     required=true,
     *     description="Company Guid",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/Certificate"
     *              )
     *          )
     *        ),
     *        @OA\Property(
     *          property="links",
     *          ref="#/components/schemas/CollectionLinks"
     *        ),
     *        @OA\Property(
     *          property="meta",
     *          ref="#/components/schemas/CollectionMeta"
     *        ),
     *     )
     *   ),
     *   @OA\Response(response=500, description="Internal server error"),
     * )
     */
    public function listAction(CertificateFilterRequest $filterRequest): ResourceCollection
    {
        $dataRequest = $this->certificateRepository
            ->fetchAll()
            ->byCompanyGuid($filterRequest->getCompanyGuid());

        $certificates = $dataRequest->paginate(30);

        return $this->resourcesMap->collection($certificates);
    }

    /**
     * @param UploadCertificateRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return CertificateResource
     *
     * @OA\Post(
     *   path="/api/certificates",
     *   description="Create certificate",
     *   operationId="certificate-create",
     *   tags={"Certificates"},
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadCertificateRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Certificate"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Certificate successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Certificate was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function createAction(UploadCertificateRequest $request): CertificateResource
    {
        $certificateTo = $this->certificateService->create(
            $request->getFile(),
            $this->guardCompanyGuid($request->getCompanyGuid()),
            $this->getCurrentUser()
        );

        /** @var CertificateResource $resource */
        $resource = $this->resourcesMap->resource($certificateTo);
        $resource->isCreatedResource();
        $resource->withMessage('Certificate was successfully created');

        return $resource;
    }

    /**
     * @param string                      $companyId
     * @param UploadCertificateRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return CertificateResource
     *
     * @OA\Put(
     *   path="/api/certificates",
     *   description="Update certificate",
     *   operationId="company-update",
     *   tags={"Certificates"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="certificate_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadCertificateRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Certificate"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Certificate successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Certificate is not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function updateAction(string $certificateId, UploadCertificateRequest $request): CertificateResource
    {
        $certificateTo = $this->certificateService->update(
            $this->guardForCertificateId($certificateId),
            $request->getFile(),
            $this->getCurrentUser()
        );

        /** @var CertificateResource $resource */
        $resource = $this->resourcesMap->resource($certificateTo);
        $resource->isUpdatedResource();
        $resource->withMessage('Certificate was successfully updated');

        return $resource;
    }

    /**
     * @param string $certificateId
     *
     * @throws ConflictHttpException
     * @throws UnregisteredMappingException
     *
     * @return CertificateResource
     *
     * @OA\Delete(
     *   path="/api/certificates/{certificate_guid}",
     *   description="Delete certificate",
     *   operationId="certificate-delete",
     *   tags={"Certificates"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="certificate_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Certificate"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Certificate successfully deleted")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Certificate was not found"),
     *   @OA\Response(response=409, description="Conflict"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function deleteAction(string $certificateId): CertificateResource
    {
        $this->certificateService->delete(
            $certificateTo = $this->guardForCertificateId($certificateId)
        );

        /** @var CertificateResource $resource */
        $resource = $this->resourcesMap->resource($certificateTo);
        $resource->isDeletedResource();
        $resource->withMessage('Certificate was successfully deleted');

        return $resource;
    }

    /**
     * @param string $certificateId
     *
     * @throws UnregisteredMappingException
     *
     * @return CertificateTo
     */
    private function guardForCertificateId(string $certificateId): CertificateTo
    {
        $certificateTo = $this->certificateRepository->find($certificateId);
        if (empty($certificateTo)) {
            throw new NotFoundHttpException('Certificate was not found');
        }

        return $certificateTo;
    }
}
