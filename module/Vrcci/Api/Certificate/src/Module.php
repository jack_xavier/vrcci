<?php

namespace Vrcci\Api\Certificate;

use App\Module\AbstractModule;
use App\Module\Feature\RoutesProviderInterface;

class Module extends AbstractModule implements RoutesProviderInterface
{
    public const ALIAS = 'vrcci.api.certificate';

    /**
     * @return string
     */
    public static function getAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @return string[]|string
     */
    public function getRoutesPath()
    {
        return ['api' => __DIR__ . '/../routes/api.php'];
    }
}
