<?php

use Vrcci\Api\Auth\Common\Http\Controller\AuthController;
use Vrcci\Api\Auth\Common\Http\Controller\RegistrationController;

Route::prefix('/auth')
     ->name('api.auth.')
     ->group(
         function (): void {
             Route::post('/', AuthController::class . '@generateTokenAction')
                  ->name('login');

             Route::put('/', AuthController::class . '@refreshTokenAction')
                  ->name('refresh');

             Route::delete('/', AuthController::class . '@destroyTokenAction')
                  ->name('logout')
                  ->middleware('auth:api');
         }
     );

Route::prefix('/registration')
     ->name('api.registration.')
     ->group(
         function (): void {
             Route::post('/', RegistrationController::class . '@registrationAction')
                  ->name('registration')
                  ->middleware('guest');
         }
     );
