<?php

namespace Vrcci\Api\Auth\Common\Http\Controller;

use App\Http\Controllers\Controller;
use App\Http\Resource\ResourcesMap;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Vrcci\Api\Auth\Common\Http\Request\RegistrationRequest;
use Vrcci\Auth\Jwt\Dto\Resource\JwtPairResource;
use Vrcci\Auth\Jwt\Http\JwtTokenExtractor;
use Vrcci\Auth\Jwt\Service\JwtTokenService;
use Vrcci\Settings\User\Entity\UserSettings;
use Vrcci\User\Dto\UserTo;
use Vrcci\User\Notification\RegistrationNotification;
use Vrcci\User\Service\UserService;

class RegistrationController extends Controller
{
    use ThrottlesLogins;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var JwtTokenService
     */
    private $jwtTokenService;

    /**
     * @var ResourcesMap
     */
    private $resourcesMap;

    /**
     * @var JwtTokenExtractor
     */
    private $jwtTokenExtractor;

    /**
     * @param UserService       $userService
     * @param JwtTokenService   $jwtTokenService
     * @param JwtTokenExtractor $jwtTokenExtractor
     * @param ResourcesMap      $resourcesMap
     */
    public function __construct(
        UserService $userService,
        JwtTokenService $jwtTokenService,
        JwtTokenExtractor $jwtTokenExtractor,
        ResourcesMap $resourcesMap
    ) {
        $this->userService       = $userService;
        $this->resourcesMap      = $resourcesMap;
        $this->jwtTokenService   = $jwtTokenService;
        $this->jwtTokenExtractor = $jwtTokenExtractor;
    }

    /**
     * @param RegistrationRequest $request
     *
     * @return JsonResponse|JwtPairResource
     *
     * @throws ValidationException
     *
     * @OA\Post(
     *   path="/api/registration",
     *   description="New user registration",
     *   operationId="registration",
     *   tags={"Registration"},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *    @OA\JsonContent(
     *        ref="#/components/schemas/RegistrationRequest"
     *      )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/JwtTokenPair"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="User was registered")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Forbidden"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function registrationAction(RegistrationRequest $request)
    {
        /** @var UserTo $user */
        $user = $this->userService->create($request->getAttributes());
        $user->getOriginal()->notify(new RegistrationNotification());

        /** @var JwtPairResource $resource */
        $resource = $this->resourcesMap->resource(
            $this->jwtTokenService->generatePair($user->getGuid())
        );

        $resource->withMessage('User was registered');

        return $resource;
    }
}
