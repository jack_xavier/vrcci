<?php

namespace Vrcci\Api\Auth\Common\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourcesMap;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Vrcci\Auth\Jwt\Dto\Resource\JwtPairResource;
use Vrcci\Auth\Jwt\Enum\JwtTypeEnum;
use Vrcci\Auth\Jwt\Http\JwtTokenExtractor;
use Vrcci\Auth\Jwt\Service\JwtTokenService;
use Vrcci\User\Dto\UserTo;

class AuthController extends Controller
{
    use ThrottlesLogins, CurrentUserTrait;

    /**
     * @var JwtTokenService
     */
    private $jwtTokenService;

    /**
     * @var ResourcesMap
     */
    private $resourcesMap;

    /**
     * @var JwtTokenExtractor
     */
    private $jwtTokenExtractor;

    /**
     * @param JwtTokenService   $jwtTokenService
     * @param JwtTokenExtractor $jwtTokenExtractor
     * @param ResourcesMap      $resourcesMap
     */
    public function __construct(
        JwtTokenService $jwtTokenService,
        JwtTokenExtractor $jwtTokenExtractor,
        ResourcesMap $resourcesMap
    ) {
        $this->resourcesMap      = $resourcesMap;
        $this->jwtTokenService   = $jwtTokenService;
        $this->jwtTokenExtractor = $jwtTokenExtractor;
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @OA\Delete(
     *   path="/api/auth",
     *   description="Invalidate JWT token. Logout",
     *   operationId="logout",
     *   tags={"Auth"},
     *   @OA\Response(
     *     response=204,
     *     description="Successful operation"
     *   ),
     *   @OA\Response(response=500, description="Internal server error"),
     *   security={
     *         {"bearerAuth": {}}
     *    }
     * )
     */
    public function destroyTokenAction(Request $request): Response
    {
        $token = $this->jwtTokenExtractor->extractTokenFromRequest($request);
        $this->jwtTokenService->revoke($token->getId());

        return response(null, 204);
    }

    /**
     * @param Request $request
     *
     * @return JwtPairResource|Resource
     *
     * @OA\Put(
     *   path="/api/auth",
     *   description="Get new pair of JWT tokens using refresh token. Invalidates previous pair",
     *   operationId="refreshToken",
     *   tags={"Auth"},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *    @OA\JsonContent(
     *         @OA\Property(property="refreshToken", type="string")
     *      )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/JwtTokenPair"
     *         ),
     *      )
     *   ),
     *   @OA\Response(response=400, description="Refresh token is required"),
     *   @OA\Response(response=403, description="Forbidden")
     * )
     */
    public function refreshTokenAction(Request $request): JwtPairResource
    {
        $token = $this->jwtTokenExtractor->extractValidTokenOrFail($request, JwtTypeEnum::REFRESH);

        if (null === $token) {
            abort(400, 'Refresh token is required');
        }

        return $this->resourcesMap->resource(
            $this->jwtTokenService->refreshPair($token)
        );
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse|JwtPairResource
     *
     * @throws ValidationException
     *
     * @OA\Post(
     *   path="/api/auth",
     *   description="Generate new JWT pair using user credentials. Login using email/password",
     *   operationId="login",
     *   tags={"Auth"},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *    @OA\JsonContent(
     *         @OA\Property(property="email", type="string"),
     *         @OA\Property(property="password", type="string"),
     *         @OA\Property(property="remember", type="integer", example=1)
     *      )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/JwtTokenPair"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Authentication successful")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Forbidden"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function generateTokenAction(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            $this->clearLoginAttempts($request);

            return $this->authenticated($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Need for `ThrottlesLogins`
     *
     * @return string
     */
    protected function username(): string
    {
        return 'email';
    }

    /**
     * @param Request $request
     *
     * @throws ValidationException
     *
     * @return void
     */
    protected function validateLogin(Request $request): void
    {
        $this->validate(
            $request,
            [
                $this->username() => 'required|string',
                'password'        => 'required|string',
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function attemptLogin(Request $request): bool
    {
        $userProvider = Auth::getProvider();

        $credentials = $request->only($this->username(), 'password');

        $user = $userProvider->retrieveByCredentials($credentials);

        if ($user && $userProvider->validateCredentials($user, $credentials)) {
            Auth::setUser($user);

            return true;
        }

        return false;
    }

    /**
     * @param Request $request
     *
     * @return JwtPairResource
     */
    protected function authenticated(Request $request): JwtPairResource
    {
        /** @var UserTo $user */
        $user = $this->getCurrentUser();

        /** @var JwtPairResource $resource */
        $resource = $this->resourcesMap->resource(
            $this->jwtTokenService->generatePair($user->getGuid())
        );

        $resource->withMessage(trans('auth.success'));

        return $resource;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    protected function sendFailedLoginResponse(Request $request): JsonResponse
    {
        return response()->json(['error' => trans('auth.failed')], 403);
    }
}
