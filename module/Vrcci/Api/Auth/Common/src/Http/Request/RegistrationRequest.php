<?php

namespace Vrcci\Api\Auth\Common\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Vrcci\User\Dto\Attributes\UserAttributesVo;

/**
 * @OA\Schema(
 *     schema="RegistrationRequest",
 *     @OA\Property(property="email", type="string", description="Email"),
 *     @OA\Property(property="password", type="string", description="Password"),
 *     @OA\Property(property="lang", type="string", description="Default user language"),
 * )
 */
class RegistrationRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => [
                'required',
                'string',
                'email',
                Rule::unique('users', 'email'),
            ],
            'password' => [
                'required',
                'string',
                'min:6',
            ],
            'lang'     => [
                'nullable',
                'string',
                'min:2',
            ],
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return UserAttributesVo
     */
    public function getAttributes(): UserAttributesVo
    {
        return UserAttributesVo::fromArray($this->validated());
    }
}
