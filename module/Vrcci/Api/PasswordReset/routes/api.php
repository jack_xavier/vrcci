<?php

use Vrcci\Api\PasswordReset\Http\Controller\ForgotPasswordController;
use Vrcci\Api\PasswordReset\Http\Controller\PasswordResetController;

Route::prefix('/password-reset')
     ->name('api.password-reset.')
     ->group(
         function (): void {
             Route::post('/', ForgotPasswordController::class . '@forgotAction')
                  ->name('forgot')
                  ->middleware('guest');

             Route::put('/', PasswordResetController::class . '@resetAction')
                  ->name('reset')
                  ->middleware('guest');
         }
     );
