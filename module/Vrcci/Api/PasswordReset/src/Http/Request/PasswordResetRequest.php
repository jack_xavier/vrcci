<?php

namespace Vrcci\Api\PasswordReset\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Vrcci\Api\PasswordReset\Validation\PasswordResetTokenRule;

/**
 * @OA\Schema(
 *     schema="PasswordResetRequest",
 *     @OA\Property(property="token", type="string", description="Token"),
 *     @OA\Property(property="password", type="string", description="Password"),
 * )
 */
class PasswordResetRequest extends FormRequest
{
    /**
     * @param PasswordResetTokenRule $passwordResetTokenRule
     *
     * @return mixed[]
     */
    public function rules(PasswordResetTokenRule $passwordResetTokenRule): array
    {
        return [
            'token'    => [
                'required',
                'string',
                $passwordResetTokenRule,
            ],
            'password' => [
                'required',
                'string',
                'min:6',
            ],
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->get('password');
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->get('token');
    }
}
