<?php

namespace Vrcci\Api\PasswordReset\Http\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Vrcci\Api\PasswordReset\Http\Request\ForgotPasswordRequest;
use Vrcci\PasswordReset\Service\PasswordResetService;
use Vrcci\User\Dto\UserTo;
use Vrcci\User\Eloquent\Repository\UserRepository;

class ForgotPasswordController extends Controller
{
    /**
     * @var PasswordResetService
     */
    private $passwordResetService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param PasswordResetService $passwordResetService
     * @param UserRepository       $userRepository
     */
    public function __construct(PasswordResetService $passwordResetService, UserRepository $userRepository)
    {
        $this->passwordResetService = $passwordResetService;
        $this->userRepository       = $userRepository;
    }

    /**
     * @param ForgotPasswordRequest $request
     *
     * @return Response
     *
     * @OA\Post(
     *   path="/api/password-reset",
     *   description="Send email with url for reset password",
     *   operationId="password-reset-send-email",
     *   tags={"Password reset"},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *   @OA\JsonContent(
     *        ref="#/components/schemas/ForgotPasswordRequest"
     *      )
     *   ),
     *   @OA\Response(
     *     response=204,
     *     description="Successful operation",
     *   ),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    protected function forgotAction(ForgotPasswordRequest $request): Response
    {
        /** @var UserTo $user */
        $user = $this->userRepository->fetchAll()->byEmail($request->getEmail())->first();

        $passwordReset = $this->passwordResetService->updateOrCreate($user);

        // TODO move this to notification service
        $user->getOriginal()->sendPasswordResetNotification($passwordReset->getToken());

        return response(null, 204);
    }
}
