<?php

namespace Vrcci\Api\PasswordReset\Http\Controller;

use App\Http\Controllers\Controller;
use App\Http\Resource\ResourcesMap;
use Vrcci\Api\PasswordReset\Http\Request\PasswordResetRequest;
use Vrcci\Auth\Jwt\Dto\Resource\JwtPairResource;
use Vrcci\Auth\Jwt\Service\JwtTokenService;
use Vrcci\PasswordReset\Dto\PasswordResetTo;
use Vrcci\PasswordReset\Eloquent\Repository\PasswordResetRepository;
use Vrcci\PasswordReset\Service\PasswordResetService;

class PasswordResetController extends Controller
{
    /**
     * @var JwtTokenService
     */
    private $jwtTokenService;

    /**
     * @var ResourcesMap
     */
    private $resourcesMap;

    /**
     * @var PasswordResetRepository
     */
    private $passwordResetRepository;

    /**
     * @var PasswordResetService
     */
    private $passwordResetService;

    /**
     * @param JwtTokenService         $jwtTokenService
     * @param ResourcesMap            $resourcesMap
     * @param PasswordResetRepository $passwordResetRepository
     * @param PasswordResetService    $passwordResetService
     */
    public function __construct(
        JwtTokenService $jwtTokenService,
        ResourcesMap $resourcesMap,
        PasswordResetRepository $passwordResetRepository,
        PasswordResetService $passwordResetService
    ) {
        $this->jwtTokenService         = $jwtTokenService;
        $this->resourcesMap            = $resourcesMap;
        $this->passwordResetRepository = $passwordResetRepository;
        $this->passwordResetService    = $passwordResetService;
    }

    /**
     * @param PasswordResetRequest $request
     *
     * @return JwtPairResource
     *
     * @OA\Put(
     *   path="/api/password-reset",
     *   description="Reset password",
     *   operationId="password-reset",
     *   tags={"Password reset"},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *   @OA\JsonContent(
     *        ref="#/components/schemas/PasswordResetRequest"
     *      )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/JwtTokenPair"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="User was registered")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    protected function resetAction(PasswordResetRequest $request): JwtPairResource
    {
        /** @var PasswordResetTo $passwordReset */
        $passwordReset = $this->passwordResetRepository
            ->fetchAll()
            ->byToken($request->getToken())
            ->first();

        $this->passwordResetService->resetPassword($passwordReset, $request->getPassword());

        /** @var JwtPairResource $resource */
        $resource = $this->resourcesMap->resource(
            $this->jwtTokenService->generatePair($passwordReset->getUser()->getGuid())
        );

        $resource->withMessage(trans('auth.success'));

        return $resource;
    }
}
