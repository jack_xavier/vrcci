<?php

namespace Vrcci\Api\PasswordReset\Validation;

use App\Validator\CompositeMessageRuleTrait;
use Illuminate\Contracts\Validation\Rule;
use Vrcci\PasswordReset\Dto\PasswordResetTo;
use Vrcci\PasswordReset\Eloquent\Repository\PasswordResetRepository;
use Vrcci\PasswordReset\Service\PasswordResetService;

class PasswordResetTokenRule implements Rule
{
    use CompositeMessageRuleTrait;

    private const EXPIRED = 'expired';
    private const INVALID = 'invalid';

    /**
     * @var PasswordResetRepository
     */
    private $passwordResetRepository;

    /**
     * @var PasswordResetService
     */
    private $passwordResetService;

    /**
     * @param PasswordResetRepository $passwordResetRepository
     * @param PasswordResetService    $passwordResetService
     */
    public function __construct(
        PasswordResetRepository $passwordResetRepository,
        PasswordResetService $passwordResetService
    ) {
        $this->passwordResetRepository = $passwordResetRepository;
        $this->passwordResetService    = $passwordResetService;
    }

    /**
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        /** @var PasswordResetTo $passwordReset */
        $passwordReset = $this->passwordResetRepository
            ->fetchAll()
            ->byToken($value)
            ->first();

        if (empty($passwordReset)) {
            $this->error(self::INVALID);

            return false;
        }

        if ($this->passwordResetService->checkTokenExpired($passwordReset)) {
            $this->error(self::EXPIRED);

            return false;
        }

        return true;
    }

    /**
     * @return string[]
     */
    protected function getMessageTemplates(): array
    {
        return [
            self::EXPIRED => 'Token is expired',
            self::INVALID => 'Token is invalid',
        ];
    }
}
