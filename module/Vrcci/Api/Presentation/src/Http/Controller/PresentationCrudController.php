<?php

namespace Vrcci\Api\Presentation\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourceCollection;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Presentation\Http\Request\PresentationFilterRequest;
use Vrcci\Api\Presentation\Http\Request\UploadPresentationRequest;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Presentation\Dto\PresentationTo;
use Vrcci\Presentation\Dto\Resources\PresentationResource;
use Vrcci\Presentation\Eloquent\Repository\PresentationRepository;
use Vrcci\Presentation\Service\PresentationCrudService;

class PresentationCrudController extends Controller
{
    use CurrentUserTrait,
        GuardForCompanyGuidTrait;

    /**
     * @var PresentationRepository
     */
    protected $presentationRepository;

    /**
     * @var PresentationCrudService
     */
    protected $presentationService;

    /**
     * @var CompanyRepository
     */
    protected $companyRepository;

    /**
     * @var ResourcesMap
     */
    protected $resourcesMap;

    /**
     * @param PresentationRepository  $presentationRepository
     * @param PresentationCrudService $presentationService
     * @param CompanyRepository       $companyRepository
     * @param ResourcesMap            $resourcesMap
     */
    public function __construct(
        PresentationRepository $presentationRepository,
        PresentationCrudService $presentationService,
        CompanyRepository $companyRepository,
        ResourcesMap $resourcesMap
    ) {
        $this->presentationRepository = $presentationRepository;
        $this->presentationService = $presentationService;
        $this->companyRepository = $companyRepository;
        $this->resourcesMap = $resourcesMap;
    }

    /**
     * @param PresentationFilterRequest $filterRequest
     *
     * @throws AuthenticationException
     * @throws UnregisteredMappingException
     *
     * @return ResourceCollection
     *
     * @OA\Get(
     *   path="/api/presentations",
     *   description="Get presentations list",
     *   operationId="presentation-list",
     *   tags={"Presentations"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="query",
     *     required=true,
     *     description="Company Guid",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/Presentation"
     *              )
     *          )
     *        ),
     *        @OA\Property(
     *          property="links",
     *          ref="#/components/schemas/CollectionLinks"
     *        ),
     *        @OA\Property(
     *          property="meta",
     *          ref="#/components/schemas/CollectionMeta"
     *        ),
     *     )
     *   ),
     *   @OA\Response(response=500, description="Internal server error"),
     * )
     */
    public function listAction(PresentationFilterRequest $filterRequest): ResourceCollection
    {
        $dataRequest = $this->presentationRepository
            ->fetchAll()
            ->byCompanyGuid($filterRequest->getCompanyGuid());

        $presentations = $dataRequest->paginate(30);

        return $this->resourcesMap->collection($presentations);
    }

    /**
     * @param UploadPresentationRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return PresentationResource
     *
     * @OA\Post(
     *   path="/api/presentations",
     *   description="Create presentation",
     *   operationId="presentation-create",
     *   tags={"Presentations"},
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadPresentationRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Presentation"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Presentation successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Presentation was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function createAction(UploadPresentationRequest $request): PresentationResource
    {
        $presentationTo = $this->presentationService->create(
            $request->getFile(),
            $this->guardCompanyGuid($request->getCompanyGuid()),
            $this->getCurrentUser()
        );

        /** @var PresentationResource $resource */
        $resource = $this->resourcesMap->resource($presentationTo);
        $resource->isCreatedResource();
        $resource->withMessage('Presentation was successfully created');

        return $resource;
    }

    /**
     * @param string                    $presentationId
     * @param UploadPresentationRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return PresentationResource
     *
     * @OA\Put(
     *   path="/api/presentations",
     *   description="Update presentation",
     *   operationId="company-update",
     *   tags={"Presentations"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="presentation_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadPresentationRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Presentation"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Presentation successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Presentation is not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function updateAction(string $presentationId, UploadPresentationRequest $request): PresentationResource
    {
        $presentationTo = $this->presentationService->update(
            $this->guardForPresentationId($presentationId),
            $request->getFile(),
            $this->getCurrentUser()
        );

        /** @var PresentationResource $resource */
        $resource = $this->resourcesMap->resource($presentationTo);
        $resource->isUpdatedResource();
        $resource->withMessage('Presentation was successfully updated');

        return $resource;
    }

    /**
     * @param string $presentationId
     *
     * @throws ConflictHttpException
     * @throws UnregisteredMappingException
     *
     * @return PresentationResource
     *
     * @OA\Delete(
     *   path="/api/presentations/{presentation_guid}",
     *   description="Delete presentation",
     *   operationId="presentation-delete",
     *   tags={"Presentations"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="presentation_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Presentation"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Presentation successfully deleted")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Presentation was not found"),
     *   @OA\Response(response=409, description="Conflict"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function deleteAction(string $presentationId): PresentationResource
    {
        $this->presentationService->delete(
            $presentationTo = $this->guardForPresentationId($presentationId)
        );

        /** @var PresentationResource $resource */
        $resource = $this->resourcesMap->resource($presentationTo);
        $resource->isDeletedResource();
        $resource->withMessage('Presentation was successfully deleted');

        return $resource;
    }

    /**
     * @param string $presentationId
     *
     * @throws UnregisteredMappingException
     *
     * @return PresentationTo
     */
    private function guardForPresentationId(string $presentationId): PresentationTo
    {
        $presentationTo = $this->presentationRepository->find($presentationId);
        if (empty($presentationTo)) {
            throw new NotFoundHttpException('Presentation was not found');
        }

        return $presentationTo;
    }
}
