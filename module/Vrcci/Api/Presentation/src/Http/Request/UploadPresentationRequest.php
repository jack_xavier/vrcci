<?php

namespace Vrcci\Api\Presentation\Http\Request;

use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;

/**
 * @OA\Schema(
 *     schema="UploadPresentationRequest",
 *     @OA\Property(property="file", type="file", description="Presentation file"),
 *     @OA\Property(property="company_guid", type="string", description="Company GUID"),
 *
 * )
 */
class UploadPresentationRequest extends FormRequest
{
    use AuthorizedRequest;

    public const INPUT_FILE = 'file';
    public const INPUT_COMPANY_GUID = 'company_guid';
    public const MEMORY_LIMIT = '20480';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::INPUT_FILE => [
                'required',
                'mimes:mpeg,ogg,mp4',
                'max:' . self::MEMORY_LIMIT,
            ],
            self::INPUT_COMPANY_GUID => [
                'string',
                Rule::exists('companies', 'guid'),
            ],
        ];
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file(self::INPUT_FILE);
    }

    /**
     * @return float
     */
    public function getFileSize(): float
    {
        return $this->getFile()
            ? $this->getFile()->getSize() / 1000
            : .0;
    }

    /**
     * @return UploadedFile|null
     */
    public function getCompanyGuid(): ?string
    {
        return $this->get(self::INPUT_COMPANY_GUID);
    }

    /**
     * @return array
     */
    public function messages() : array
    {
        return
            array_merge(
                parent::messages(),
                [
                    'file.max' => 'Общий объём файлов презентаций не должен превышать 20 МБ.',
                    'file.mimes' => 'Файл презентации должен иметь формат .mp4.',
                ]
            );
    }
}
