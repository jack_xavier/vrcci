<?php

namespace Vrcci\Api\Presentation\Http\Request;

use Vrcci\Api\Company\Http\Request\CompanyGuidFilterRequest;

/**
 * @OA\Schema(
 *     schema="PresentationFilterRequest",
 *     @OA\Property(property="company_guid", type="string", description="Company GUID"),
 * )
 */
class PresentationFilterRequest extends CompanyGuidFilterRequest
{
}
