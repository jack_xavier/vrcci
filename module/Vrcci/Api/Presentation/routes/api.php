<?php

use Vrcci\Api\Presentation\Http\Controller\PresentationCrudController;

Route::prefix('/presentations')
    ->name('api.presentation.')
    ->middleware('auth:api')
    ->group(
        function (): void {
            Route::get('/', PresentationCrudController::class . '@listAction')
                ->name('list');
            Route::post('/', PresentationCrudController::class . '@createAction')
                ->name('create');
            Route::put('/{presentation_guid}', PresentationCrudController::class . '@updateAction')
                ->name('update');
            Route::delete('/{presentation_guid}', PresentationCrudController::class . '@deleteAction')
                ->name('delete');
        }
    );
