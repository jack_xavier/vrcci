<?php

use Vrcci\Api\Excursion\Http\Controller\ExcursionCrudController;

Route::prefix('/excursions')
    ->name('api.excursion.')
    ->middleware('auth:api')
    ->group(
        function (): void {
            Route::get('/', ExcursionCrudController::class . '@listAction')
                ->name('list');
            Route::post('/', ExcursionCrudController::class . '@createAction')
                ->name('create');
            Route::put('/{excursion_guid}', ExcursionCrudController::class . '@updateAction')
                ->name('update');
            Route::delete('/{excursion_guid}', ExcursionCrudController::class . '@deleteAction')
                ->name('delete');
        }
    );
