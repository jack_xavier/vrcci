<?php

namespace Vrcci\Api\Excursion\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourceCollection;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Excursion\Http\Request\ExcursionFilterRequest;
use Vrcci\Api\Excursion\Http\Request\UploadExcursionRequest;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Excursion\Dto\ExcursionTo;
use Vrcci\Excursion\Dto\Resources\ExcursionResource;
use Vrcci\Excursion\Eloquent\Repository\ExcursionRepository;
use Vrcci\Excursion\Service\ExcursionCrudService;

class ExcursionCrudController extends Controller
{
    use CurrentUserTrait,
        GuardForCompanyGuidTrait;

    /**
     * @var ExcursionRepository
     */
    protected $excursionRepository;

    /**
     * @var ExcursionCrudService
     */
    protected $excursionService;

    /**
     * @var CompanyRepository
     */
    protected $companyRepository;

    /**
     * @var ResourcesMap
     */
    protected $resourcesMap;

    /**
     * @param ExcursionRepository  $excursionRepository
     * @param ExcursionCrudService $excursionService
     * @param CompanyRepository    $companyRepository
     * @param ResourcesMap         $resourcesMap
     */
    public function __construct(
        ExcursionRepository $excursionRepository,
        ExcursionCrudService $excursionService,
        CompanyRepository $companyRepository,
        ResourcesMap $resourcesMap
    ) {
        $this->excursionRepository = $excursionRepository;
        $this->excursionService = $excursionService;
        $this->companyRepository = $companyRepository;
        $this->resourcesMap = $resourcesMap;
    }

    /**
     * @param ExcursionFilterRequest $filterRequest
     *
     * @throws AuthenticationException
     * @throws UnregisteredMappingException
     *
     * @return ResourceCollection
     *
     * @OA\Get(
     *   path="/api/excursions",
     *   description="Get excursions list",
     *   operationId="excursion-list",
     *   tags={"Excursions"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="query",
     *     required=true,
     *     description="Company Guid",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadExcursionRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/Excursion"
     *              )
     *          )
     *        ),
     *        @OA\Property(
     *          property="links",
     *          ref="#/components/schemas/CollectionLinks"
     *        ),
     *        @OA\Property(
     *          property="meta",
     *          ref="#/components/schemas/CollectionMeta"
     *        ),
     *     )
     *   ),
     *   @OA\Response(response=500, description="Internal server error"),
     * )
     */
    public function listAction(ExcursionFilterRequest $filterRequest): ResourceCollection
    {
        $dataRequest = $this->excursionRepository
            ->fetchAll()
            ->byCompanyGuid($filterRequest->getCompanyGuid());

        $excursions = $dataRequest->paginate(30);

        return $this->resourcesMap->collection($excursions);
    }

    /**
     * @param UploadExcursionRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return ExcursionResource
     *
     * @OA\Post(
     *   path="/api/excursions",
     *   description="Create excursion",
     *   operationId="excursion-create",
     *   tags={"Excursions"},
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadExcursionRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Excursion"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Excursion successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Excursion was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function createAction(UploadExcursionRequest $request): ExcursionResource
    {
        $excursionTo = $this->excursionService->create(
            $request->getFile(),
            $this->guardCompanyGuid($request->getCompanyGuid()),
            $this->getCurrentUser()
        );

        /** @var ExcursionResource $resource */
        $resource = $this->resourcesMap->resource($excursionTo);
        $resource->isCreatedResource();
        $resource->withMessage('Excursion was successfully created');

        return $resource;
    }

    /**
     * @param string                 $excursionId
     * @param UploadExcursionRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return ExcursionResource
     *
     * @OA\Put(
     *   path="/api/excursions",
     *   description="Update excursion",
     *   operationId="company-update",
     *   tags={"Excursions"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="excursion_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadExcursionRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Excursion"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Excursion successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Excursion is not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function updateAction(string $excursionId, UploadExcursionRequest $request): ExcursionResource
    {
        $excursionTo = $this->excursionService->update(
            $this->guardForExcursionId($excursionId),
            $request->getFile(),
            $this->getCurrentUser()
        );

        /** @var ExcursionResource $resource */
        $resource = $this->resourcesMap->resource($excursionTo);
        $resource->isUpdatedResource();
        $resource->withMessage('Excursion was successfully updated');

        return $resource;
    }

    /**
     * @param string $excursionId
     *
     * @throws ConflictHttpException
     * @throws UnregisteredMappingException
     *
     * @return ExcursionResource
     *
     * @OA\Delete(
     *   path="/api/excursions/{excursion_guid}",
     *   description="Delete excursion",
     *   operationId="excursion-delete",
     *   tags={"Excursions"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="excursion_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Excursion"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Excursion successfully deleted")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Excursion was not found"),
     *   @OA\Response(response=409, description="Conflict"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function deleteAction(string $excursionId): ExcursionResource
    {
        $this->excursionService->delete(
            $excursionTo = $this->guardForExcursionId($excursionId)
        );

        /** @var ExcursionResource $resource */
        $resource = $this->resourcesMap->resource($excursionTo);
        $resource->isDeletedResource();
        $resource->withMessage('Excursion was successfully deleted');

        return $resource;
    }

    /**
     * @param string $excursionId
     *
     * @throws UnregisteredMappingException
     *
     * @return ExcursionTo
     */
    private function guardForExcursionId(string $excursionId): ExcursionTo
    {
        $excursionTo = $this->excursionRepository->find($excursionId);
        if (empty($excursionTo)) {
            throw new NotFoundHttpException('Excursion was not found');
        }

        return $excursionTo;
    }
}
