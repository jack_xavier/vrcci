<?php

namespace Vrcci\Api\Excursion\Http\Request;

use Vrcci\Api\Company\Http\Request\CompanyGuidFilterRequest;

/**
 * @OA\Schema(
 *     schema="ExcursionFilterRequest",
 *     @OA\Property(property="company_guid", type="string", description="Company GUID"),
 * )
 */
class ExcursionFilterRequest extends CompanyGuidFilterRequest
{
}
