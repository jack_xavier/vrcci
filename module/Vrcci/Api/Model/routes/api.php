<?php

use Vrcci\Api\Model\Http\Controller\ModelCrudController;
use Vrcci\Api\Model\Http\Controller\StandModelCrudController;
use Vrcci\Api\Model\Http\Controller\TextureCrudController;

Route::prefix('/stand-models')
    ->name('api.stand-model.')
    ->middleware('auth:api')
    ->group(
        function (): void {
            Route::get('/{stand-model_guid}', StandModelCrudController::class . '@findAction')
                ->name('find');
            Route::get('/', StandModelCrudController::class . '@listAction')
                ->name('list');
            Route::post('/', StandModelCrudController::class . '@createAction')
                ->name('create');
            Route::put('/{stand-model_guid}', StandModelCrudController::class . '@updateAction')
                ->name('update');
            Route::delete('/{stand-model_guid}', StandModelCrudController::class . '@deleteAction')
                ->name('delete');
        }
    );

Route::prefix('/models')
    ->name('api.models.')
    ->middleware('auth:api')
    ->group(
        function (): void {
            Route::post('/', ModelCrudController::class . '@createAction')
                ->name('create');
            Route::put('/{model_guid}', ModelCrudController::class . '@updateAction')
                ->name('update');
            Route::delete('/{model_guid}', ModelCrudController::class . '@deleteAction')
                ->name('delete');
        }
    );

Route::prefix('/textures')
    ->name('api.textures.')
    ->middleware('auth:api')
    ->group(
        function (): void {
            Route::post('/', TextureCrudController::class . '@createAction')
                ->name('create');
            Route::put('/{texture_guid}', TextureCrudController::class . '@updateAction')
                ->name('update');
            Route::delete('/{texture_guid}', TextureCrudController::class . '@deleteAction')
                ->name('delete');
        }
    );
