<?php

namespace Vrcci\Api\Model\Http\Request;

use App\Dto\Attributes\AbstractAttributesObject;
use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;
use Vrcci\Model\Dto\Attributes\ModelAttributesVo;

/**
 * @OA\Schema(
 *     schema="StandModelCreateRequest",
 *     @OA\Property(property="descripition", type="string", description="The description of a stand model"),
 *     @OA\Property(property="company_guid", type="string", description="The company GUID"),
 *     @OA\Property(property="model", type="file", description="Model file"),
 *     @OA\Property(
 *          property="textures",
 *          type="array",
 *          @OA\Items(type="string", format="binary")
 *     ),
 * )
 */
class StandModelCreateRequest extends FormRequest
{
    use AuthorizedRequest;

    public const INPUT_MODEL_FILE = 'model_file';
    public const INPUT_DESCRIPTION = 'description';
    public const INPUT_TEXTURES = 'textures';
    public const INPUT_TEXTURES_FILES = 'textures.*';
    public const INPUT_COMPANY_GUID = 'company_guid';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::INPUT_MODEL_FILE => [
                'required',
                'extensions:obj,fbx,dae',
            ],
            self::INPUT_DESCRIPTION => [
                'nullable',
                'string',
                'max:1536',
            ],
            self::INPUT_TEXTURES => [
                'array',
            ],
            self::INPUT_TEXTURES_FILES => [
                'required',
                'mimes:png',
            ],
            self::INPUT_COMPANY_GUID => [
                'string',
                Rule::exists('companies', 'guid'),
            ],
        ];
    }

    /**
     * @return ModelAttributesVo|AbstractAttributesObject
     */
    public function getAttributes(): AbstractAttributesObject
    {
        return ModelAttributesVo::fromArray($this->validated());
    }

    /**
     * @return string|null
     */
    public function getCompanyGuid(): ?string
    {
        return $this->get(self::INPUT_COMPANY_GUID);
    }

    /**
     * @return UploadedFile|null
     */
    public function getModel(): ?UploadedFile
    {
        return $this->get(self::INPUT_MODEL_FILE);
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return array_merge(
            parent::messages(),
            [
                'description.max' => 'Максимальное количество символов в поле "Описание" не должно превышать 1536.',
                'textures.*.mimes' => 'Файл текстуры должен иметь формат .png.',
                'model_file.extensions' => 'Файл 3D-модели должен иметь формат .obj, .fbx, .dae.',
            ]
        );
    }
}
