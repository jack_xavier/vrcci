<?php

namespace Vrcci\Api\Model\Http\Request;

use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;

/**
 * @OA\Schema(
 *     schema="UploadTextureRequest",
 *     @OA\Property(property="file", type="file", description="Upload Texture file"),
 *     @OA\Property(property="company_guid", type="string", description="Company GUID"),
 * )
 */
class UploadTextureRequest extends FormRequest
{
    use AuthorizedRequest;

    public const INPUT_FILE = 'file';
    public const INPUT_STAND_MODEL_GUID = 'stand_model_guid';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::INPUT_FILE => [
                'required',
                'mimes:png',
                'max:10240',
            ],
            self::INPUT_STAND_MODEL_GUID => [
                'string',
                Rule::exists('stand_models', 'guid'),
            ],
        ];
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file(self::INPUT_FILE);
    }

    /**
     * @return UploadedFile|null
     */
    public function getStandModelGuid(): ?string
    {
        return $this->get(self::INPUT_STAND_MODEL_GUID);
    }
}
