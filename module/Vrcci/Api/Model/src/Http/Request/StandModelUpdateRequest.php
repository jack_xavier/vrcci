<?php

namespace Vrcci\Api\Model\Http\Request;

use App\Dto\Attributes\AbstractAttributesObject;
use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;
use Vrcci\Model\Dto\Attributes\UpdateModelAttributesVo;

/**
 * @OA\Schema(
 *     schema="StandModelUpdateRequest",
 *     @OA\Property(property="description", type="string", description="Stand model description"),
 * )
 */
class StandModelUpdateRequest extends FormRequest
{
    use AuthorizedRequest;

    public const INPUT_DESCRIPTION = 'description';
    public const INPUT_MODEL_FILE = 'model_file';
    public const INPUT_MODEL_INDEXES = 'model_indexes';
    public const INPUT_TEXTURES = 'textures';
    public const INPUT_TEXTURES_FILES = 'textures.*';
    public const INPUT_TEXTURES_INDEXES = 'texture_indexes';
    public const INPUT_COMPANY_GUID = 'company_guid';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::INPUT_MODEL_FILE => [
                'nullable',
                'extensions:obj,fbx,dae',
            ],
            self::INPUT_MODEL_INDEXES => [
                'array',
            ],
            self::INPUT_DESCRIPTION => [
                'nullable',
                'string',
                'max:1536',
            ],
            self::INPUT_TEXTURES => [
                'array',
            ],
            self::INPUT_TEXTURES_FILES => [
                'required',
                'mimes:png',
            ],
            self::INPUT_TEXTURES_INDEXES => [
                'array',
            ],
            self::INPUT_COMPANY_GUID => [
                'string',
                Rule::exists('companies', 'guid'),
            ],
        ];
    }

    /**
     * @return UpdateModelAttributesVo|AbstractAttributesObject
     */
    public function getAttributes(): AbstractAttributesObject
    {
        return UpdateModelAttributesVo::fromArray($this->validated());
    }

    /**
     * @return UploadedFile|null
     */
    public function getModelFile()
    {
        $model = $this->get(self::INPUT_MODEL_FILE);
        $model = $this->attributes->get(self::INPUT_MODEL_FILE);
        dd($this->attributes());
        return $this->get(self::INPUT_MODEL_FILE);
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return array_merge(
            parent::messages(),
            [
                'description.max' => 'Максимальное количество символов в поле "Описание" не должно превышать 1536.',
                'textures.*.mimes' => 'Файл текстуры должен иметь формат .png.',
                'model_file.extensions' => 'Файл 3D-модели должен иметь формат .obj, .fbx, .dae.',
            ]
        );
    }
}
