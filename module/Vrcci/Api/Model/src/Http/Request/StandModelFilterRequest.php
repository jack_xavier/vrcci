<?php

namespace Vrcci\Api\Model\Http\Request;

use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @OA\Schema(
 *     schema="StandModelFilterRequest",
 *     @OA\Property(property="company_guid", type="string", description="Company Guid"),
 * )
 */
class StandModelFilterRequest extends FormRequest
{
    use AuthorizedRequest;

    public const INPUT_COMPANY_GUID = 'company_guid';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::INPUT_COMPANY_GUID => [
                Rule::exists('companies', 'guid'),
            ],
        ];
    }

    /**
     * @return string|null
     */
    public function getCompanyGuid(): ?string
    {
        return $this->get(self::INPUT_COMPANY_GUID);
    }
}
