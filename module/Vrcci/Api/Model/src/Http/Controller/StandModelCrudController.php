<?php

namespace Vrcci\Api\Model\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourceCollection;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Model\Http\Request\StandModelCreateRequest;
use Vrcci\Api\Model\Http\Request\StandModelFilterRequest;
use Vrcci\Api\Model\Http\Request\StandModelUpdateRequest;
use Vrcci\Model\Dto\Resource\StandModelResource;
use Vrcci\Model\Dto\StandModelTo;
use Vrcci\Model\Service\StandModelCrudService;
use Vrcci\Model\Eloquent\Repository\StandModelRepository;

class StandModelCrudController extends Controller
{
    use CurrentUserTrait,
        GuardForCompanyGuidTrait;

    /**
     * @var StandModelCrudService
     */
    protected $standModelService;

    /**
     * @var StandModelRepository
     */
    protected $standModelRepository;

    /**
     * @var ResourcesMap
     */
    protected $resourcesMap;

    /**
     * @param StandModelFilterRequest $request
     *
     * @throws AuthenticationException
     * @throws UnregisteredMappingException
     *
     * @return ResourceCollection
     *
     * @OA\Get(
     *   path="/api/stand-model",
     *   description="Get stand models list",
     *   operationId="stand-model-list",
     *   tags={"stand-model"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="path",
     *     required=true,
     *     description="Company GUID",
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/StandModel"
     *              )
     *          )
     *        ),
     *        @OA\Property(
     *          property="links",
     *          ref="#/components/schemas/CollectionLinks"
     *        ),
     *        @OA\Property(
     *          property="meta",
     *          ref="#/components/schemas/CollectionMeta"
     *        ),
     *     )
     *   ),
     *   @OA\Response(response=500, description="Internal server error"),
     *   @OA\Response(response=404, description="Stand Model was not found"),
     * )
     */
    public function listAction(StandModelFilterRequest $request): ResourceCollection
    {
        $this->guardCompanyGuid($request->getCompanyGuid());

        $dataRequest = $this->standModelRepository
            ->fetchAll()
            ->byCompanyGuid($request->getCompanyGuid());

        $standModels = $dataRequest->paginate($request->get('limit', 3));

        return $this->resourcesMap->collection($standModels);
    }

    /**
     * @param string $standModelGuid
     *
     * @throws UnregisteredMappingException
     *
     * @return StandModelResource
     *
     * @OA\Get(
     *   path="/api/stand-model/{stand_model_guid}",
     *   description="Get stand Models",
     *   operationId="stand-model-find",
     *   tags={"stand-model"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="stand_model_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          ref="#/components/schemas/StandModel"
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Stand Model was not found"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function findAction(string $standModelGuid): StandModelResource
    {
        $standModelTo = $this->guardForStandModel($standModelGuid);
        /** @var StandModelResource $standModelResource */
        $standModelResource = $this->resourcesMap->resource($standModelTo);

        return $standModelResource;
    }

    /**
     * @param StandModelCreateRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return StandModelResource
     *
     * @OA\Post(
     *   path="/api/stand-model",
     *   description="Create stand model",
     *   operationId="stand-model-create",
     *   tags={"stand-model"},
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/StandModelCreateRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/StandModel"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Stand model successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Stand model was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function createAction(StandModelCreateRequest $request): StandModelResource
    {
        $standModelTo = $this->standModelService->create(
            $request->getAttributes(),
            $this->guardCompanyGuid($request->getCompanyGuid()),
            $this->getCurrentUser()
        );

        /** @var StandModelResource $resource */
        $resource = $this->resourcesMap->resource($standModelTo);
        $resource->isCreatedResource();
        $resource->withMessage('Stand model was successfully created');

        return $resource;
    }

    /**
     * @param string                  $standModelGuid
     * @param StandModelUpdateRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return StandModelResource
     *
     * @OA\Put(
     *   path="/api/stand-model",
     *   description="Update stand model",
     *   operationId="stand-model-update",
     *   tags={"stand-model"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="stand_model_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/StandModelUpdateRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/StandModel"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="StandModel successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Stand model is not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function updateAction(string $standModelGuid, StandModelUpdateRequest $request): StandModelResource
    {
        $standModelTo = $this->standModelService->update(
            $this->guardForStandModel($standModelGuid),
            $request->getAttributes()
        );

        /** @var StandModelResource $resource */
        $resource = $this->resourcesMap->resource($standModelTo);
        $resource->isUpdatedResource();
        $resource->withMessage('Stand model was successfully updated');

        return $resource;
    }

    /**
     * @param string $standModelGuid
     *
     * @throws ConflictHttpException
     * @throws UnregisteredMappingException
     *
     * @return StandModelResource
     *
     * @OA\Delete(
     *   path="/api/stand-model/{stand_model_guid}",
     *   description="Delete Stand Model",
     *   operationId="stand-model-delete",
     *   tags={"stand-model"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="stand_model_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/StandModel"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="StandModel successfully deleted")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="StandModel was not found"),
     *   @OA\Response(response=409, description="Conflict"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function deleteAction(string $standModelGuid): StandModelResource
    {
        $this->standModelService->delete(
            $standModelTo = $this->guardForStandModel($standModelGuid)
        );

        /** @var StandModelResource $resource */
        $resource = $this->resourcesMap->resource($standModelTo);
        $resource->isDeletedResource();
        $resource->withMessage('StandModel was successfully deleted');

        return $resource;
    }

    /**
     * @param string $standModelGuid
     *
     * @throws UnregisteredMappingException
     * @return StandModelTo
     */
    private function guardForStandModel(string $standModelGuid): StandModelTo
    {
        $standModelTo = $this->standModelRepository->find($standModelGuid);
        if (empty($standModelTo)) {
            throw new NotFoundHttpException('Stand model was not found');
        }

        return $standModelTo;
    }
}
