<?php

namespace Vrcci\Api\Model\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Model\Http\Request\UploadTextureRequest;
use Vrcci\Model\Dto\Resources\TextureResource;
use Vrcci\Model\Dto\StandModelTo;
use Vrcci\Model\Dto\TextureTo;
use Vrcci\Model\Service\TextureFileService;
use Vrcci\Model\Eloquent\Repository\StandModelRepository;
use Vrcci\Model\Eloquent\Repository\TextureRepository;

class TextureCrudController extends Controller
{
    use CurrentUserTrait,
        GuardForCompanyGuidTrait;

    /**
     * @var TextureRepository
     */
    protected $textureRepository;

    /**
     * @var TextureFileService
     */
    protected $textureFileService;

    /**
     * @var StandModelRepository
     */
    protected $standModelRepository;

    /**
     * @var ResourcesMap
     */
    protected $resourcesMap;

    /**
     * TextureCrudController constructor.
     *
     * @param TextureRepository    $textureRepository
     * @param TextureFileService   $textureFileService
     * @param StandModelRepository $standModelRepository
     * @param ResourcesMap         $resourcesMap
     */
    public function __construct(
        TextureRepository $textureRepository,
        TextureFileService $textureFileService,
        StandModelRepository $standModelRepository,
        ResourcesMap $resourcesMap
    ) {
        $this->textureRepository = $textureRepository;
        $this->textureFileService = $textureFileService;
        $this->standModelRepository = $standModelRepository;
        $this->resourcesMap = $resourcesMap;
    }

    /**
     * @param UploadTextureRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return TextureResource
     *
     * @OA\Post(
     *   path="/api/textures",
     *   description="Create texture",
     *   operationId="texture-create",
     *   tags={"Textures"},
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadTextureRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Texture"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Texture successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Texture was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function createAction(UploadTextureRequest $request): TextureResource
    {
        $textureTo = $this->textureFileService->create(
            $request->getFile(),
            $this->guardForStandModelId($request->getStandModelGuid()),
            $this->getCurrentUser()
        );

        /** @var TextureResource $resource */
        $resource = $this->resourcesMap->resource($textureTo);
        $resource->isCreatedResource();
        $resource->withMessage('Texture was successfully created');

        return $resource;
    }

    /**
     * @param string               $textureId
     * @param UploadTextureRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return TextureResource
     *
     * @OA\Put(
     *   path="/api/textures",
     *   description="Update texture",
     *   operationId="texture-update",
     *   tags={"Textures"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="texture_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadTextureRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Texture"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Texture successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Texture is not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function updateAction(string $textureId, UploadTextureRequest $request): TextureResource
    {
        $textureTo = $this->textureFileService->update(
            $this->guardForTextureId($textureId),
            $request->getFile(),
            $this->getCurrentUser()
        );

        /** @var TextureResource $resource */
        $resource = $this->resourcesMap->resource($textureTo);
        $resource->isUpdatedResource();
        $resource->withMessage('Texture was successfully updated');

        return $resource;
    }

    /**
     * @param string $textureId
     *
     * @throws ConflictHttpException
     * @throws UnregisteredMappingException
     *
     * @return TextureResource
     *
     * @OA\Delete(
     *   path="/api/textures/{texture_guid}",
     *   description="Delete texture",
     *   operationId="texture-delete",
     *   tags={"Textures"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="texture_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Texture"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Texture successfully deleted")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Texture was not found"),
     *   @OA\Response(response=409, description="Conflict"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function deleteAction(string $textureId): TextureResource
    {
        $this->textureFileService->delete(
            $textureTo = $this->guardForTextureId($textureId)
        );

        /** @var TextureResource $resource */
        $resource = $this->resourcesMap->resource($textureTo);
        $resource->isDeletedResource();
        $resource->withMessage('Texture was successfully deleted');

        return $resource;
    }

    /**
     * @param string $textureId
     *
     * @throws UnregisteredMappingException
     *
     * @return TextureTo
     */
    private function guardForTextureId(string $textureId): TextureTo
    {
        $textureTo = $this->textureRepository->find($textureId);
        if (empty($textureTo)) {
            throw new NotFoundHttpException('Texture file was not found');
        }

        return $textureTo;
    }

    /**
     * @param string $standModelTo
     *
     * @throws UnregisteredMappingException
     *
     * @return StandModelTo
     */
    private function guardForStandModelId(string $standModelTo): StandModelTo
    {
        $standModelTo = $this->standModelRepository->find($standModelTo);
        if (empty($standModelTo)) {
            throw new NotFoundHttpException('Stand model was not found');
        }

        return $standModelTo;
    }
}
