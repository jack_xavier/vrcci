<?php

namespace Vrcci\Api\Model\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourceCollection;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Model\Http\Request\UploadModelRequest;
use Vrcci\Model\Dto\ModelTo;
use Vrcci\Model\Dto\Resources\ModelResource;
use Vrcci\Model\Dto\StandModelTo;
use Vrcci\Model\Eloquent\Repository\ModelRepository;
use Vrcci\Model\Service\ModelFileService;
use Vrcci\Model\Eloquent\Repository\StandModelRepository;

class ModelCrudController extends Controller
{
    use CurrentUserTrait,
        GuardForCompanyGuidTrait;

    /**
     * @var ModelRepository
     */
    protected $modelRepository;

    /**
     * @var ModelFileService
     */
    protected $modelService;

    /**
     * @var StandModelRepository
     */
    protected $standModelRepository;

    /**
     * @var ResourcesMap
     */
    protected $resourcesMap;

    /**
     * ModelCrudController constructor.
     *
     * @param ModelRepository      $modelRepository
     * @param ModelFileService     $modelService
     * @param StandModelRepository $standModelRepository
     * @param ResourcesMap         $resourcesMap
     */
    public function __construct(
        ModelRepository $modelRepository,
        ModelFileService $modelService,
        StandModelRepository $standModelRepository,
        ResourcesMap $resourcesMap
    ) {
        $this->modelRepository = $modelRepository;
        $this->modelService = $modelService;
        $this->standModelRepository = $standModelRepository;
        $this->resourcesMap = $resourcesMap;
    }

    /**
     * @param UploadModelRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return ModelResource
     *
     * @OA\Post(
     *   path="/api/models",
     *   description="Create model",
     *   operationId="model-create",
     *   tags={"Models"},
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadModelRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Model"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Model successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Model was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function createAction(UploadModelRequest $request): ModelResource
    {
        $modelTo = $this->modelService->create(
            $request->getFile(),
            $this->guardForStandModelId($request->getStandModelGuid())->getOriginal(),
            $this->getCurrentUser()
        );

        /** @var ModelResource $resource */
        $resource = $this->resourcesMap->resource($modelTo);
        $resource->isCreatedResource();
        $resource->withMessage('Model was successfully created');

        return $resource;
    }

    /**
     * @param string             $modelGuid
     * @param UploadModelRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return ModelResource
     *
     * @OA\Put(
     *   path="/api/models",
     *   description="Update model",
     *   operationId="model-update",
     *   tags={"Models"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="model_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\MediaType(
     *        mediaType="multipart/form-data",
     *        @OA\JsonContent(
     *          ref="#/components/schemas/UploadModelRequest"
     *        )
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Model"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Model successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Model is not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function updateAction(string $modelGuid, UploadModelRequest $request): ModelResource
    {
        $modelTo = $this->modelService->update(
            $this->guardForModelId($modelGuid),
            $request->getFile(),
            $this->getCurrentUser()
        );

        /** @var ModelResource $resource */
        $resource = $this->resourcesMap->resource($modelTo);
        $resource->isUpdatedResource();
        $resource->withMessage('Model was successfully updated');

        return $resource;
    }

    /**
     * @param string $modelGuid
     *
     * @throws ConflictHttpException
     * @throws UnregisteredMappingException
     *
     * @return ModelResource
     *
     * @OA\Delete(
     *   path="/api/models/{model_guid}",
     *   description="Delete model",
     *   operationId="model-delete",
     *   tags={"Models"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="model_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Model"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Model successfully deleted")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Model was not found"),
     *   @OA\Response(response=409, description="Conflict"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function deleteAction(string $modelGuid): ModelResource
    {
        $this->modelService->delete(
            $modelTo = $this->guardForModelId($modelGuid)
        );

        /** @var ModelResource $resource */
        $resource = $this->resourcesMap->resource($modelTo);
        $resource->isDeletedResource();
        $resource->withMessage('Model was successfully deleted');

        return $resource;
    }

    /**
     * @param string $modelGuid
     *
     * @throws UnregisteredMappingException
     *
     * @return ModelTo
     */
    private function guardForModelId(string $modelGuid): ModelTo
    {
        $modelTo = $this->modelRepository->find($modelGuid);
        if (empty($modelTo)) {
            throw new NotFoundHttpException('Model file was not found');
        }

        return $modelTo;
    }

    /**
     * @param string $standModelGuid
     *
     * @throws UnregisteredMappingException
     *
     * @return StandModelTo
     */
    private function guardForStandModelId(string $standModelGuid): StandModelTo
    {
        $standModelTo = $this->standModelRepository->find($standModelGuid);
        if (empty($standModelTo)) {
            throw new NotFoundHttpException('Stand model file was not found');
        }

        return $standModelTo;
    }
}
