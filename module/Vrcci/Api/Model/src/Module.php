<?php

namespace Vrcci\Api\Model;

use App\Module\AbstractModule;
use App\Module\Feature\RoutesProviderInterface;

class Module extends AbstractModule implements RoutesProviderInterface
{
    public const ALIAS = 'vrcci.api.stand-model';

    /**
     * @return string
     */
    public static function getAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @return string[]|string
     */
    public function getRoutesPath()
    {
        return ['api' => __DIR__ . '/../routes/api.php'];
    }
}
