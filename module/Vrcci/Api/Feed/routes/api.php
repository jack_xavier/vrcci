<?php

use App\Entity\GuidGenerator;
use Vrcci\Api\Feed\Http\Controller\FeedCrudController;

Route::prefix('company/{company_guid}/feed')
    ->name('api.feed.')
    ->where(['feed_guid' => '[0-9]+'])
    ->group(
        function (): void {
            Route::get('/', FeedCrudController::class . '@listAction')
                ->middleware('guest')
                ->name('list')
                ->where('company_guid', GuidGenerator::GUID_PATTERN)
                ->where('feed_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{feed_guid}', FeedCrudController::class . '@findAction')
                ->name('find')
                ->middleware('guest')
                ->where('company_guid', GuidGenerator::GUID_PATTERN)
                ->where('feed_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/', FeedCrudController::class . '@createAction')
                ->name('create')
                ->middleware('guest')
                ->where('company_guid', GuidGenerator::GUID_PATTERN)
                ->where('feed_guid', GuidGenerator::GUID_PATTERN);
            Route::put('/{feed_guid}', FeedCrudController::class . '@updateAction')
                ->name('update')
                ->middleware('guest')
                ->where('company_guid', GuidGenerator::GUID_PATTERN)
                ->where('feed_guid', GuidGenerator::GUID_PATTERN);
            Route::delete('/{feed_guid}', FeedCrudController::class . '@deleteAction')
                ->name('delete')
                ->middleware('guest')
                ->where('company_guid', GuidGenerator::GUID_PATTERN)
                ->where('feed_guid', GuidGenerator::GUID_PATTERN);
        }
    );


