<?php

namespace Vrcci\Api\Feed\Http\Request;

use App\Dto\Attributes\AbstractAttributesObject;
use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Vrcci\Feed\Dto\Attributes\FeedAttributesVo;

/**
 * @OA\Schema(
 *     schema="FeedEditRequest",
 *     @OA\Property(property="title", type="string", description="Заголовок"),
 *     @OA\Property(property="description", type="string", description="Описание"),
 * )
 */
class FeedEditRequest extends FormRequest
{
    use AuthorizedRequest;

    public const INPUT_TITLE = 'title';
    public const INPUT_DESCRIPTION = 'description';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::INPUT_TITLE => ['required', 'string', 'max:128'],
            self::INPUT_DESCRIPTION => ['nullable', 'string', 'max:4096'],
        ];
    }

    /**
     * @return FeedAttributesVo|AbstractAttributesObject
     */
    public function getAttributes(): AbstractAttributesObject
    {
        return FeedAttributesVo::fromArray($this->validated());
    }

    /**
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes[self::INPUT_TITLE] = 'Заголовок';

        return $attributes;
    }
}
