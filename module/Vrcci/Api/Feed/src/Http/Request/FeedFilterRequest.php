<?php

namespace Vrcci\Api\Feed\Http\Request;

use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @OA\Schema(
 *     schema="FeedFilterRequest",
 *     @OA\Property(property="title", type="string", description="Feed name"),
 *     @OA\Property(property="company_guid", type="string", description="Company Guid"),
 * )
 */
class FeedFilterRequest extends FormRequest
{
    use AuthorizedRequest;

    public const VAR_DEFAULT_LIMIT = 15;
    public const INPUT_COMPANY_GUID = 'company_guid';
    public const INPUT_TITLE = 'title';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::INPUT_TITLE => ['nullable', 'string', 'max:128'],
            self::INPUT_COMPANY_GUID => [
                Rule::exists('companies', 'guid'),
            ],
        ];
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->get(self::INPUT_TITLE);
    }

    /**
     * @return string|null
     */
    public function getCompanyGuid(): ?string
    {
        return $this->get(self::INPUT_COMPANY_GUID);
    }

    /**
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->get('page');
    }

    /**
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes[self::INPUT_TITLE] = 'Заголовок';

        return $attributes;
    }
}
