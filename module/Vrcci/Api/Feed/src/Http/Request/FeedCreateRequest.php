<?php

namespace Vrcci\Api\Feed\Http\Request;

use App\Dto\Attributes\AbstractAttributesObject;
use App\Http\Request\AuthorizedRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Vrcci\Feed\Dto\Attributes\FeedAttributesVo;

/**
 * @OA\Schema(
 *     schema="FeedCreateRequest",
 *     @OA\Property(property="company_guid", type="string", description="Company GUID"),
 *     @OA\Property(property="title", type="string", description="Заголовок"),
 *     @OA\Property(property="description", type="string", description="Описание"),
 * )
 */
class FeedCreateRequest extends FormRequest
{
    use AuthorizedRequest;

    public const INPUT_COMPANY_GUID = 'company_guid';
    public const INPUT_TITLE = 'title';
    public const INPUT_DESCRIPTION = 'description';

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            self::INPUT_TITLE => ['required', 'string', 'max:128'],
            self::INPUT_COMPANY_GUID => [
                Rule::exists('companies', 'guid'),
            ],
            self::INPUT_DESCRIPTION => ['nullable', 'string', 'max:4096'],
        ];
    }

    /**
     * @return FeedAttributesVo|AbstractAttributesObject
     */
    public function getAttributes(): AbstractAttributesObject
    {
        return FeedAttributesVo::fromArray($this->validated());
    }

    /**
     * @return string|null
     */
    public function getCompanyGuid(): ?string
    {
        return $this->get(self::INPUT_COMPANY_GUID);
    }

    /**
     * @return array
     */
    public function attributes(): array
    {
        $attributes = parent::attributes();
        $attributes[self::INPUT_TITLE] = 'Заголовок';

        return $attributes;
    }
}
