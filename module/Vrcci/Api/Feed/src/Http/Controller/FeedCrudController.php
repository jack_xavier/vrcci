<?php

namespace Vrcci\Api\Feed\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourceCollection;
use App\Http\Resource\ResourcesMap;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Feed\Http\Request\FeedCreateRequest;
use Vrcci\Api\Feed\Http\Request\FeedEditRequest;
use Vrcci\Api\Feed\Http\Request\FeedFilterRequest;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Feed\Dto\FeedTo;
use Vrcci\Feed\Dto\Resources\FeedResource;
use Vrcci\Feed\Eloquent\Repository\FeedRepository;
use Vrcci\Feed\Service\FeedCrudService;

class FeedCrudController extends Controller
{
    use CurrentUserTrait,
        GuardForCompanyGuidTrait;

    /**
     * @var FeedCrudService
     */
    private $service;

    /**
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * @var ResourcesMap
     */
    private $resourcesMap;

    /**
     * @param FeedCrudService   $service
     * @param FeedRepository    $repository
     * @param CompanyRepository $companyRepository
     * @param ResourcesMap      $resourcesMap
     */
    public function __construct(
        FeedCrudService $service,
        FeedRepository $repository,
        CompanyRepository $companyRepository,
        ResourcesMap $resourcesMap
    ) {
        $this->service = $service;
        $this->feedRepository = $repository;
        $this->companyRepository = $companyRepository;
        $this->resourcesMap = $resourcesMap;
    }

    /**
     * @param FeedFilterRequest $request
     *
     * @throws AuthenticationException
     * @throws UnregisteredMappingException
     *
     * @return ResourceCollection
     *
     * @OA\Get(
     *   path="/api/feed",
     *   description="Get feed list",
     *   operationId="feed-list",
     *   tags={"Feed"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="title",
     *     in="query",
     *     required=false,
     *     description="Feed name",
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="company_guid",
     *     in="path",
     *     required=true,
     *     description="Company GUID",
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/Feed"
     *              )
     *          )
     *        ),
     *        @OA\Property(
     *          property="links",
     *          ref="#/components/schemas/CollectionLinks"
     *        ),
     *        @OA\Property(
     *          property="meta",
     *          ref="#/components/schemas/CollectionMeta"
     *        ),
     *     )
     *   ),
     *   @OA\Response(response=500, description="Internal server error"),
     *   @OA\Response(response=404, description="Feed was not found"),
     * )
     */
    public function listAction(string $companyGuid, FeedFilterRequest $request): ResourceCollection
    {
        $this->guardCompanyGuid($companyGuid);

        $dataRequest = $this->feedRepository
            ->fetchAll()
            ->byCompanyGuid($companyGuid);

        if (null !== $title = $request->getTitle()) {
            $dataRequest->byTitleLike($request->getTitle());
        }

        $feed = $dataRequest->paginate($request->get('limit', 30));

        return $this->resourcesMap->collection($feed);
    }

    /**
     * @param string $feedGuid
     *
     * @throws UnregisteredMappingException
     *
     * @return FeedResource
     *
     * @OA\Get(
     *   path="/api/feed/{feed_guid}",
     *   description="Get Feed",
     *   operationId="feed-find",
     *   tags={"Feed"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="feed_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *        @OA\Property(
     *          property="data",
     *          ref="#/components/schemas/Feed"
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Feed was not found"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function findAction(string $companyGuid, string $feedGuid): FeedResource
    {
        $this->guardCompanyGuid($companyGuid);

        $feedTo = $this->guardFeedId($feedGuid);
        /** @var FeedResource $feedResource */
        $feedResource = $this->resourcesMap->resource($feedTo);

        return $feedResource;
    }

    /**
     * @param FeedEditRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws AuthenticationException
     *
     * @return FeedResource
     *
     * @OA\Post(
     *   path="/api/feed",
     *   description="Create feed",
     *   operationId="feed-create",
     *   tags={"Feed"},
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\JsonContent(
     *        ref="#/components/schemas/FeedCreateRequest"
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Feed"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Feed successfully created")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Company was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function createAction(string $companyGuid, FeedCreateRequest $request): FeedResource
    {
        $companyTo = $this->guardCompanyGuid($companyGuid);

        $feedTo = $this->service->create(
            $request->getAttributes(),
            $companyTo,
            $this->getCurrentUser()
        );

        /** @var FeedResource $resource */
        $resource = $this->resourcesMap->resource($feedTo);
        $resource->isCreatedResource();
        $resource->withMessage('Feed successfully created');

        return $resource;
    }

    /**
     * @param string          $feedGuid
     * @param FeedEditRequest $request
     *
     * @throws UnregisteredMappingException
     *
     * @return FeedResource
     *
     * @OA\Put(
     *   path="/api/feed/{feed_guid}",
     *   description="Update temlate",
     *   operationId="feed-update",
     *   tags={"Feed"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="feed_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *     @OA\JsonContent(
     *        ref="#/components/schemas/FeedEditRequest"
     *      )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Feed"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Feed successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Feed was not found"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function updateAction(string $companyGuid, string $feedGuid, FeedEditRequest $request): FeedResource
    {
        $this->guardCompanyGuid($companyGuid);

        $feed = $this->service->update(
            $this->guardFeedId($feedGuid),
            $request->getAttributes()
        );

        /** @var FeedResource $resource */
        $resource = $this->resourcesMap->resource($feed);
        $resource->isUpdatedResource();
        $resource->withMessage('Feed successfully updated');

        return $resource;
    }

    /**
     * @param string $feedGuid
     *
     * @throws Exception
     * @throws ConflictHttpException
     *
     * @return FeedResource
     *
     * @OA\Delete(
     *   path="/api/feed/{feed_guid}",
     *   description="Delete feed",
     *   operationId="feed-delete",
     *   tags={"Feed"},
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(
     *     name="feed_guid",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/Feed"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="Feed successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Feed was not found"),
     *   @OA\Response(response=409, description="Conflict"),
     *   @OA\Response(response=500, description="Internal server error")
     * )
     */
    public function deleteAction(string $companyGuid, string $feedGuid): FeedResource
    {
        $this->guardCompanyGuid($companyGuid);
        $feedTo = $this->guardFeedId($feedGuid);

        $this->service->delete($feedTo);

        /** @var FeedResource $resource */
        $resource = $this->resourcesMap->resource($feedTo);
        $resource->isDeletedResource();
        $resource->withMessage('Feed successfully deleted');

        return $resource;
    }

    /**
     * @param string $feedGuid
     *
     * @throws AccessDeniedHttpException
     * @throws NotFoundHttpException
     * @throws UnregisteredMappingException
     *
     * @return FeedTo
     */
    protected function guardFeedId(string $feedGuid): FeedTo
    {
        $feedTo = $this->feedRepository->find($feedGuid);
        if (empty($feedTo)) {
            throw new NotFoundHttpException('Feed was not found');
        }

        return $feedTo;
    }
}
