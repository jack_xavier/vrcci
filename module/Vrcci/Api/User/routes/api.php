<?php

use Vrcci\Api\User\Http\Controller\UserCrudController;

Route::prefix('/users')
     ->name('api.users.')
     ->group(
         function (): void {
             Route::get('/{user_id}', UserCrudController::class . '@findAction')
                  ->name('find')
                  ->middleware('auth:api')
                  ->where('user_id', '[0-9]+');

             Route::put('/{user_id}', UserCrudController::class . '@updateAction')
                  ->name('update')
                  ->middleware('auth:api')
                  ->where('user_id', '[0-9]+');
         }
     );
