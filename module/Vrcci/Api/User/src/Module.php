<?php

namespace Vrcci\Api\User;

use App\Module\AbstractModule;
use App\Module\Feature\RoutesProviderInterface;

class Module extends AbstractModule implements RoutesProviderInterface
{
    protected const ALIAS = 'vrcci.api.user';

    /**
     * @return string
     */
    public static function getAlias(): string
    {
        return static::ALIAS;
    }

    /**
     * @return array
     */
    public function getRoutesPath(): array
    {
        return ['api' => __DIR__ . '/../routes/api.php'];
    }
}
