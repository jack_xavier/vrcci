<?php

namespace Vrcci\Api\User\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Http\Resource\ResourcesMap;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vrcci\Api\User\Http\Request\UserUpdateRequest;
use Vrcci\User\Dto\Resources\UserResource;
use Vrcci\User\Dto\UserTo;
use Vrcci\User\Eloquent\Repository\UserRepository;
use Vrcci\User\Service\UserService;

class UserCrudController extends Controller
{
    use CurrentUserTrait;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var ResourcesMap
     */
    protected $resourcesMap;

    /**
     * @param UserService    $userService
     * @param UserRepository $userRepository
     * @param ResourcesMap   $resourcesMap
     */
    public function __construct(UserService $userService, UserRepository $userRepository, ResourcesMap $resourcesMap)
    {
        $this->userService    = $userService;
        $this->userRepository = $userRepository;
        $this->resourcesMap   = $resourcesMap;
    }

    /**
     * @param int $userId
     *
     * @return UserResource
     *
     * @OA\Get(
     *   path="/api/users/{user_id}",
     *   description="Get user information",
     *   operationId="getUser",
     *   tags={"User"},
     *   @OA\Parameter(
     *     name="user_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="integer")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/User"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="User successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=401, description="Unauthorized"),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=404, description="Not found"),
     *   @OA\Response(response=500, description="Internal server error"),
     *   security={
     *       {"bearerAuth": {}}
     *    }
     * )
     */
    public function findAction(int $userId): UserResource
    {
        $user = $this->guardUserId($userId);

        /** @var UserResource $resource */
        $resource = $this->resourcesMap->resource($user);

        return $resource;
    }

    /**
     * @param UserUpdateRequest $request
     * @param int               $userId
     *
     * @return UserResource
     *
     * @OA\Put(
     *   path="/api/users/{user_id}",
     *   description="Update user",
     *   operationId="user-update",
     *   tags={"User"},
     *   @OA\Parameter(
     *     name="user_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="integer")
     *   ),
     *   @OA\RequestBody(
     *     description="Parameters",
     *     required=true,
     *   @OA\JsonContent(
     *        ref="#/components/schemas/UserUpdateRequest"
     *      )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successful operation",
     *     @OA\JsonContent(
     *         @OA\Property(
     *            property="data",
     *            ref="#/components/schemas/User"
     *         ),
     *          @OA\Property(
     *            property="meta",
     *            @OA\Property(property="message", type="string", example="User successfully updated")
     *        )
     *     )
     *   ),
     *   @OA\Response(response=404, description="Token is invalid"),
     *   @OA\Response(response=403, description="Action is forbidden"),
     *   @OA\Response(response=422, description="Unprocessable Entity"),
     *   @OA\Response(response=500, description="Internal server error"),
     *   security={
     *       {"bearerAuth": {}}
     *    }
     * )
     */
    public function updateAction(UserUpdateRequest $request, int $userId): UserResource
    {
        $user = $this->guardUserId($userId);

        $user = $this->userService->update($user, $request->getAttributes());

        /** @var UserResource $resource */
        $resource = $this->resourcesMap->resource($user);
        $resource->isUpdatedResource();
        $resource->withMessage('User successfully updated');

        return $resource;
    }

    /**
     * @param int $userId
     *
     * @return UserTo
     */
    protected function guardUserId(int $userId): UserTo
    {
        $user = $this->userRepository->find($userId);
        if (empty($user)) {
            throw new NotFoundHttpException('User is not found');
        }

        if (!$this->hasAccess($user)) {
            throw new AccessDeniedHttpException('Action is forbidden');
        }

        return $user;
    }
}
