<?php

namespace Vrcci\Api\User\Http\Request;

use App\Helper\CurrentUserTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Vrcci\User\Dto\Attributes\UserAttributesVo;

/**
 * @OA\Schema(
 *     schema="UserUpdateRequest",
 *     @OA\Property(property="email", type="string", description="Email"),
 *     @OA\Property(property="password", type="string", description="Password"),
 * )
 */
class UserUpdateRequest extends FormRequest
{
    use CurrentUserTrait;

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            'email'    => [
                'required',
                'email',
                Rule::unique('users', 'email')->ignore($this->getUserId(), 'id'),
            ],
            'password' => [
                'nullable',
                'string',
                'min:6',
            ],
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return UserAttributesVo
     */
    public function getAttributes(): UserAttributesVo
    {
        return UserAttributesVo::fromArray($this->validated());
    }

    /**
     * @return int
     */
    protected function getUserId(): int
    {
        return $this->route('user_id');
    }
}
