<?php

use Vrcci\User\Http\UserController;

Route::middleware('auth:web')
    ->prefix('/users')
    ->name('vrcci.users.')
    ->group(
        function (): void {
            Route::get('list', UserController::class . '@listAction')
                ->name('list');
        }
    );

