<?php

namespace Vrcci\User\Providers;

use App\Providers\AutoMapperProvider as BaseAutoMapperProvider;
use Illuminate\Support\ServiceProvider;
use Vrcci\User\Mapper\UserMapperFactory;

class AutoMapperProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->tag(
            [
                UserMapperFactory::class,
            ],
            BaseAutoMapperProvider::FACTORY_TAG
        );
    }
}
