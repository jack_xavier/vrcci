<?php

namespace Vrcci\User\Dto\Attributes;

use App\Dto\Attributes\AbstractAttributesObject;

class UserAttributesVo extends AbstractAttributesObject
{
    /**
     * Returns available attribute names
     *
     * @return array
     */
    public static function attributes(): array
    {
        return [
            'email',
            'password',
        ];
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->get('email');
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->get('password');
    }
}
