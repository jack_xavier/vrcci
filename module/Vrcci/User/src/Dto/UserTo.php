<?php

namespace Vrcci\User\Dto;

use App\Dto\StoresOriginalModel;
use Vrcci\User\Entity\User;

/**
 * @method User getOriginal()
 */
class UserTo
{
    use StoresOriginalModel;

    /**
     * @var string
     */
    protected $email;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
