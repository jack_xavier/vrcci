<?php

namespace Vrcci\User\Dto\Resources;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\Extractor\DefaultResourceExtractor;
use App\Http\Resource\Extractor\ExtractorInterface;

/**
 * @OA\Schema(
 *     schema="User",
 *     @OA\Property(property="id", type="string", description="User id"),
 *     @OA\Property(property="email", type="string", description="User email"),
 * )
 */
class UserResource extends AbstractExtractableResource
{
    /**
     * @var array
     */
    protected $serializable = [
        'id',
        'email',
    ];

    /**
     * @param ExtractorInterface $extractor
     * @param object             $resource
     *
     * @return array
     */
    protected function extract(ExtractorInterface $extractor, $resource): array
    {
        if ($extractor instanceof DefaultResourceExtractor) {
            $extractor->properties($this->serializable);
        }

        $extractor->extract($resource);

        return $extractor->toArray();
    }
}
