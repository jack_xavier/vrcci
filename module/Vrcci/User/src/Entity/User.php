<?php

namespace Vrcci\User\Entity;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Vrcci\Auth\Jwt\Entity\AccessToken;
use Vrcci\User\Notification\PasswordResetNotification;

class User extends Authenticatable
{
    use Notifiable,
        LaratrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token): void
    {
        $this->notify(new PasswordResetNotification($token));
    }

    /**
     * @return HasOne
     */
    public function accessToken(): HasOne
    {
        return $this->hasOne(AccessToken::class, 'user_id', 'id');
    }
}
