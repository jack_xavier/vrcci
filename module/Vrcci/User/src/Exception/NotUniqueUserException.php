<?php

namespace App\Exceptions\User;

use Exception;
use Throwable;
use Vrcci\User\Dto\UserTo;
use Vrcci\User\Entity\User;

class NotUniqueUserException extends Exception
{
    const EMAIL_NOT_UNIQUE    = 1;
    const USERNAME_NOT_UNIQUE = 2;

    /**
     * @var User
     */
    protected $user;

    /**
     * @param UserTo         $user
     * @param Throwable|null $previous
     *
     * @return static
     */
    public static function byEmail(UserTo $user, Throwable $previous = null)
    {
        $message  = sprintf('User with Email [%s] already exists', $user->getEmail());
        $ex       = new static($message, static::EMAIL_NOT_UNIQUE, $previous);
        $ex->user = $user;

        return $ex;
    }
}
