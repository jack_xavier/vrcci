<?php

namespace Vrcci\User;

use App\Module\AbstractModule;
use App\Module\Feature\RoutesProviderInterface;
use App\Module\Feature\ViewsProviderInterface;
use Vrcci\User\Providers\AutoMapperProvider;

class Module extends AbstractModule implements RoutesProviderInterface, ViewsProviderInterface
{
    public const ALIAS = 'vrcci.user';

    /**
     * @return array
     */
    public function getRoutesPath(): array
    {
        return ['web' => __DIR__ . '/../routes/web.php'];
    }

    /**
     * @return string
     */
    public function getViewsPath(): string
    {
        return __DIR__ . '/../resources/views';
    }

    /**
     * @return string
     */
    public static function getAlias()
    {
        return static::ALIAS;
    }

    /**
     * @inheritdoc
     */
    public function register()
    {
        parent::register();

        $this->app->register(AutoMapperProvider::class);
    }

    /**
     * @inheritdoc
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }
}
