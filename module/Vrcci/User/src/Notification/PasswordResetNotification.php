<?php

namespace Vrcci\User\Notification;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordResetNotification extends ResetPassword
{
    /**
     * @param mixed $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        $query = http_build_query(['token' => $this->token]);
        $url   = url(sprintf('%s/password-reset?%s', config('app.url'), $query));

        return (new MailMessage())
            ->subject(__('reset-pass-mail.subject'))
            ->markdown('mail/templates/reset-password', ['url' => $url]);
    }
}
