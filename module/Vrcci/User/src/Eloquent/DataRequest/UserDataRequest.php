<?php

namespace Vrcci\User\Eloquent\DataRequest;

use App\Eloquent\AbstractDataRequest;

class UserDataRequest extends AbstractDataRequest
{
    /**
     * @param string $email
     *
     * @return UserDataRequest
     */
    public function byEmail(string $email): self
    {
        $this->qb->where('email', $email);

        return $this;
    }
}
