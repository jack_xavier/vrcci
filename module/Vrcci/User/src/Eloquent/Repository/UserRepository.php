<?php

namespace Vrcci\User\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Vrcci\User\Dto\UserTo;
use Vrcci\User\Eloquent\DataRequest\UserDataRequest;
use Vrcci\User\Entity\User;

class UserRepository
{
    /**
     * @var AutoMapperInterface
     */
    protected $mapper;

    /**
     * UserRepository constructor.
     *
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param int $id
     *
     * @return UserTo|null
     */
    public function find(int $id)
    {
        try {
            $user = User::with([])->findOrFail($id);

            return $this->mapper->mapToObject($user, new UserTo());
        } catch (ModelNotFoundException $exception) {
            return null;
        }
    }

    /**
     * @return UserDataRequest
     */
    public function fetchAll(): UserDataRequest
    {
        $dataRequest = UserDataRequest::create(User::with([]));
        $dataRequest->withTransformer(
            function (User $user) {
                return $this->mapper->mapToObject($user, new UserTo());
            }
        );

        return $dataRequest;
    }
}
