<?php

namespace Vrcci\User\Service;

use App;
use App\Entity\Role;
use App\Exceptions\User\NotUniqueUserException;
use App\Helper\CurrentUserTrait;
use AutoMapperPlus\AutoMapperInterface;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\AuthManager;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Vrcci\Auth\Common\Enum\AccessEnum;
use Vrcci\Template\Helper\DefaultTemplateHelper;
use Vrcci\User\Dto\Attributes\UserAttributesVo;
use Vrcci\User\Dto\UserTo;
use Vrcci\User\Eloquent\Repository\UserRepository;
use Vrcci\User\Entity\User;

class UserService
{
    use CurrentUserTrait;

    /**
     * @var AuthManager
     */
    protected $auth;

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var AutoMapperInterface
     */
    protected $mapper;

    /**
     * @param AuthManager         $auth
     * @param UserRepository      $repository
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AuthManager $auth, UserRepository $repository, AutoMapperInterface $mapper)
    {
        $this->auth       = $auth;
        $this->repository = $repository;
        $this->mapper     = $mapper;
    }

    /**
     * @throws AuthenticationException
     * @return UserTo
     */
    public function getCurrent(): UserTo
    {
        /** @var User $user */
        $user = $this->auth->guard()->user();

        if (!$user) {
            throw new AuthenticationException('User is not authenticated');
        }

        return $this->mapper->mapToObject($user, new UserTo());
    }

    /**
     * @param UserAttributesVo $attributes
     * @param string           $role
     *
     * @throws NotUniqueUserException
     * @return UserTo
     */
    public function create(UserAttributesVo $attributes, string $role = AccessEnum::R_USER): UserTo
    {
        $user = null;
        $user = $this->repository->fetchAll()->byEmail($attributes->getEmail())->first();

        if ($user instanceof UserTo) {
            throw NotUniqueUserException::byEmail($user);
        }

        $user = new User();
        $user->fill($attributes->toArray());
        $user->password = $attributes->getPassword() !== null ? bcrypt($attributes->getPassword()) : null;
        $user->accessToken()->create(['token' => hash('sha256', Str::random(60))]);

        DB::transaction(
            function () use ($user, $role) {
                $user->save();
                $this->updateRole($user, $role);
            }
        );

        return $this->mapper->mapToObject($user, new UserTo());
    }

    /**
     * @param UserTo           $user
     * @param UserAttributesVo $attributes
     *
     * @return UserTo
     */
    public function update(UserTo $user, UserAttributesVo $attributes): UserTo
    {
        $original = $user->getOriginal();
        $original->fill($attributes->toArray());

        if (null !== $attributes->getPassword()) {
            $original->password = bcrypt($attributes->getPassword());
        }

        $original->save();

        return $this->mapper->mapToObject($original, $user);
    }

    /**
     * @param User   $user
     * @param string $role
     *
     * @return User
     */
    public function updateRole(User $user, string $role)
    {
        $user->roles()->sync([Role::where('name', $role)->firstOrFail()->id]);

        return $user;
    }
}
