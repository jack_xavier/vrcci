<?php

namespace Vrcci\User\Http;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\User\Eloquent\Repository\UserRepository;

class UserController extends Controller implements ModuleAliasAwareInstance
{
    use GuardForCompanyGuidTrait;
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     *
     * @throws UnregisteredMappingException
     *
     * @return View
     */
    public function listAction(Request $request): View
    {
        $dataRequest = $this->userRepository
            ->fetchAll();

        $paginator = $dataRequest->withPage(
            $request->get('page') ?? 1,
            15
        );

        return $this->makeView('list')
            ->with('users', $paginator);
    }
}
