<?php

namespace Vrcci\User\Mapper;

use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\MappingFactoryInterface;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\User\Dto\UserTo;
use Vrcci\User\Entity\User;

class UserMapperFactory implements MappingFactoryInterface
{
    /**
     * @param AutoMapperConfig $config
     */
    public function registerMapping(AutoMapperConfig $config)
    {
        $config->registerMapping(User::class, UserTo::class)
            ->forMember('originalModel', new MapOriginal());
    }
}
