<?php

namespace Vrcci\Auth\Common\UserProvider;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class VrcciUserProvider extends EloquentUserProvider
{
    /**
     * @param UserContract $user
     * @param array        $credentials
     *
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials): bool
    {
        if (null === $user->getAuthPassword()) {
            return false;
        }

        return parent::validateCredentials($user, $credentials);
    }
}
