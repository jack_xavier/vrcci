<?php

namespace Vrcci\Auth\Common;

use App\Module\AbstractModule;
use Vrcci\Auth\Common\Provider\AuthServiceProvider;

class Module extends AbstractModule
{
    public const ALIAS = 'auth.common';

    /**
     * @return string
     */
    public static function getAlias(): string
    {
        return static::ALIAS;
    }

    /**
     * @return void
     */
    public function register(): void
    {
        $this->loadMigrationsFrom(dirname(__DIR__) . '/database/migrations');

        $this->app->register(AuthServiceProvider::class);
    }
}
