<?php

namespace Vrcci\Auth\Common\Provider;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Vrcci\Auth\Common\UserProvider\VrcciUserProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerPolicies();

        Auth::provider(
            'vrcci',
            function ($app, array $config) {
                return new VrcciUserProvider($app['hash'], $config['model']);
            }
        );
    }
}
