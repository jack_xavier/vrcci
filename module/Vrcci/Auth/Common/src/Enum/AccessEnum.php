<?php

namespace Vrcci\Auth\Common\Enum;

class AccessEnum
{
    public const R_ADMIN = 'role.admin';
    public const R_USER  = 'role.user';

    public const P_FULL_LOOKUP = 'permission.full.lookup';

    /**
     * @return string[]
     */
    public static function getAvailableRoles(): array
    {
        return [
            static::R_ADMIN,
            static::R_USER,
        ];
    }
}
