<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;
use Vrcci\User\Entity\User;

class SeedUserTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        /** @var User $user */
        foreach (User::all()->all() as $user) {
            $user->accessToken()->create(['token' => hash('sha256', Str::random(60))]);
            $user->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('user_access_tokens')->truncate();
    }
}
