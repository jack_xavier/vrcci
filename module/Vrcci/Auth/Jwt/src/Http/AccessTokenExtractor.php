<?php

namespace Vrcci\Auth\Jwt\Http;

use Illuminate\Http\Request;
use Vrcci\Auth\Jwt\Entity\AccessToken;

class AccessTokenExtractor
{
    /**
     * @param Request $request
     *
     * @return string|null
     */
    public function extractTokenFromRequest(Request $request): ?string
    {
        return $request->get('access_token') ?? null;
    }

    /**
     * @param Request $request
     *
     * @return AccessToken|null
     */
    public function extractValidTokenOrFail(Request $request): ?AccessToken
    {
        if (null === $token = $this->extractTokenFromRequest($request)) {
            return null;
        }

        return AccessToken::all()->where('token', $token)->first();
    }
}
