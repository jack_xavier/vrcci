<?php

namespace Vrcci\Auth\Jwt\Http\Auth;

use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;
use Vrcci\Auth\Jwt\Http\AccessTokenExtractor;
use Vrcci\User\Entity\User;

class AccessTokenGuard implements Guard
{
    use GuardHelpers;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var User|Authenticatable|null
     */
    protected $user;

    /**
     * @var AccessTokenExtractor
     */
    protected $tokenExtractor;

    /**
     * @param AccessTokenExtractor $tokenExtractor
     * @param Request              $request
     * @param UserProvider         $provider
     */
    public function __construct(
        AccessTokenExtractor $tokenExtractor,
        Request $request,
        UserProvider $provider
    ) {
        $this->request = $request;
        $this->provider = $provider;
        $this->tokenExtractor = $tokenExtractor;
    }

    /**
     * @return Authenticatable|null
     */
    public function user(): ?Authenticatable
    {
        if (null !== $this->user) {
            return $this->user;
        }

        $token = $this->tokenExtractor->extractValidTokenOrFail($this->request);

        if (null === $token) {
            return null;
        }

        return $this->user = $this->provider->retrieveById((int) $token->user_id);
    }

    /**
     * @param array $credentials
     *
     * @return bool
     */
    public function validate(array $credentials = []): bool
    {
        return null !== (clone $this)->setRequest($credentials['request'])->setUser()->user();
    }

    /**
     * Set the current request instance.
     *
     * @param Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request): self
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @param Authenticatable $user
     *
     * @return $this
     */
    public function setUser(?Authenticatable $user = null): self
    {
        $this->user = $user;

        return $this;
    }
}
