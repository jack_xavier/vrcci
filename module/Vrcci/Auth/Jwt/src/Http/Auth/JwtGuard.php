<?php

namespace Vrcci\Auth\Jwt\Http\Auth;

use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;
use Vrcci\Auth\Jwt\Exception\JwtAuthenticationException;
use Vrcci\Auth\Jwt\Http\JwtTokenExtractor;
use Vrcci\User\Entity\User;

class JwtGuard implements Guard
{
    use GuardHelpers;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var User|Authenticatable|null
     */
    protected $user;

    /**
     * @var JwtTokenExtractor
     */
    protected $jwtTokenExtractor;

    /**
     * JwtGuard constructor.
     *
     * @param JwtTokenExtractor $jwtTokenExtractor
     * @param Request           $request
     * @param UserProvider      $provider
     */
    public function __construct(
        JwtTokenExtractor $jwtTokenExtractor,
        Request $request,
        UserProvider $provider
    ) {
        $this->request           = $request;
        $this->provider          = $provider;
        $this->jwtTokenExtractor = $jwtTokenExtractor;
    }

    /**
     * Get the currently authenticated user.
     *
     * @return Authenticatable|null
     *
     * @throws JwtAuthenticationException
     */
    public function user(): ?Authenticatable
    {
        if (null !== $this->user) {
            return $this->user;
        }

        $token = $this->jwtTokenExtractor->extractValidTokenOrFail($this->request);

        if (null === $token) {
            return null;
        }

        return $this->user = $this->provider->retrieveById((int)$token->getUserId());
    }

    /**
     * Validate a user's credentials.
     *
     * @param array $credentials
     *
     * @return bool
     */
    public function validate(array $credentials = []): bool
    {
        try {
            return (clone $this)->setRequest($credentials['request'])->setUser()->user() !== null;
        } catch (JwtAuthenticationException $exception) {
            return false;
        }
    }

    /**
     * Set the current request instance.
     *
     * @param  Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request): self
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @param Authenticatable $user
     *
     * @return $this
     */
    public function setUser(?Authenticatable $user = null): self
    {
        $this->user = $user;

        return $this;
    }
}
