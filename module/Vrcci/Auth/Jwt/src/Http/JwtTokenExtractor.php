<?php

namespace Vrcci\Auth\Jwt\Http;


use Illuminate\Http\Request;
use Vrcci\Auth\Jwt\Dto\JwtTokenTo;
use Vrcci\Auth\Jwt\Enum\JwtTypeEnum;
use Vrcci\Auth\Jwt\Exception\JwtAuthenticationException;
use Vrcci\Auth\Jwt\Service\JwtTokenService;

class JwtTokenExtractor
{
    /**
     * @var string
     */
    protected $refreshTokenInputKey;

    /**
     * @var JwtTokenService
     */
    private $tokenService;

    public function __construct(JwtTokenService $tokenService, string $refreshTokenInputKey = 'refresh_token')
    {
        $this->refreshTokenInputKey = $refreshTokenInputKey;
        $this->tokenService         = $tokenService;
    }

    /**
     * @param Request $request
     * @param string  $type
     *
     * @return JwtTokenTo|null
     *
     * @throws JwtAuthenticationException
     */
    public function extractTokenFromRequest(Request $request, string $type = JwtTypeEnum::ACCESS): ?JwtTokenTo
    {
        $token = null;

        switch ($type) {
            case JwtTypeEnum::REFRESH:
                $token = $request->query($this->refreshTokenInputKey);

                if (empty($token)) {
                    $token = $request->input($this->refreshTokenInputKey);
                }
                break;

            case JwtTypeEnum::ACCESS:
                $token = $request->bearerToken();
                break;
        }

        if (empty($token)) {
            return null;
        }

        return $this->tokenService->decode($token);
    }

    /**
     * @param Request $request
     * @param string  $type
     *
     * @return JwtTokenTo|null Returns valid token if exists in request or null
     *
     * @throws JwtAuthenticationException Thrown if token exists but invalid
     */
    public function extractValidTokenOrFail(Request $request, string $type = JwtTypeEnum::ACCESS): ?JwtTokenTo
    {
        if (null === $token = $this->extractTokenFromRequest($request, $type)) {
            return null;
        }


        $this->tokenService->verify($token, $type);

        return $token;
    }
}
