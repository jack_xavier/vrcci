<?php

namespace Vrcci\Auth\Jwt\Entity;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Vrcci\User\Entity\User;

/**
 * @property integer  $id
 * @property integer  $user_id
 * @property boolean  $revoked
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Jwt extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_jwt_tokens';

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
