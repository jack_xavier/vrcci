<?php

namespace Vrcci\Auth\Jwt\Entity;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Vrcci\User\Entity\User;

/**
 * @property integer  $id
 * @property integer  $user_id
 * @property string   $token
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class AccessToken extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_access_tokens';

    /**
     * @var string[]
     */
    protected $fillable = [
        'token',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
