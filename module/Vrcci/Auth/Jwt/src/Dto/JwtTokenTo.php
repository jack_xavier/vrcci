<?php

namespace Vrcci\Auth\Jwt\Dto;

use Lcobucci\JWT\Token;

class JwtTokenTo
{
    /**
     * @var string
     */
    protected $token;

    /**
     * JwtTokenTo constructor.
     *
     * @param Token $token
     */
    public function __construct(Token $token)
    {
        $this->token = $token;
    }

    /**
     * @return Token
     */
    public function getToken(): Token
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->token->getClaim('type');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->token->getClaim('jti');
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return (int)$this->token->getClaim('sub');
    }

    /**
     * @return int|null
     */
    public function getIssuedTimestamp(): ?int
    {
        if (!$this->token->hasClaim('iat')) {
            return null;
        }

        return $this->token->getClaim('iat');
    }

    /**
     * @return int|null
     */
    public function getExpiredTimestamp(): ?int
    {
        if (!$this->token->hasClaim('exp')) {
            return null;
        }

        return $this->token->getClaim('exp');
    }
}
