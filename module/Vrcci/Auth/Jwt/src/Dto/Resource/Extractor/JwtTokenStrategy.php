<?php

namespace Vrcci\Auth\Jwt\Dto\Resource\Extractor;

use App\Http\Resource\Extractor\Strategy\StrategyInterface;
use Vrcci\Auth\Jwt\Dto\JwtPairTo;
use Vrcci\Auth\Jwt\Dto\JwtTokenTo;

class JwtTokenStrategy implements StrategyInterface
{
    /**
     * @param JwtTokenTo     $value
     * @param JwtPairTo|null $context
     *
     * @return mixed[]
     */
    public function extract($value, $context = null): array
    {
        return [
            'token'     => (string)$value->getToken(),
            'id'        => $value->getId(),
            'userId'    => $value->getUserId(),
            'issuedAt'  => $value->getIssuedTimestamp(),
            'expiredAt' => $value->getExpiredTimestamp(),
        ];
    }
}
