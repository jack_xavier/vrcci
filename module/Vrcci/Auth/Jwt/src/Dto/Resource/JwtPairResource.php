<?php

namespace Vrcci\Auth\Jwt\Dto\Resource;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\Extractor\DefaultResourceExtractor;
use App\Http\Resource\Extractor\ExtractorInterface;
use App\Http\Resource\Extractor\Strategy\MapNamingStrategy;
use App\Http\Resource\Helper\MessageResourceTrait;
use App\Http\Resource\Helper\MetaProviderTrait;
use App\Http\Resource\Helper\RedirectResourceTrait;
use Vrcci\Auth\Jwt\Dto\JwtPairTo;
use Vrcci\Auth\Jwt\Dto\Resource\Extractor\JwtTokenStrategy;

/**
 * @OA\Schema(
 *     schema="JwtToken",
 *     @OA\Property(property="token", type="string", example="base64(json).base64(json).base64(signature)", description="Token itself in string form"),
 *     @OA\Property(property="id", type="integer", example=12, description="ID of token (jti)"),
 *     @OA\Property(property="userId", type="integer", example=1, description="User's ID"),
 *     @OA\Property(property="issuedAt", type="integer", example=1541505947, description="Time when token was created in unix timestamp form"),
 *     @OA\Property(property="expiredAt", type="integer", example=1549281947, description="Time when token will expired in unix timestamp form"),
 * )
 *
 * @OA\Schema(
 *     schema="JwtTokenPair",
 *     @OA\Property(property="access", ref="#/components/schemas/JwtToken", description="Access token"),
 *     @OA\Property(property="refresh", ref="#/components/schemas/JwtToken", description="Refresh token"),
 * )
 */
class JwtPairResource extends AbstractExtractableResource
{
    use MetaProviderTrait,
        MessageResourceTrait,
        RedirectResourceTrait;

    /**
     * @var string[]
     */
    protected $serializable = [
        'accessToken',
        'refreshToken',
    ];

    /**
     * @param ExtractorInterface $extractor
     * @param JwtPairTo          $resource
     *
     * @return array
     */
    protected function extract(ExtractorInterface $extractor, $resource): array
    {
        if ($extractor instanceof DefaultResourceExtractor) {
            $extractor->properties($this->serializable);

            $extractor->addStrategy(
                'accessToken',
                new JwtTokenStrategy()
            );

            $extractor->addStrategy(
                'refreshToken',
                new JwtTokenStrategy()
            );

            $extractor->setNamingStrategy(
                new MapNamingStrategy(
                    [
                        'accessToken'  => 'access',
                        'refreshToken' => 'refresh',
                    ]
                )
            );
        }

        $extractor->extract($resource);

        return $extractor->toArray();
    }
}
