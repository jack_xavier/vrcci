<?php

namespace Vrcci\Auth\Jwt\Dto;

class JwtPairTo
{
    /**
     * @var JwtTokenTo
     */
    protected $accessToken;

    /**
     * @var JwtTokenTo
     */
    protected $refreshToken;

    /**
     * JwtPairTo constructor.
     *
     * @param JwtTokenTo $accessToken
     * @param JwtTokenTo $refreshToken
     */
    public function __construct(JwtTokenTo $accessToken, JwtTokenTo $refreshToken)
    {
        $this->accessToken  = $accessToken;
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return JwtTokenTo
     */
    public function getAccessToken(): JwtTokenTo
    {
        return $this->accessToken;
    }

    /**
     * @return JwtTokenTo
     */
    public function getRefreshToken(): JwtTokenTo
    {
        return $this->refreshToken;
    }
}
