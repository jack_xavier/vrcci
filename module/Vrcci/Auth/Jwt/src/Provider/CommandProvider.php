<?php

namespace Vrcci\Auth\Jwt\Provider;

use Illuminate\Support\ServiceProvider;
use Vrcci\Auth\Jwt\Console\Command\GenerateKeysCommand;
use Vrcci\Auth\Jwt\Module;

class CommandProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {
        $this->commands(GenerateKeysCommand::class);

        $this->app
            ->when(GenerateKeysCommand::class)
            ->needs('$publicKey')
            ->give(Module::getConfig('keys.public'));

        $this->app
            ->when(GenerateKeysCommand::class)
            ->needs('$privateKey')
            ->give(Module::getConfig('keys.private'));
    }
}
