<?php

namespace Vrcci\Auth\Jwt\Provider;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Vrcci\Auth\Jwt\Http\JwtTokenExtractor;
use Vrcci\Auth\Jwt\Module;
use Vrcci\Auth\Jwt\Service\Config\Configuration;
use Vrcci\Auth\Jwt\Service\Config\FileKey;
use Vrcci\Auth\Jwt\Service\JwtTokenService;

class JwtServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app
            ->singleton(
                JwtTokenService::class,
                function (Application $app) {
                    $public  = new FileKey(Module::getConfig('keys.public'));
                    $private = new FileKey(Module::getConfig('keys.private'));

                    $config = (new Configuration($public, $private))
                        ->setAccessTokenLifetime(Module::getConfig('access_token_lifetime'))
                        ->setRefreshTokenLifetime(Module::getConfig('refresh_token_lifetime'));

                    return new JwtTokenService($config);
                }
            );

        $this->app
            ->when(JwtTokenExtractor::class)
            ->needs('$refreshTokenInputKey')
            ->give(Module::getConfig('refresh_token_input_key', 'refresh_token'));
    }
}
