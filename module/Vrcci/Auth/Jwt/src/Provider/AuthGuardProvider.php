<?php

namespace Vrcci\Auth\Jwt\Provider;

use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Vrcci\Auth\Jwt\Http\AccessTokenExtractor;
use Vrcci\Auth\Jwt\Http\Auth\AccessTokenGuard;
use Vrcci\Auth\Jwt\Http\Auth\JwtGuard;
use Vrcci\Auth\Jwt\Http\JwtTokenExtractor;

class AuthGuardProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        Auth::extend(
            'jwt',
            function (Application $app, $name, array $config) {
                return new JwtGuard(
                    $app->make(JwtTokenExtractor::class),
                    $app->make(Request::class),
                    Auth::createUserProvider($config['provider'])
                );
            }
        );

        Auth::extend(
            'access_token',
            function (Application $app, $name, array $config) {
                return new AccessTokenGuard(
                    $app->make(AccessTokenExtractor::class),
                    $app->make(Request::class),
                    Auth::createUserProvider($config['provider'])
                );
            }
        );
    }
}
