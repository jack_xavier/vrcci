<?php

namespace Vrcci\Auth\Jwt\Service;

use Carbon\Carbon;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\ValidationData;
use Vrcci\Auth\Jwt\Dto\JwtPairTo;
use Vrcci\Auth\Jwt\Dto\JwtTokenTo;
use Vrcci\Auth\Jwt\Entity\Jwt as JwtEntity;
use Vrcci\Auth\Jwt\Enum\JwtStatusCodeEnum;
use Vrcci\Auth\Jwt\Enum\JwtTypeEnum;
use Vrcci\Auth\Jwt\Exception\JwtAuthenticationException;
use Vrcci\Auth\Jwt\Service\Config\Configuration;

class JwtTokenService
{
    /**
     * @var Configuration
     */
    protected $configuration;

    /**
     * @var Sha256
     */
    protected $signer;

    /**
     * @var Parser
     */
    protected $parser;

    /**
     * JwtTokenService constructor.
     *
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;

        $this->parser = new Parser();
        $this->signer = new Sha256();
    }

    /**
     * @param string $token
     *
     * @return JwtTokenTo
     *
     * @throws JwtAuthenticationException
     */
    public function decode(string $token): JwtTokenTo
    {
        try {
            return new JwtTokenTo($this->parser->parse($token));
        } catch (\InvalidArgumentException $exception) {
            throw JwtAuthenticationException::forCode(JwtStatusCodeEnum::BROKEN);
        }
    }

    /**
     * @param JwtTokenTo $tokenTo
     * @param string     $type
     *
     * @throws JwtAuthenticationException
     */
    public function verify(JwtTokenTo $tokenTo, string $type = JwtTypeEnum::ACCESS): void
    {
        $token = $tokenTo->getToken();

        if (!$token->verify($this->signer, $this->configuration->getPublicKey())) {
            throw JwtAuthenticationException::forCode(JwtStatusCodeEnum::SIGN_PROBLEMS);
        }

        if ($tokenTo->getType() !== $type) {
            throw JwtAuthenticationException::forCode(JwtStatusCodeEnum::WRONG_TYPE);
        }

        if (!$token->validate(new ValidationData(Carbon::now()->getTimestamp()))) {
            throw JwtAuthenticationException::forCode(JwtStatusCodeEnum::EXPIRED);
        }

        // Only refresh tokens should be checked
        // Because access tokens have short life
        if ($type === JwtTypeEnum::REFRESH) {
            $exists = JwtEntity::where(
                [
                    ['id', '=', $tokenTo->getId()],
                    ['user_id', '=', $tokenTo->getUserId()],
                ]
            )->exists();

            if (!$exists) {
                throw JwtAuthenticationException::forCode(JwtStatusCodeEnum::REVOKED);
            }
        }
    }

    /**
     * @param int $userId
     */
    public function revokeForUser(int $userId): void
    {
        JwtEntity::where('user_id', '=', $userId)->delete();
    }

    /**
     * @param int $jti
     */
    public function revoke(int $jti): void
    {
        JwtEntity::where('id', '=', $jti)->delete();
    }

    /**
     * @param JwtTokenTo $refreshToken
     *
     * @return JwtPairTo
     *
     * @throws JwtAuthenticationException
     */
    public function refreshPair(JwtTokenTo $refreshToken): JwtPairTo
    {
        $this->verify($refreshToken, JwtTypeEnum::REFRESH);

        // Revoke previous token
        $this->revoke($refreshToken->getId());

        return $this->generatePair($refreshToken->getUserId());
    }

    /**
     * @param int $userId
     *
     * @return JwtPairTo
     */
    public function generatePair(int $userId): JwtPairTo
    {
        $jwt = new JwtEntity();
        $jwt->user()->associate($userId);
        $jwt->save();

        $accessToken  = $this->buildToken($jwt, JwtTypeEnum::ACCESS, $this->configuration->getAccessTokenLifetime());
        $refreshToken = $this->buildToken($jwt, JwtTypeEnum::REFRESH, $this->configuration->getRefreshTokenLifetime());

        return new JwtPairTo($accessToken, $refreshToken);
    }

    /**
     * @param JwtEntity $jwt
     * @param string    $type
     * @param int|null  $lifetime in seconds
     *
     * @return JwtTokenTo
     */
    private function buildToken(JwtEntity $jwt, string $type, ?int $lifetime): JwtTokenTo
    {
        $builder = (new Builder())
            ->set('type', $type)
            ->setSubject((int)$jwt->user_id)
            ->setId((int)$jwt->id);

        if (null !== $lifetime) {
            $builder->setExpiration(Carbon::now()->addSeconds($lifetime)->getTimestamp());
        }

        return $this->encode($builder);
    }

    /**
     * @param Builder $builder
     *
     * @return JwtTokenTo
     */
    protected function encode(Builder $builder): JwtTokenTo
    {
        $token = $builder
            ->setIssuedAt(Carbon::now()->getTimestamp())
            ->sign($this->signer, $this->configuration->getPrivateKey())
            ->getToken();

        return new JwtTokenTo($token);
    }
}
