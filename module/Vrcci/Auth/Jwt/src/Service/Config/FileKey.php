<?php

namespace Vrcci\Auth\Jwt\Service\Config;

class FileKey implements KeyInterface
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var StringKey
     */
    protected $key;

    /**
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return void
     */
    protected function ensureKey(): void
    {
        if (null === $this->key) {
            $this->key = new StringKey(file_get_contents($this->path));
        }
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        $this->ensureKey();

        return $this->key->getKey();
    }
}
