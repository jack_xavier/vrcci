<?php

namespace Vrcci\Auth\Jwt\Service\Config;

interface KeyInterface
{
    /**
     * @return string
     */
    public function getKey(): string;
}
