<?php

namespace Vrcci\Auth\Jwt\Service\Config;

class Configuration
{
    /**
     * @var KeyInterface
     */
    protected $publicKey;

    /**
     * @var KeyInterface
     */
    protected $privateKey;

    /**
     * @var int|null Null means infinite
     */
    protected $accessTokenLifetime;

    /**
     * @var int|null Null means infinite
     */
    protected $refreshTokenLifetime;

    /**
     * @param KeyInterface $publicKey
     * @param KeyInterface $privateKey
     */
    public function __construct(KeyInterface $publicKey, KeyInterface $privateKey)
    {
        $this->publicKey  = $publicKey;
        $this->privateKey = $privateKey;
    }

    /**
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->publicKey->getKey();
    }

    /**
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->privateKey->getKey();
    }

    /**
     * @param int|null $accessTokenLifetime
     *
     * @return $this
     */
    public function setAccessTokenLifetime(?int $accessTokenLifetime): self
    {
        $this->accessTokenLifetime = $accessTokenLifetime;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAccessTokenLifetime(): ?int
    {
        return $this->accessTokenLifetime;
    }

    /**
     * @param int|null $refreshTokenLifetime
     *
     * @return $this
     */
    public function setRefreshTokenLifetime(?int $refreshTokenLifetime): self
    {
        $this->refreshTokenLifetime = $refreshTokenLifetime;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRefreshTokenLifetime(): ?int
    {
        return $this->refreshTokenLifetime;
    }
}
