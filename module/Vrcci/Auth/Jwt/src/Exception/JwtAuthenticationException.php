<?php

namespace Vrcci\Auth\Jwt\Exception;

use Illuminate\Auth\AuthenticationException;
use Vrcci\Auth\Jwt\Enum\JwtStatusCodeEnum;

class JwtAuthenticationException extends AuthenticationException
{
    public const MESSAGES = [
        JwtStatusCodeEnum::EXPIRED       => 'Token is expired',
        JwtStatusCodeEnum::BROKEN        => 'Token is not available to parse',
        JwtStatusCodeEnum::WRONG_TYPE    => 'Token has wrong type for this request',
        JwtStatusCodeEnum::REVOKED       => 'Token is revoked',
        JwtStatusCodeEnum::SIGN_PROBLEMS => 'Unable to verify signature',
    ];

    /**
     * JwtAuthenticationException constructor.
     *
     * @param string $message
     * @param int    $code
     * @param array  $guards
     */
    public function __construct(
        string $message = 'Unauthenticated',
        int $code = 0,
        array $guards = []
    ) {
        parent::__construct($message, $guards);

        $this->code = $code;
    }

    /**
     * @param int $code
     *
     * @return self
     */
    public static function forCode(int $code): self
    {
        return new self(self::MESSAGES[$code] ?? 'Unauthenticated', $code);
    }
}
