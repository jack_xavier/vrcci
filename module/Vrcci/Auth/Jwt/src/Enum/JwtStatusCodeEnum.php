<?php

namespace Vrcci\Auth\Jwt\Enum;

class JwtStatusCodeEnum
{
    public const EXPIRED = 10;

    /**
     * Unable to parse JWT token
     */
    public const BROKEN     = 11;
    public const WRONG_TYPE = 12;
    public const REVOKED    = 13;

    /**
     * It can be happen when keys regenerated
     */
    public const SIGN_PROBLEMS = 14;
}
