<?php

namespace Vrcci\Auth\Jwt\Enum;

class JwtTypeEnum
{
    public const ACCESS = 'access';

    public const REFRESH = 'refresh';
}
