<?php

namespace Vrcci\Auth\Jwt;

use App\Module\AbstractModule;
use App\Module\Feature\ConfigProviderInterface;
use Illuminate\Support\Facades\Config;

class Module extends AbstractModule implements ConfigProviderInterface
{
    public const ALIAS = 'vrcci.auth.jwt';

    /**
     * @return string
     */
    public static function getAlias(): string
    {
        return static::ALIAS;
    }

    /**
     * @return void
     */
    public function register(): void
    {
        $this->loadMigrationsFrom(dirname(__DIR__) . '/database/migrations');

        $this->app->register(Provider\JwtServiceProvider::class);
        $this->app->register(Provider\CommandProvider::class);
    }

    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app->register(Provider\AuthGuardProvider::class);
    }

    /**
     * @return string
     */
    public function getConfigPath(): string
    {
        return dirname(__DIR__) . '/config/config.php';
    }

    /**
     * @param string     $key
     * @param mixed|null $default
     *
     * @return mixed|null
     */
    public static function getConfig(string $key, $default = null)
    {
        return Config::get(self::getAlias() . '.' . $key, $default);
    }
}
