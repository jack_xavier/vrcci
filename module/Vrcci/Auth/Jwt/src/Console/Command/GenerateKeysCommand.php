<?php

namespace Vrcci\Auth\Jwt\Console\Command;

use Illuminate\Console\Command;
use phpseclib\Crypt\RSA;

/**
 * Class GenerateKeys
 *
 * @package Vrcci\NotificationDelivery\Console\Command
 */
class GenerateKeysCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'jwt:keys
                                      {--force : Overwrite keys they already exist}
                                      {--length=4096 : The length of the private key}';

    /**
     * @var string
     */
    protected $description = 'Create the encryption keys for API authentication';

    /**
     * @var string
     */
    protected $publicKey;

    /**
     * @var string
     */
    protected $privateKey;

    public function __construct(string $publicKey, string $privateKey)
    {
        parent::__construct();

        $this->publicKey  = $publicKey;
        $this->privateKey = $privateKey;
    }

    /**
     * Generates pair of public and private keys
     *
     * @param RSA $rsa
     */
    public function handle(RSA $rsa)
    {
        $keys = $rsa->createKey($this->input ? (int)$this->option('length') : 4096);

        $publicKey  = $this->publicKey;
        $privateKey = $this->privateKey;

        if ((file_exists($publicKey) || file_exists($privateKey)) && !$this->option('force')) {
            $this->error('Encryption keys already exist. Use the --force option to overwrite them.');

            return;
        }

        file_put_contents($publicKey, array_get($keys, 'publickey'));
        file_put_contents($privateKey, array_get($keys, 'privatekey'));

        $this->info('Encryption keys generated successfully.');
    }
}
