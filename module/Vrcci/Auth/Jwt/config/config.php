<?php

return [
    'access_token_lifetime'   => env('JWT_ACCESS_TTL', 1 * 30 * 24 * 60 * 60), // hour by default
    'refresh_token_lifetime'  => env('JWT_REFRESH_TTL', 3 * 30 * 24 * 60 * 60), // three months by default
    'refresh_token_input_key' => 'refreshToken',
    'keys'                    => [
        'private' => env('JWT_PRIVATE_KEY'),
        'public'  => env('JWT_PUBLIC_KEY'),
    ],
];
