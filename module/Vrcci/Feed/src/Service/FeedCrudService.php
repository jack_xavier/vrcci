<?php

namespace Vrcci\Feed\Service;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Exception;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Feed\Dto\Attributes\FeedAttributesVo;
use Vrcci\Feed\Dto\FeedTo;
use Vrcci\Feed\Eloquent\Entity\Feed;
use Vrcci\User\Dto\UserTo;

class FeedCrudService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param FeedAttributesVo $attributes
     * @param CompanyTo        $companyTo
     * @param UserTo           $user
     *
     * @throws UnregisteredMappingException
     *
     * @return FeedTo
     */
    public function create(FeedAttributesVo $attributes, CompanyTo $companyTo, UserTo $user): FeedTo
    {
        $feed = (new Feed())->fill($attributes->toArray());

        $feed->createdBy()->associate($user->getOriginal());
        $feed->company()->associate($companyTo->getOriginal());

        $feed->save();

        return $this->mapper->mapToObject($feed, new FeedTo());
    }

    /**
     * @param FeedTo           $feedTo
     * @param FeedAttributesVo $attributes
     *
     * @throws UnregisteredMappingException
     *
     * @return FeedTo
     */
    public function update(FeedTo $feedTo, FeedAttributesVo $attributes): FeedTo
    {
        $original = $feedTo->getOriginal();
        $original->fill($attributes->toArray());
        $original->save();

        return $this->mapper->mapToObject($original, $feedTo);
    }

    /**
     * @param FeedTo $feedTo
     *
     * @throws Exception
     *
     * @return void
     */
    public function delete(FeedTo $feedTo): void
    {
        $feedTo->getOriginal()->delete();
    }
}
