<?php

namespace Vrcci\Feed\Http\Controller;

use App\Helper\CurrentUserTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Feed\Http\Request\FeedCreateRequest;
use Vrcci\Api\Feed\Http\Request\FeedEditRequest;
use Vrcci\Api\Feed\Http\Request\FeedFilterRequest;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Feed\Dto\FeedTo;
use Vrcci\Feed\Eloquent\Repository\FeedRepository;
use Vrcci\Feed\Service\FeedCrudService;

class CompanyFeedController extends Controller implements ModuleAliasAwareInstance
{
    use GuardForCompanyGuidTrait;
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;

    /**
     * @var FeedCrudService
     */
    private $feedService;

    /**
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * @param CompanyRepository $companyRepository
     * @param FeedCrudService   $feedService
     * @param FeedRepository    $feedRepository
     */
    public function __construct(
        CompanyRepository $companyRepository,
        FeedCrudService $feedService,
        FeedRepository $feedRepository
    ) {
        $this->companyRepository = $companyRepository;
        $this->feedService = $feedService;
        $this->feedRepository = $feedRepository;
    }

    /**
     * @param string|null $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return View
     */
    public function formAction(string $companyGuid, ?string $feedGuid = null): View
    {
        $company = $this->guardCompanyGuid($companyGuid);

        $feedTo = $feedGuid
            ? $this->guardFeedGuid($feedGuid)
            : null;

        return $this->makeView('edit-form')
            ->with('feed', $feedTo)
            ->with('company', $company);
    }

    /**
     * @param FeedFilterRequest $request
     *
     * @throws UnregisteredMappingException
     *
     * @return View
     */
    public function listAction(string $companyGuid, FeedFilterRequest $request): View
    {
        $company = $this->guardCompanyGuid($companyGuid);

        $dataRequest = $this->feedRepository
            ->fetchAll()
            ->byCompanyGuid($companyGuid);

        if (null !== $title = $request->getTitle()) {
            $dataRequest->byTitleLike($request->getTitle());
        }

        $paginator = $dataRequest->withPage(
            $request->getPage(),
            $request::VAR_DEFAULT_LIMIT
        );

        return $this->makeView('list')
            ->with('company', $company)
            ->with('feed', $paginator);
    }

    /**
     * @param FeedCreateRequest $request
     *
     * @throws UnregisteredMappingException
     * @throws \Illuminate\Auth\AuthenticationException
     * @return RedirectResponse
     */
    public function createAction(FeedCreateRequest $request): RedirectResponse
    {
        $companyTo = $this->guardCompanyGuid($request->getCompanyGuid());

        try {
            $feedTo = $this->feedService->create(
                $request->getAttributes(),
                $companyTo,
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при создании новости. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.feed.list',
            [
                'company_guid' => $feedTo->getCompany()->getGuid(),
            ]
        );
    }

    /**
     * @param string          $feedGuid
     * @param FeedEditRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function updateAction(string $feedGuid, FeedEditRequest $request): RedirectResponse
    {
        $feedTo = $this->guardFeedGuid($feedGuid);

        try {
            $feedTo = $this->feedService->update(
                $feedTo,
                $request->getAttributes()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при обновлении новости. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.feed.list',
            [
                'company_guid' => $feedTo->getCompany()->getGuid(),
            ]
        );
    }

    /**
     * @param string $feedGuid
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function deleteAction(string $feedGuid): RedirectResponse
    {
        $feedTo = $this->guardFeedGuid($feedGuid);

        try {
            $this->feedService->delete($feedTo);
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при удалении новости. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.feed.list',
            [
                'company_guid' => $feedTo->getCompany(),
            ]
        );
    }

    /**
     * @param string $feedGuid
     *
     * @throws UnregisteredMappingException
     *
     * @return FeedTo
     */
    protected function guardFeedGuid(string $feedGuid): FeedTo
    {
        $feedTo = $this->feedRepository->find($feedGuid);
        if (empty($feedTo)) {
            throw new NotFoundHttpException('Новость не найдена');
        }

        return $feedTo;
    }
}
