<?php

namespace Vrcci\Feed\Dto;

use App\Dto\StoresOriginalModel;
use Vrcci\Company\Dto\StoresCompanyToHelper;
use Vrcci\Feed\Eloquent\Entity\Feed;

/**
 * @method Feed getOriginal()
 */
class FeedTo
{
    use StoresOriginalModel, StoresCompanyToHelper;

    /**
     * @var string
     */
    private $guid = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     *
     * @return FeedTo
     */
    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return FeedTo
     */
    public function setTitle(string $title = ''): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return FeedTo
     */
    public function setDescription(string $description = ''): self
    {
        $this->description = $description;

        return $this;
    }
}
