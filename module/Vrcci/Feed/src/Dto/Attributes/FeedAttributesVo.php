<?php

namespace Vrcci\Feed\Dto\Attributes;

use App\Dto\Attributes\AbstractAttributesObject;

class FeedAttributesVo extends AbstractAttributesObject
{
    /**
     * Returns available attribute names
     *
     * @return string[]
     */
    public static function attributes(): array
    {
        return [
            'title',
            'description',
        ];
    }
}
