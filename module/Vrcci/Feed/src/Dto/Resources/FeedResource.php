<?php

namespace Vrcci\Feed\Dto\Resources;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\Extractor\DefaultResourceExtractor;
use App\Http\Resource\Extractor\ExtractorInterface;
use Vrcci\Feed\Dto\FeedTo;

/**
 * @OA\Schema(
 *     schema="Feed",
 *     @OA\Property(property="guid", type="string", description="Feed GUID"),
 *     @OA\Property(property="title", type="string", description="Feed title"),
 *     @OA\Property(property="description", type="string", description="Feed description"),
 *     ),
 * )
 */
class FeedResource extends AbstractExtractableResource
{
    /**
     * @var string[]
     */
    protected $extractable = [
        'guid',
        'title',
        'description',
    ];

    /**
     * @param ExtractorInterface $extractor
     * @param FeedTo             $resource
     *
     * @return mixed[]
     */
    protected function extract(ExtractorInterface $extractor, $resource): array
    {
        if ($extractor instanceof DefaultResourceExtractor) {
            $extractor->properties($this->extractable);
        }

        $extractor->extract($resource);

        return $extractor->toArray();
    }
}
