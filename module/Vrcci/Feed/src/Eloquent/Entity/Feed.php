<?php

namespace Vrcci\Feed\Eloquent\Entity;

use App\Entity\GuidModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Vrcci\Company\Eloquent\Entity\Company;
use Vrcci\User\Entity\User;

/**
 * @property string title
 */
class Feed extends GuidModel
{
    /**
     * @var string
     */
    protected $table = 'company_feeds';

    /**
     * @var string[]
     */
    protected $fillable = [
        'title',
        'description'
    ];

    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_guid', 'guid');
    }

    /**
     * @return BelongsTo
     */
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by_id', 'id');
    }
}
