<?php

namespace Vrcci\Feed\Eloquent\Mapper;

use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\AutoMapper\Operation\MapToRelation;
use App\Mapper\MappingFactoryInterface;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Feed\Dto\FeedTo;
use Vrcci\Feed\Eloquent\Entity\Feed;

class FeedMapperFactory implements MappingFactoryInterface
{
    /**
     * @param AutoMapperConfig $config
     *
     * @return void
     */
    public function registerMapping(AutoMapperConfig $config): void
    {
        $config->registerMapping(Feed::class, FeedTo::class)
            ->forMember('originalModel', new MapOriginal())
            ->forMember('company', new MapToRelation(CompanyTo::class));
    }
}
