<?php

namespace Vrcci\Feed\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Feed\Dto\FeedTo;
use Vrcci\Feed\Eloquent\DataRequest\FeedDataRequest;
use Vrcci\Feed\Eloquent\Entity\Feed;

class FeedRepository
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return FeedDataRequest
     */
    public function fetchAll(): FeedDataRequest
    {
        $dataRequest = FeedDataRequest::create(Feed::with(['company']));
        $dataRequest->withTransformer(
            function (Feed $company) {
                return $this->mapper->mapToObject($company, new FeedTo());
            }
        );

        return $dataRequest;
    }

    /**
     * @param string $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return FeedTo|null
     */
    public function find(string $guid): ?FeedTo
    {
        return $this->fetchAll()->byGuid($guid)->first();
    }
}
