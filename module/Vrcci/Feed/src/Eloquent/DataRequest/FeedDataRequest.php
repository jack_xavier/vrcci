<?php

namespace Vrcci\Feed\Eloquent\DataRequest;

use App\Eloquent\AbstractDataRequest;

class FeedDataRequest extends AbstractDataRequest
{
    /**
     * @param string $feedGuid
     *
     * @return FeedDataRequest
     */
    public function byGuid(string $feedGuid): self
    {
        $this->qb->where('guid', $feedGuid);

        return $this;
    }

    /**
     * @param string $companyGuid
     *
     * @return $this
     */
    public function byCompanyGuid(string $companyGuid): self
    {
        $this->qb->where('company_guid', $companyGuid);

        return $this;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function byTitleLike(string $title): self
    {
        $this->qb->where('title', $title);

        return $this;
    }
}
