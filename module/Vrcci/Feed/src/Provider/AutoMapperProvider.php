<?php

namespace Vrcci\Feed\Providers;

use App\Providers\AutoMapperProvider as BaseAutoMapperProvider;
use Illuminate\Support\ServiceProvider;
use Vrcci\Feed\Eloquent\Mapper\FeedMapperFactory;

class AutoMapperProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app->tag(
            [FeedMapperFactory::class],
            [BaseAutoMapperProvider::FACTORY_TAG]
        );
    }
}
