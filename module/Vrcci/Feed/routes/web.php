<?php

use App\Entity\GuidGenerator;
use Vrcci\Feed\Http\Controller\CompanyFeedController;

Route::middleware('auth:web')
    ->prefix('/company-feed')
    ->name('vrcci.feed.')
    ->group(
        function (): void {
            Route::get('/{company_guid}/form/{feed_guid?}', CompanyFeedController::class . '@formAction')
                ->name('form')
                ->where('company_guid', GuidGenerator::GUID_PATTERN)
                ->where('feed_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{company_guid}/list', CompanyFeedController::class . '@listAction')
                ->name('list')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{feed_guid}/update', CompanyFeedController::class . '@updateAction')
                ->name('update')
                ->where('feed_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/create', CompanyFeedController::class . '@createAction')
                ->name('create');
            Route::get('/{feed_guid}/delete', CompanyFeedController::class . '@deleteAction')
                ->name('delete')
                ->where('feed_guid', GuidGenerator::GUID_PATTERN);
        }
    );

