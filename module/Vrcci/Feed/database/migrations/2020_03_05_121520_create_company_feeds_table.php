<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyFeedsTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create(
            'company_feeds',
            function (Blueprint $table): void {
                $table->string('guid');
                $table->string('title');
                $table->text('description')->nullable();
                $table->string('company_guid');
                $table->unsignedInteger('created_by_id');
                $table->timestamps();

                $table->primary('guid');

                $table->foreign('company_guid')
                    ->references('guid')
                    ->on('companies')
                    ->onDelete('cascade');

                $table->foreign('created_by_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('company_feeds');
    }
}
