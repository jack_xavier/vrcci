<?php

namespace Vrcci\PasswordReset\Mapper;

use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\AutoMapper\Operation\MapToRelation;
use App\Mapper\MappingFactoryInterface;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\PasswordReset\Dto\PasswordResetTo;
use Vrcci\PasswordReset\Entity\PasswordReset;
use Vrcci\User\Dto\UserTo;

class PasswordResetMapperFactory implements MappingFactoryInterface
{
    /**
     * @param AutoMapperConfig $config
     */
    public function registerMapping(AutoMapperConfig $config): void
    {
        $config->registerMapping(PasswordReset::class, PasswordResetTo::class)
               ->forMember('originalModel', new MapOriginal())
               ->forMember('user', new MapToRelation(UserTo::class));
    }
}
