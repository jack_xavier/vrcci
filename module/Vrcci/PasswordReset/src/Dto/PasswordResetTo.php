<?php

namespace Vrcci\PasswordReset\Dto;

use App\Dto\StoresOriginalModel;
use DateTime;
use Vrcci\User\Dto\UserTo;

class PasswordResetTo
{
    use StoresOriginalModel;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var UserTo
     */
    protected $user;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     *
     * @return PasswordResetTo
     */
    public function setToken(string $token): self
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return UserTo
     */
    public function getUser(): UserTo
    {
        return $this->user;
    }

    /**
     * @param UserTo $user
     *
     * @return PasswordResetTo
     */
    public function setUser(UserTo $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
