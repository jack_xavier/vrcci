<?php

namespace Vrcci\PasswordReset\Service;

use AutoMapperPlus\AutoMapperInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Vrcci\PasswordReset\Dto\PasswordResetTo;
use Vrcci\PasswordReset\Entity\PasswordReset;
use Vrcci\User\Dto\UserTo;

class PasswordResetService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param UserTo $user
     *
     * @return PasswordResetTo
     */
    public function updateOrCreate(UserTo $user): PasswordResetTo
    {
        $token = str_random(60);

        /** @var PasswordReset $passwordReset */
        $passwordReset = PasswordReset::updateOrCreate(
            ['user_id' => $user->getGuid()],
            ['token' => $token]
        );
        $passwordReset->load('user');

        return $this->mapper->mapToObject($passwordReset, new PasswordResetTo());
    }

    /**
     * @param PasswordResetTo $passwordReset
     * @param string          $password
     */
    public function resetPassword(PasswordResetTo $passwordReset, string $password): void
    {
        $user           = $passwordReset->getUser()->getOriginal();
        $user->password = Hash::make($password);
        $user->save();

        $passwordReset->getOriginal()->delete();
    }

    /**
     * @param PasswordResetTo $passwordReset
     *
     * @return bool
     */
    public function checkTokenExpired(PasswordResetTo $passwordReset): bool
    {
        return $passwordReset->getCreatedAt() < Carbon::now()->subHour();
    }
}
