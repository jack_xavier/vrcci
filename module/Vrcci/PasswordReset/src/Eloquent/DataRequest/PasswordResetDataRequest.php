<?php

namespace Vrcci\PasswordReset\Eloquent\DataRequest;

use App\Eloquent\AbstractDataRequest;
use DateInterval;
use DateTime;

class PasswordResetDataRequest extends AbstractDataRequest
{
    /**
     * @param string $token
     *
     * @return PasswordResetDataRequest
     */
    public function byToken(string $token): self
    {
        $this->qb->where('token', $token);

        return $this;
    }

    /**
     * @return PasswordResetDataRequest
     */
    public function notExpired(): self
    {
        $now = new DateTime();
        $this->qb->where('updated_at', '>=', $now->sub(new DateInterval('PT1H')));

        return $this;
    }
}
