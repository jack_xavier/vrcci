<?php

namespace Vrcci\PasswordReset\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Vrcci\PasswordReset\Dto\PasswordResetTo;
use Vrcci\PasswordReset\Eloquent\DataRequest\PasswordResetDataRequest;
use Vrcci\PasswordReset\Entity\PasswordReset;

class PasswordResetRepository
{
    /**
     * @var AutoMapperInterface
     */
    protected $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param int $id
     *
     * @return PasswordResetTo|null
     */
    public function find(int $id): ?PasswordResetTo
    {
        try {
            $passwordReset = PasswordReset::with([])->findOrFail($id);

            return $this->mapper->mapToObject($passwordReset, new PasswordResetTo());
        } catch (ModelNotFoundException $exception) {
            return null;
        }
    }

    /**
     * @return PasswordResetDataRequest
     */
    public function fetchAll(): PasswordResetDataRequest
    {
        $dataRequest = PasswordResetDataRequest::create(PasswordReset::with(['user']));
        $dataRequest->withTransformer(
            function (PasswordReset $passwordReset) {
                return $this->mapper->mapToObject($passwordReset, new PasswordResetTo());
            }
        );

        return $dataRequest;
    }
}
