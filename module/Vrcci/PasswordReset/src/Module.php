<?php

namespace Vrcci\PasswordReset;

use App\Module\AbstractModule;
use Vrcci\PasswordReset\Providers\AutoMapperProvider;

class Module extends AbstractModule
{
    const ALIAS = 'vrcci.password-reset';

    /**
     * @return string
     */
    public static function getAlias()
    {
        return static::ALIAS;
    }

    /**
     * @inheritdoc
     */
    public function register()
    {
        parent::register();

        $this->app->register(AutoMapperProvider::class);
    }

    /**
     * @inheritdoc
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }
}
