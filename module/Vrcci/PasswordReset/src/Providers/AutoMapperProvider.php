<?php

namespace Vrcci\PasswordReset\Providers;

use App\Providers\AutoMapperProvider as BaseAutoMapperProvider;
use Illuminate\Support\ServiceProvider;
use Vrcci\PasswordReset\Mapper\PasswordResetMapperFactory;

class AutoMapperProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->tag(
            [
                PasswordResetMapperFactory::class,
            ],
            BaseAutoMapperProvider::FACTORY_TAG
        );
    }
}
