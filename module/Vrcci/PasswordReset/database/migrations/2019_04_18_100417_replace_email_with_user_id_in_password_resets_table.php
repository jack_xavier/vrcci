<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReplaceEmailWithUserIdInPasswordResetsTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::table(
            'password_resets',
            function (Blueprint $table): void {
                $table->dropForeign(['email']);
                $table->dropColumn('email');
                $table->unsignedInteger('user_id');

                $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade');
            }
        );
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::table(
            'password_resets',
            function (Blueprint $table): void {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
                $table->string('email')->index();

                $table->foreign('email')
                      ->references('email')
                      ->on('users')
                      ->onDelete('cascade');
            }
        );
    }
}
