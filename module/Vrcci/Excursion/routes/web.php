<?php

use App\Entity\GuidGenerator;
use Vrcci\Excursion\Http\Controller\ExcursionController;

Route::middleware('auth:web')
    ->prefix('/excursions')
    ->name('vrcci.excursion.')
    ->group(
        function (): void {
            Route::get('/{company_guid}/list', ExcursionController::class . '@listAction')
                ->name('list')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{company_guid}/upload', ExcursionController::class . '@uploadAction')
                ->name('upload')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{excursion_guid}/reupload', ExcursionController::class . '@reuploadAction')
                ->name('reupload')
                ->where('excursion_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{excursion_guid}/delete', ExcursionController::class . '@deleteAction')
                ->name('delete')
                ->where('excursion_guid', GuidGenerator::GUID_PATTERN);
        }
    );

