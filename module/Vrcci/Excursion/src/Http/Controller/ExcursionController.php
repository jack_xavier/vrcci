<?php

namespace Vrcci\Excursion\Http\Controller;

use App\Exceptions\FileUploadingException;
use App\Helper\CurrentUserTrait;
use App\Helper\FileLimitTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Excursion\Http\Request\UploadExcursionRequest;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Excursion\Dto\ExcursionTo;
use Vrcci\Excursion\Eloquent\Repository\ExcursionRepository;
use Vrcci\Excursion\Service\ExcursionCrudService;

class ExcursionController extends Controller implements ModuleAliasAwareInstance
{
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;
    use GuardForCompanyGuidTrait;
    use FileLimitTrait;

    /**
     * @var ExcursionRepository
     */
    private $excursionRepository;

    /**
     * @var ExcursionCrudService
     */
    private $excursionService;

    /**
     * @param CompanyRepository    $companyRepository
     * @param ExcursionRepository  $excursionRepository
     * @param ExcursionCrudService $excursionService
     */
    public function __construct(
        CompanyRepository $companyRepository,
        ExcursionRepository $excursionRepository,
        ExcursionCrudService $excursionService
    ) {
        $this->companyRepository = $companyRepository;
        $this->excursionRepository = $excursionRepository;
        $this->excursionService = $excursionService;
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     * @return View
     */
    public function listAction(string $companyGuid): View
    {
        $company = $this->guardCompanyGuid($companyGuid);

        $excursions = $this->excursionRepository
            ->fetchAll()
            ->byCompanyGuid($companyGuid)
            ->get();

        return $this->makeView('list')
            ->with('company', $company)
            ->with('excursions', $excursions);
    }

    /**
     * @param string                 $companyGuid
     * @param UploadExcursionRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function uploadAction(string $companyGuid, UploadExcursionRequest $request): RedirectResponse
    {
        $company = $this->guardCompanyGuid($companyGuid);

        try {
            $this->doValidate(
                $company,
                $company->getOriginal()->excursions()->get(),
                $request,
                'excursion'
            );
        } catch (FileUploadingException $exception) {
            return Redirect::back()->withErrors(
                [
                    $exception->getMessage(),
                ]
            );
        }

        try {
            $this->excursionService->create(
                $request->getFile(),
                $company,
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при загрузке экскурсии. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.excursion.list',
            ['company_guid' => $company->getGuid(),]
        );
    }

    /**
     * @param string                 $excursionGuid
     * @param UploadExcursionRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function reuploadAction(string $excursionGuid, UploadExcursionRequest $request): RedirectResponse
    {
        $excursionTo = $this->guardExcursionGuid($excursionGuid);
        $company = $excursionTo->getCompany();

        try {
            $this->doValidate(
                $company,
                $company->getOriginal()->excursions()->get(),
                $request,
                'excursion',
                true
            );
        } catch (FileUploadingException $exception) {
            return Redirect::back()->withErrors(
                [
                    $exception->getMessage(),
                ]
            );
        }

        try {
            $excursionTo = $this->excursionService->update(
                $excursionTo,
                $request->getFile(),
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при перезагрузке файла экскурсии. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.excursion.list',
            [
                'company_guid' => $excursionTo->getCompany()->getGuid(),
            ]
        );
    }

    /**
     * @param string|null $excursionGuid
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function deleteAction(?string $excursionGuid): RedirectResponse
    {
        $excursionTo = $this->guardExcursionGuid($excursionGuid);

        try {
            $this->excursionService->delete($excursionTo);
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при удалении файла экскурсии. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.excursion.list',
            ['company_guid' => $excursionTo->getCompany()->getGuid()]
        );
    }

    /**
     * @param string $excursionGuid
     *
     * @throws UnregisteredMappingException
     * @return ExcursionTo
     */
    protected function guardExcursionGuid(string $excursionGuid): ExcursionTo
    {
        $excursionTo = $this->excursionRepository->find($excursionGuid);
        if (empty($excursionTo)) {
            throw new NotFoundHttpException('Файл экскурсии не найден');
        }

        return $excursionTo;
    }
}
