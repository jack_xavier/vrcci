<?php

namespace Vrcci\Excursion\Service;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Exception;
use Illuminate\Http\UploadedFile;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Excursion\Dto\ExcursionTo;
use Vrcci\Excursion\Eloquent\Entity\Excursion;
use Vrcci\User\Dto\UserTo;

class ExcursionCrudService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param UploadedFile $file
     * @param CompanyTo    $companyTo
     * @param UserTo       $userTo
     *
     * @throws UnregisteredMappingException
     * @return ExcursionTo
     */
    public function create(UploadedFile $file, CompanyTo $companyTo, UserTo $userTo): ExcursionTo
    {
        $excursion = new Excursion();
        $excursion->company()->associate($companyTo->getOriginal());

        $excursion->setFile($file);

        $excursion->uploadedBy()->associate($userTo->getOriginal());
        $excursion->save();

        return $this->mapper->mapToObject($excursion, new ExcursionTo());
    }

    /**
     * @param ExcursionTo  $excursionTo
     * @param UploadedFile $file
     * @param UserTo       $userTo
     *
     * @throws UnregisteredMappingException
     *
     * @return ExcursionTo
     */
    public function update(ExcursionTo $excursionTo, UploadedFile $file, UserTo $userTo): ExcursionTo
    {
        $excursion = $excursionTo->getOriginal();
        $excursion->setFile($file);

        $excursion->uploadedBy()->associate($userTo->getOriginal());
        $excursion->save();

        return $this->mapper->mapToObject($excursion, $excursionTo);
    }

    /**
     * @param ExcursionTo $excursionTo
     *
     * @throws Exception
     *
     * @return void
     */
    public function delete(ExcursionTo $excursionTo): void
    {
        $excursionTo->getOriginal()->delete();
    }
}
