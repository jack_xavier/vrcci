<?php

namespace Vrcci\Excursion\Providers;

use App\Providers\AutoMapperProvider as BaseAutoMapperProvider;
use Illuminate\Support\ServiceProvider;
use Vrcci\Excursion\Eloquent\Mapper\ExcursionMapperFactory;

class AutoMapperProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app->tag(
            [ExcursionMapperFactory::class],
            [BaseAutoMapperProvider::FACTORY_TAG]
        );
    }
}
