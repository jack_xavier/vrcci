<?php

namespace Vrcci\Excursion\Eloquent\DataRequest;

use App\Eloquent\AbstractDataRequest;

class ExcursionDataRequest extends AbstractDataRequest
{
    /**
     * @param string $companyGuid
     *
     * @return ExcursionDataRequest
     */
    public function byCompanyGuid(string $companyGuid): self
    {
        $this->qb->where('company_guid', $companyGuid);

        return $this;
    }

    /**
     * @param string $guid
     *
     * @return $this
     */
    public function byGuid(string $guid): self
    {
        $this->qb->where('guid', $guid);

        return $this;
    }
}
