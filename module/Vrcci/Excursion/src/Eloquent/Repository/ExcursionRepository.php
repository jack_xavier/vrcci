<?php

namespace Vrcci\Excursion\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Excursion\Eloquent\DataRequest\ExcursionDataRequest;
use Vrcci\Excursion\Dto\ExcursionTo;
use Vrcci\Excursion\Eloquent\Entity\Excursion;

class ExcursionRepository
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return ExcursionDataRequest
     */
    public function fetchAll(): ExcursionDataRequest
    {
        $dataRequest = ExcursionDataRequest::create(Excursion::with(['company']));
        $dataRequest->withTransformer(
            function (Excursion $excursion) {
                return $this->mapper->mapToObject($excursion, new ExcursionTo());
            }
        );

        return $dataRequest;
    }

    /**
     * @param int $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return ExcursionTo|null
     */
    public function find(string $guid): ?ExcursionTo
    {
        return $this->fetchAll()->byGuid($guid)->first();
    }
}
