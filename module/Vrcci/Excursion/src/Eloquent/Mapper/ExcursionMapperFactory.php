<?php

namespace Vrcci\Excursion\Eloquent\Mapper;

use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\AutoMapper\Operation\MapToRelation;
use App\Mapper\FileMapperFactory;
use App\Mapper\MappingFactoryInterface;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Excursion\Dto\ExcursionTo;
use Vrcci\Excursion\Eloquent\Entity\Excursion;
use Vrcci\Presentation\Dto\PresentationTo;

class ExcursionMapperFactory extends FileMapperFactory
{
    /**
     * @var string
     */
    protected $entityClassName = Excursion::class;

    /**
     * @var string
     */
    protected $dtoClassName = ExcursionTo::class;
}
