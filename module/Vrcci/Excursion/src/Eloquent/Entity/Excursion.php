<?php

namespace Vrcci\Excursion\Eloquent\Entity;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Vrcci\Company\Eloquent\Entity\Company;
use App\Entity\File;

class Excursion extends File
{
    /**
     * @var string
     */
    protected $table = 'excursions';

    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_guid', 'guid');
    }

    /**
     * @return string
     */
    protected function getDirectoryName(): string
    {
        return sprintf('%s/excursions', $this->company->title);
    }
}
