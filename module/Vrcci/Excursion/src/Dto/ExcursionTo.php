<?php

namespace Vrcci\Excursion\Dto;

use App\Dto\ContainsFileToTrait;
use App\Dto\StoresOriginalModel;
use Vrcci\Company\Dto\StoresCompanyToHelper;
use Vrcci\Excursion\Eloquent\Entity\Excursion;

/**
 * @method Excursion getOriginal()
 */
class ExcursionTo
{
    use StoresOriginalModel, ContainsFileToTrait, StoresCompanyToHelper;
}
