<?php

namespace Vrcci\Excursion\Dto\Resources;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\Extractor\DefaultResourceExtractor;
use App\Http\Resource\Extractor\ExtractorInterface;
use App\Http\Resource\StoresFileResourceTrait;
use Vrcci\Excursion\Dto\ExcursionTo;

/**
 * @OA\Schema(
 *     schema="Excursion",
 *     @OA\Property(property="guid", type="string", description="Excursion guid"),
 *     @OA\Property(property="url", type="string", description="File url"),
 *     @OA\Property(property="filename", type="string", description="File name"),
 *     ),
 * )
 */
class ExcursionResource extends AbstractExtractableResource
{
    use StoresFileResourceTrait;
}
