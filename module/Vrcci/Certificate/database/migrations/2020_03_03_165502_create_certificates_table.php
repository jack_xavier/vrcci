<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create(
            'certificates',
            function (Blueprint $table): void {
                $table->string('guid');
                $table->string('original_filename');
                $table->string('filename');
                $table->string('company_guid');
                $table->unsignedInteger('uploaded_by_id');
                $table->timestamps();

                $table->primary('guid');

                $table->foreign('company_guid')
                      ->references('guid')
                      ->on('companies')
                      ->onDelete('cascade');

                $table->foreign('uploaded_by_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade');
            }
        );
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('certificates');
    }
}
