<?php

namespace Vrcci\Certificate\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Certificate\Eloquent\DataRequest\CertificateDataRequest;
use Vrcci\Certificate\Dto\CertificateTo;
use Vrcci\Certificate\Eloquent\Entity\Certificate;

class CertificateRepository
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return CertificateDataRequest
     */
    public function fetchAll(): CertificateDataRequest
    {
        $dataRequest = CertificateDataRequest::create(Certificate::with(['company']));
        $dataRequest->withTransformer(
            function (Certificate $certificate) {
                return $this->mapper->mapToObject($certificate, new CertificateTo());
            }
        );

        return $dataRequest;
    }

    /**
     * @param string $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return CertificateTo|null
     */
    public function find(string $guid): ?CertificateTo
    {
        return $this->fetchAll()->byGuid($guid)->first();
    }
}
