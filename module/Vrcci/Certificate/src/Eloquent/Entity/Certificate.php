<?php

namespace Vrcci\Certificate\Eloquent\Entity;

use App\Entity\File;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Vrcci\Company\Eloquent\Entity\Company;

class Certificate extends File
{
    /**
     * @var string
     */
    protected $table = 'certificates';

    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_guid', 'guid');
    }

    /**
     * @return string
     */
    protected function getDirectoryName(): string
    {
        return sprintf('%s/certificates', $this->company->title);
    }
}
