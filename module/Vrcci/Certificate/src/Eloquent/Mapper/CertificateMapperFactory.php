<?php

namespace Vrcci\Certificate\Eloquent\Mapper;

use App\Mapper\AutoMapper\Operation\MapOriginal;
use App\Mapper\AutoMapper\Operation\MapToRelation;
use App\Mapper\FileMapperFactory;
use App\Mapper\MappingFactoryInterface;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Vrcci\Certificate\Dto\CertificateTo;
use Vrcci\Certificate\Eloquent\Entity\Certificate;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Excursion\Dto\ExcursionTo;

class CertificateMapperFactory extends FileMapperFactory
{
    /**
     * @var string
     */
    protected $entityClassName = Certificate::class;

    /**
     * @var string
     */
    protected $dtoClassName = CertificateTo::class;
}
