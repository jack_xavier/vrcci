<?php

namespace Vrcci\Certificate\Service;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Exception;
use Illuminate\Http\UploadedFile;
use Vrcci\Certificate\Dto\CertificateTo;
use Vrcci\Certificate\Eloquent\Entity\Certificate;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\User\Dto\UserTo;

class CertificateCrudService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param UploadedFile $file
     * @param CompanyTo    $companyTo
     * @param UserTo       $userTo
     *
     * @throws UnregisteredMappingException
     * @return CertificateTo
     */
    public function create(UploadedFile $file, CompanyTo $companyTo, UserTo $userTo): CertificateTo
    {
        $certificate = new Certificate();
        $certificate->company()->associate($companyTo->getOriginal());

        $certificate->setFile($file);

        $certificate->uploadedBy()->associate($userTo->getOriginal());
        $certificate->save();

        return $this->mapper->mapToObject($certificate, new CertificateTo());
    }

    /**
     * @param CertificateTo $certificateTo
     * @param UploadedFile  $file
     * @param UserTo        $userTo
     *
     * @throws UnregisteredMappingException
     *
     * @return CertificateTo
     */
    public function update(CertificateTo $certificateTo, UploadedFile $file, UserTo $userTo): CertificateTo
    {
        $certificate = $certificateTo->getOriginal();
        $certificate->setFile($file);

        $certificate->uploadedBy()->associate($userTo->getOriginal());
        $certificate->save();

        return $this->mapper->mapToObject($certificate, $certificateTo);
    }

    /**
     * @param CertificateTo $certificateTo
     *
     * @throws Exception
     *
     * @return void
     */
    public function delete(CertificateTo $certificateTo): void
    {
        $certificateTo->getOriginal()->delete();
    }
}
