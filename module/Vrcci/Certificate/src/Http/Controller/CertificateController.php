<?php

namespace Vrcci\Certificate\Http\Controller;

use App\Exceptions\FileUploadingException;
use App\Helper\CurrentUserTrait;
use App\Helper\FileLimitTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Vrcci\Api\Certificate\Http\Request\UploadCertificateRequest;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Certificate\Dto\CertificateTo;
use Vrcci\Certificate\Eloquent\Repository\CertificateRepository;
use Vrcci\Certificate\Service\CertificateCrudService;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;

class CertificateController extends Controller implements ModuleAliasAwareInstance
{
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;
    use GuardForCompanyGuidTrait;
    use FileLimitTrait;

    /**
     * @var CertificateRepository
     */
    private $certificateRepository;

    /**
     * @var CertificateCrudService
     */
    private $certificateService;

    /**
     * @param CompanyRepository      $companyRepository
     * @param CertificateRepository  $certificateRepository
     * @param CertificateCrudService $certificateService
     */
    public function __construct(
        CompanyRepository $companyRepository,
        CertificateRepository $certificateRepository,
        CertificateCrudService $certificateService
    ) {
        $this->companyRepository = $companyRepository;
        $this->certificateRepository = $certificateRepository;
        $this->certificateService = $certificateService;
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     * @return View
     */
    public function listAction(string $companyGuid): View
    {
        $company = $this->guardCompanyGuid($companyGuid);

        $certificates = $this->certificateRepository
            ->fetchAll()
            ->byCompanyGuid($companyGuid)
            ->get();

        return $this->makeView('list')
            ->with('company', $company)
            ->with('certificates', $certificates);
    }

    /**
     * @param string                   $companyGuid
     * @param UploadCertificateRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function uploadAction(string $companyGuid, UploadCertificateRequest $request): RedirectResponse
    {
        $company = $this->guardCompanyGuid($companyGuid);

        try {
            $this->doValidate(
                $company,
                $company->getOriginal()->certificates()->get(),
                $request,
                'certificates'
            );
        } catch (FileUploadingException $exception) {
            return Redirect::back()->withErrors(
                [
                    $exception->getMessage(),
                ]
            );
        }

        try {
            $this->certificateService->create(
                $request->getFile(),
                $company,
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при загрузке файла. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.certificate.list',
            ['company_guid' => $company->getGuid(),]
        );
    }

    /**
     * @param string                   $certificateGuid
     * @param UploadCertificateRequest $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function reuploadAction(string $certificateGuid, UploadCertificateRequest $request): RedirectResponse
    {
        $certificateTo = $this->guardCertificateGuid($certificateGuid);
        $company = $certificateTo->getCompany();

        try {
            $this->doValidate(
                $company,
                $company->getOriginal()->certificates()->get(),
                $request,
                'certificates',
                true
            );
        } catch (FileUploadingException $exception) {
            return Redirect::back()->withErrors(
                [
                    $exception->getMessage(),
                ]
            );
        }

        try {
            $certificateTo = $this->certificateService->update(
                $certificateTo,
                $request->getFile(),
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при перезагрузке файла. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.certificate.list',
            [
                'company_guid' => $certificateTo->getCompany()->getGuid(),
            ]
        );
    }

    /**
     * @param string|null $certificateGuid
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function deleteAction(?string $certificateGuid): RedirectResponse
    {
        $certificateTo = $this->guardCertificateGuid($certificateGuid);

        try {
            $this->certificateService->delete($certificateTo);
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            return Redirect::back()->withErrors(
                [
                    'Возникла ошибка при удалении файла. ',
                ]
            );
        }

        return redirect()->route(
            'vrcci.certificate.list',
            ['company_guid' => $certificateTo->getCompany()->getGuid(),]
        );
    }

    /**
     * @param string $certificateGuid
     *
     * @throws UnregisteredMappingException
     * @return CertificateTo
     */
    protected function guardCertificateGuid(string $certificateGuid): CertificateTo
    {
        $certificateTo = $this->certificateRepository->find($certificateGuid);
        if (empty($certificateTo)) {
            throw new NotFoundHttpException('Файл не найден');
        }

        return $certificateTo;
    }
}
