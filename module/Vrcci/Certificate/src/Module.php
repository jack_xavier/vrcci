<?php

namespace Vrcci\Certificate;

use App\Module\AbstractModule;
use App\Module\Feature\RoutesProviderInterface;
use App\Module\Feature\ViewsProviderInterface;
use Vrcci\Certificate\Providers\AutoMapperProvider;

class Module extends AbstractModule implements RoutesProviderInterface, ViewsProviderInterface
{
    public const ALIAS = 'vrcci.certificate';

    /**
     * @return array
     */
    public function getRoutesPath(): array
    {
        return ['web' => __DIR__ . '/../routes/web.php'];
    }

    /**
     * @return string
     */
    public function getViewsPath(): string
    {
        return __DIR__ . '/../resources/views';
    }

    /**
     * @return string
     */
    public static function getAlias(): string
    {
        return static::ALIAS;
    }

    /**
     * @return void
     */
    public function register(): void
    {
        parent::register();

        $this->app->register(AutoMapperProvider::class);
    }

    /**
     * @return void
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }
}
