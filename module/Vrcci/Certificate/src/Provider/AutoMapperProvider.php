<?php

namespace Vrcci\Certificate\Providers;

use App\Providers\AutoMapperProvider as BaseAutoMapperProvider;
use Illuminate\Support\ServiceProvider;
use Vrcci\Certificate\Eloquent\Mapper\CertificateMapperFactory;

class AutoMapperProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app->tag(
            [CertificateMapperFactory::class],
            [BaseAutoMapperProvider::FACTORY_TAG]
        );
    }
}
