<?php

namespace Vrcci\Certificate\Dto;

use App\Dto\ContainsFileToTrait;
use App\Dto\StoresOriginalModel;
use Vrcci\Certificate\Eloquent\Entity\Certificate;
use Vrcci\Company\Dto\StoresCompanyToHelper;

/**
 * @method Certificate getOriginal()
 */
class CertificateTo
{
    use StoresOriginalModel, ContainsFileToTrait, StoresCompanyToHelper;
}
