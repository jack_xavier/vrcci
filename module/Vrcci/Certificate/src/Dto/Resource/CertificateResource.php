<?php

namespace Vrcci\Certificate\Dto\Resources;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\StoresFileResourceTrait;

/**
 * @OA\Schema(
 *     schema="Certificate",
 *     @OA\Property(property="guid", type="string", description="Certificate"),
 *     @OA\Property(property="url", type="string", description="File url"),
 *     @OA\Property(property="filename", type="string", description="File name"),
 *     ),
 * )
 */
class CertificateResource extends AbstractExtractableResource
{
    use StoresFileResourceTrait;
}
