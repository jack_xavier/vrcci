<?php

use App\Entity\GuidGenerator;
use Vrcci\Certificate\Http\Controller\CertificateController;

Route::middleware('auth:web')
    ->prefix('/certificates')
    ->name('vrcci.certificate.')
    ->group(
        function (): void {
            Route::get('/{company_guid}/list', CertificateController::class . '@listAction')
                ->name('list')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{company_guid}/upload', CertificateController::class . '@uploadAction')
                ->name('upload')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{certificate_guid}/reupload', CertificateController::class . '@reuploadAction')
                ->name('reupload')
                ->where('certificate_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{certificate_guid}/delete', CertificateController::class . '@deleteAction')
                ->name('delete')
                ->where('certificate_guid', GuidGenerator::GUID_PATTERN);
        }
    );

