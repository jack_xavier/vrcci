<?php

namespace Vrcci\Sphere360\Dto\Resources;

use App\Http\Resource\AbstractExtractableResource;
use App\Http\Resource\Extractor\DefaultResourceExtractor;
use App\Http\Resource\Extractor\ExtractorInterface;
use App\Http\Resource\StoresFileResourceTrait;
use Vrcci\Sphere360\Dto\Sphere360To;

/**
 * @OA\Schema(
 *     schema="Sphere360",
 *     @OA\Property(property="guid", type="string", description="Sphere360 Id"),
 *     @OA\Property(property="url", type="string", description="File url"),
 *     @OA\Property(property="filename", type="string", description="File name"),
 *     ),
 * )
 */
class Sphere360Resource extends AbstractExtractableResource
{
    use StoresFileResourceTrait;
}
