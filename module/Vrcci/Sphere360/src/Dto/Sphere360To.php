<?php

namespace Vrcci\Sphere360\Dto;

use App\Dto\ContainsFileToTrait;
use App\Dto\StoresOriginalModel;
use Vrcci\Company\Dto\StoresCompanyToHelper;
use Vrcci\Sphere360\Eloquent\Entity\Sphere360;

/**
 * @method Sphere360 getOriginal()
 */
class Sphere360To
{
    use StoresOriginalModel, ContainsFileToTrait, StoresCompanyToHelper;
}
