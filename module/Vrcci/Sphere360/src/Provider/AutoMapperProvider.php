<?php

namespace Vrcci\Sphere360\Providers;

use App\Providers\AutoMapperProvider as BaseAutoMapperProvider;
use Illuminate\Support\ServiceProvider;
use Vrcci\Sphere360\Eloquent\Mapper\Sphere360MapperFactory;

class AutoMapperProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app->tag(
            [Sphere360MapperFactory::class],
            [BaseAutoMapperProvider::FACTORY_TAG]
        );
    }
}
