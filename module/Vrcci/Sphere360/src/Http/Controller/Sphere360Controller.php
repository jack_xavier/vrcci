<?php

namespace Vrcci\Sphere360\Http\Controller;

use App\Exceptions\FileUploadingException;
use App\Helper\CurrentUserTrait;
use App\Helper\FileLimitTrait;
use App\Http\Controllers\Controller;
use App\Module\Behaviour\AwareOfModuleAlias;
use App\Module\Behaviour\MakesView;
use App\Module\Behaviour\ModuleAliasAwareInstance;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Vrcci\Api\Company\Http\Controller\GuardForCompanyGuidTrait;
use Vrcci\Api\Sphere360\Http\Request\UploadSphere360Request;
use Vrcci\Company\Eloquent\Repository\CompanyRepository;
use Vrcci\Sphere360\Dto\Sphere360To;
use Vrcci\Sphere360\Eloquent\Repository\Sphere360Repository;
use Vrcci\Sphere360\Service\Sphere360CrudService;

class Sphere360Controller extends Controller implements ModuleAliasAwareInstance
{
    use MakesView;
    use AwareOfModuleAlias;
    use CurrentUserTrait;
    use GuardForCompanyGuidTrait;
    use FileLimitTrait;

    /**
     * @var Sphere360Repository
     */
    private $sphere360Repository;

    /**
     * @var Sphere360CrudService
     */
    private $sphere360Service;

    /**
     * @param CompanyRepository    $companyRepository
     * @param Sphere360Repository  $sphere360Repository
     * @param Sphere360CrudService $sphere360Service
     */
    public function __construct(
        CompanyRepository $companyRepository,
        Sphere360Repository $sphere360Repository,
        Sphere360CrudService $sphere360Service
    ) {
        $this->companyRepository = $companyRepository;
        $this->sphere360Repository = $sphere360Repository;
        $this->sphere360Service = $sphere360Service;
    }

    /**
     * @param string $companyGuid
     *
     * @throws UnregisteredMappingException
     * @return View
     */
    public function listAction(string $companyGuid): View
    {
        $company = $this->guardCompanyGuid($companyGuid);

        $spheres360 = $this->sphere360Repository
            ->fetchAll()
            ->byCompanyGuid($companyGuid)
            ->get();

        return $this->makeView('list')
            ->with('company', $company)
            ->with('spheres360', $spheres360);
    }

    /**
     * @param string                 $companyGuid
     * @param UploadSphere360Request $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function uploadAction(string $companyGuid, UploadSphere360Request $request): RedirectResponse
    {
        $company = $this->guardCompanyGuid($companyGuid);

        try {
            $this->doValidate(
                $company,
                $company->getOriginal()->spheres()->get(),
                $request,
                'spheres360'
            );
        } catch (FileUploadingException $exception) {
            return Redirect::back()->withErrors(
                [
                    $exception->getMessage(),
                ]
            );
        }

        try {
            $this->sphere360Service->create(
                $request->getFile(),
                $company,
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            throw new HttpException(500, 'Возникла ошибка при загрузке файла сферы360');
        }

        return redirect()->route(
            'vrcci.sphere360.list',
            ['company_guid' => $company->getGuid()]
        );
    }

    /**
     * @param string                 $sphere360Guid
     * @param UploadSphere360Request $request
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function reuploadAction(string $sphere360Guid, UploadSphere360Request $request): RedirectResponse
    {
        $sphere360To = $this->guardSphere360Guid($sphere360Guid);
        $company = $sphere360To->getCompany();

        try {
            $this->doValidate(
                $company,
                $company->getOriginal()->spheres()->get(),
                $request,
                'spheres360',
                true
            );
        } catch (FileUploadingException $exception) {
            return Redirect::back()->withErrors(
                [
                    $exception->getMessage(),
                ]
            );
        }

        try {
            $sphere360To = $this->sphere360Service->update(
                $sphere360To,
                $request->getFile(),
                $this->getCurrentUser()
            );
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            throw new HttpException(500, 'Возникла ошибка при перезагрузке файла сферы360');
        }

        return redirect()->route(
            'vrcci.sphere360.list',
            [
                'company_guid' => $sphere360To->getCompany()->getGuid(),
            ]
        );
    }

    /**
     * @param string|null $sphere360Guid
     *
     * @throws UnregisteredMappingException
     * @return RedirectResponse
     */
    public function deleteAction(?string $sphere360Guid): RedirectResponse
    {
        $sphere360To = $this->guardSphere360Guid($sphere360Guid);

        try {
            $this->sphere360Service->delete($sphere360To);
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            throw new HttpException(500, 'Возникла ошибка при удалении файла сферы360');
        }

        return redirect()->route(
            'vrcci.sphere360.list',
            ['company_guid' => $sphere360To->getCompany()->getGuid()]
        );
    }

    /**
     * @param string $sphere360Guid
     *
     * @throws UnregisteredMappingException
     * @return Sphere360To
     */
    protected function guardSphere360Guid(string $sphere360Guid): Sphere360To
    {
        $sphere360To = $this->sphere360Repository->find($sphere360Guid);
        if (empty($sphere360To)) {
            throw new NotFoundHttpException('Файл сферы360 не найден');
        }

        return $sphere360To;
    }
}
