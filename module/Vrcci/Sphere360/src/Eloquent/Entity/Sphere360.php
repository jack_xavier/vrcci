<?php

namespace Vrcci\Sphere360\Eloquent\Entity;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Vrcci\Company\Eloquent\Entity\Company;
use App\Entity\File;

class Sphere360 extends File
{
    /**
     * @var string
     */
    protected $table = 'spheres360';

    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_guid', 'guid');
    }

    /**
     * @return string
     */
    protected function getDirectoryName(): string
    {
        return sprintf('%s/spheres360', $this->company->title);
    }
}
