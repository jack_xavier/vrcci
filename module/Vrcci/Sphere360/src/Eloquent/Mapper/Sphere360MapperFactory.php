<?php

namespace Vrcci\Sphere360\Eloquent\Mapper;

use App\Mapper\FileMapperFactory;
use Vrcci\Sphere360\Dto\Sphere360To;
use Vrcci\Sphere360\Eloquent\Entity\Sphere360;

class Sphere360MapperFactory extends FileMapperFactory
{
    /**
     * @var string
     */
    protected $entityClassName = Sphere360::class;

    /**
     * @var string
     */
    protected $dtoClassName = Sphere360To::class;
}
