<?php

namespace Vrcci\Sphere360\Eloquent\DataRequest;

use App\Eloquent\AbstractDataRequest;

class Sphere360DataRequest extends AbstractDataRequest
{
    /**
     * @param string $companyId
     *
     * @return Sphere360DataRequest
     */
    public function byCompanyGuid(string $companyId): self
    {
        $this->qb->where('company_guid', $companyId);

        return $this;
    }

    /**
     * @param string $guid
     *
     * @return $this
     */
    public function byGuid(string $guid): self
    {
        $this->qb->where('guid', $guid);

        return $this;
    }
}
