<?php

namespace Vrcci\Sphere360\Eloquent\Repository;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Vrcci\Sphere360\Eloquent\DataRequest\Sphere360DataRequest;
use Vrcci\Sphere360\Dto\Sphere360To;
use Vrcci\Sphere360\Eloquent\Entity\Sphere360;

class Sphere360Repository
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return Sphere360DataRequest
     */
    public function fetchAll(): Sphere360DataRequest
    {
        $dataRequest = Sphere360DataRequest::create(Sphere360::with(['company']));
        $dataRequest->withTransformer(
            function (Sphere360 $sphere360) {
                return $this->mapper->mapToObject($sphere360, new Sphere360To());
            }
        );

        return $dataRequest;
    }

    /**
     * @param string $guid
     *
     * @throws UnregisteredMappingException
     *
     * @return Sphere360To|null
     */
    public function find(string $guid): ?Sphere360To
    {
        return $this->fetchAll()->byGuid($guid)->first();
    }
}
