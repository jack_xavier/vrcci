<?php

namespace Vrcci\Sphere360\Service;

use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Exception;
use Illuminate\Http\UploadedFile;
use Vrcci\Sphere360\Dto\Sphere360To;
use Vrcci\Company\Dto\CompanyTo;
use Vrcci\Sphere360\Eloquent\Entity\Sphere360;
use Vrcci\User\Dto\UserTo;

class Sphere360CrudService
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param UploadedFile $file
     * @param CompanyTo    $companyTo
     * @param UserTo       $userTo
     *
     * @throws UnregisteredMappingException
     * @return Sphere360To
     */
    public function create(UploadedFile $file, CompanyTo $companyTo, UserTo $userTo): Sphere360To
    {
        $sphere360 = new Sphere360();
        $sphere360->company()->associate($companyTo->getOriginal());

        $sphere360->setFile($file);
        $sphere360->uploadedBy()->associate($userTo->getOriginal());
        $sphere360->save();

        return $this->mapper->mapToObject($sphere360, new Sphere360To());
    }

    /**
     * @param Sphere360To  $sphere360To
     * @param UploadedFile $file
     * @param UserTo       $userTo
     *
     * @throws UnregisteredMappingException
     *
     * @return Sphere360To
     */
    public function update(Sphere360To $sphere360To, UploadedFile $file, UserTo $userTo): Sphere360To
    {
        $sphere360 = $sphere360To->getOriginal();
        $sphere360->setFile($file);

        $sphere360->uploadedBy()->associate($userTo->getOriginal());
        $sphere360->save();

        return $this->mapper->mapToObject($sphere360, $sphere360To);
    }

    /**
     * @param Sphere360To $sphere360To
     *
     * @throws Exception
     *
     * @return void
     */
    public function delete(Sphere360To $sphere360To): void
    {
        $sphere360To->getOriginal()->delete();
    }
}
