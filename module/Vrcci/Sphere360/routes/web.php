<?php

use App\Entity\GuidGenerator;
use Vrcci\Sphere360\Http\Controller\Sphere360Controller;

Route::middleware('auth:web')
    ->prefix('/spheres360')
    ->name('vrcci.sphere360.')
    ->group(
        function (): void {
            Route::get('/{company_guid}/list', Sphere360Controller::class . '@listAction')
                ->name('list')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{company_guid}/upload', Sphere360Controller::class . '@uploadAction')
                ->name('upload')
                ->where('company_guid', GuidGenerator::GUID_PATTERN);
            Route::post('/{sphere360_guid}/reupload', Sphere360Controller::class . '@reuploadAction')
                ->name('reupload')
                ->where('sphere360_guid', GuidGenerator::GUID_PATTERN);
            Route::get('/{sphere360_guid}/delete', Sphere360Controller::class . '@deleteAction')
                ->name('delete')
                ->where('sphere360_guid', GuidGenerator::GUID_PATTERN);
        }
    );

